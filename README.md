# JAMBER

JAMBER is a very basic toolkit for working with [AMBER](http://ambermd.org/) potentials and models, written in JAVA. It adheres to the standard [Maven](http://maven.apache.org/what-is-maven.html) framework and has associated [JUnit](http://en.wikipedia.org/wiki/JUnit) testcases to guard against development regression.

It aims to enable a user to build a model system with support for the AMBER force field. Eventually, it could provide the building blocks for a more modern version of {x,t}leap. In addition, there is limited support for creating a universe from AMBER's prmtop and inpcrd files.

# Compiling from Source
This will compile and install the JAMBER tools (only amber2openmm at the moment) to the Debian based OS.

## [Debian Wheezy](http://www.debian.org/releases/wheezy/) / [Ubuntu Precise](http://releases.ubuntu.com/precise/) / [Ubuntu Trusty](http://releases.ubuntu.com/trusty/)

1) Install the needed packages and configure Java.

    sudo apt-get install git maven openjdk-8-jdk
    # Ensure java8 is selected
    sudo update-alternatives --config java
    sudo update-alternatives --config javac

2) Clone, build and install.

    git clone https://bitbucket.org/mjw99/jamber.git
    cd jamber ; mvn clean package ; sudo dpkg -i ./target/*.deb
    amber2openmm

# Java Code Examples

##1) Calculate the potential energy of a prmtop and corresponding coordinate file.

    :::java
    import name.mjw.jamber.io.amber.Prmtop;
    import name.mjw.jamber.io.amber.Inpcrd;
    import name.mjw.jamber.Universe;

    public class JamberExample {

	    public static void main(String[] args) {

		    // Create instances of the prmtop and coordinate file
		    Prmtop prmtop = new Prmtop("prmtop");
		    Inpcrd inpcrd = new Inpcrd("inpcrd");

		    // Create the Universe
		    Universe universe = new Universe(prmtop, inpcrd);

		    // Print some information about it
		    System.out.println(universe);
	    }

    }
	

More examples in the units tests [here](https://bitbucket.org/mjw99/jamber/src/HEAD/src/test/java/name/mjw/jamber)

# Limitations

This is currently at a *very* early stage of development and the author apologies for any errors or bugs.

Mark J. Williamson

# License
Copyright 2012 Mark J. Williamson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and

# Third Party
 - [FortranFormat](http://www.ichemlabs.com/fortranformat)

# Acknowledgements
YourKit is kindly supporting this open source project with its full-featured Java Profiler. YourKit, 
LLC is the creator of innovative and intelligent tools for profiling Java and .NET applications. 
Take a look at YourKit's leading software products:
[YourKit Java Profiler](http://www.yourkit.com/java/profiler/index.jsp)

[YourKit .NET Profiler](http://www.yourkit.com/.net/profiler/index.jsp)
