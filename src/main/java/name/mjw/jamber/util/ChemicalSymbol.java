package name.mjw.jamber.util;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.apache.log4j.Logger;

import java.util.HashMap;

/**
 * Utility classes for looking up Chemical symbols from their atomic numbers and
 * vice versa.
 * 
 * @author mjw
 * 
 */
public final class ChemicalSymbol {

	private static final Logger LOG = Logger.getLogger(ChemicalSymbol.class);

	// Make use of a BiMap since we need to do a reverse lookup
	private static final BiMap<Integer, String> zToChemicalSymbolMap;

	// Reverse of the BiMap
	private static final BiMap<String, Integer> chemicalSymbolToZMap;

	static {
		zToChemicalSymbolMap = HashBiMap.create();

		/*
		 * Generated using AWK
		 *
		 * cat elements.dat | awk '{print "zToChemicalSymbolMap.put("$1",\""$2"\""");"}'
		 */

		zToChemicalSymbolMap.put(1, "H");
		zToChemicalSymbolMap.put(2, "He");
		zToChemicalSymbolMap.put(3, "Li");
		zToChemicalSymbolMap.put(4, "Be");
		zToChemicalSymbolMap.put(5, "B");
		zToChemicalSymbolMap.put(6, "C");
		zToChemicalSymbolMap.put(7, "N");
		zToChemicalSymbolMap.put(8, "O");
		zToChemicalSymbolMap.put(9, "F");
		zToChemicalSymbolMap.put(10, "Ne");
		zToChemicalSymbolMap.put(11, "Na");
		zToChemicalSymbolMap.put(12, "Mg");
		zToChemicalSymbolMap.put(13, "Al");
		zToChemicalSymbolMap.put(14, "Si");
		zToChemicalSymbolMap.put(15, "P");
		zToChemicalSymbolMap.put(16, "S");
		zToChemicalSymbolMap.put(17, "Cl");
		zToChemicalSymbolMap.put(18, "Ar");
		zToChemicalSymbolMap.put(19, "K");
		zToChemicalSymbolMap.put(20, "Ca");
		zToChemicalSymbolMap.put(21, "Sc");
		zToChemicalSymbolMap.put(22, "Ti");
		zToChemicalSymbolMap.put(23, "V");
		zToChemicalSymbolMap.put(24, "Cr");
		zToChemicalSymbolMap.put(25, "Mn");
		zToChemicalSymbolMap.put(26, "Fe");
		zToChemicalSymbolMap.put(27, "Co");
		zToChemicalSymbolMap.put(28, "Ni");
		zToChemicalSymbolMap.put(29, "Cu");
		zToChemicalSymbolMap.put(30, "Zn");
		zToChemicalSymbolMap.put(31, "Ga");
		zToChemicalSymbolMap.put(32, "Ge");
		zToChemicalSymbolMap.put(33, "As");
		zToChemicalSymbolMap.put(34, "Se");
		zToChemicalSymbolMap.put(35, "Br");
		zToChemicalSymbolMap.put(36, "Kr");
		zToChemicalSymbolMap.put(37, "Rb");
		zToChemicalSymbolMap.put(38, "Sr");
		zToChemicalSymbolMap.put(39, "Y");
		zToChemicalSymbolMap.put(40, "Zr");
		zToChemicalSymbolMap.put(41, "Nb");
		zToChemicalSymbolMap.put(42, "Mo");
		zToChemicalSymbolMap.put(43, "Tc");
		zToChemicalSymbolMap.put(44, "Ru");
		zToChemicalSymbolMap.put(45, "Rh");
		zToChemicalSymbolMap.put(46, "Pd");
		zToChemicalSymbolMap.put(47, "Ag");
		zToChemicalSymbolMap.put(48, "Cd");
		zToChemicalSymbolMap.put(49, "In");
		zToChemicalSymbolMap.put(50, "Sn");
		zToChemicalSymbolMap.put(51, "Sb");
		zToChemicalSymbolMap.put(52, "Te");
		zToChemicalSymbolMap.put(53, "I");
		zToChemicalSymbolMap.put(54, "Xe");
		zToChemicalSymbolMap.put(55, "Cs");
		zToChemicalSymbolMap.put(56, "Ba");
		zToChemicalSymbolMap.put(57, "La");
		zToChemicalSymbolMap.put(58, "Ce");
		zToChemicalSymbolMap.put(59, "Pr");
		zToChemicalSymbolMap.put(60, "Nd");
		zToChemicalSymbolMap.put(61, "Pm");
		zToChemicalSymbolMap.put(62, "Sm");
		zToChemicalSymbolMap.put(63, "Eu");
		zToChemicalSymbolMap.put(64, "Gd");
		zToChemicalSymbolMap.put(65, "Tb");
		zToChemicalSymbolMap.put(66, "Dy");
		zToChemicalSymbolMap.put(67, "Ho");
		zToChemicalSymbolMap.put(68, "Er");
		zToChemicalSymbolMap.put(69, "Tm");
		zToChemicalSymbolMap.put(70, "Yb");
		zToChemicalSymbolMap.put(71, "Lu");
		zToChemicalSymbolMap.put(72, "Hf");
		zToChemicalSymbolMap.put(73, "Ta");
		zToChemicalSymbolMap.put(74, "W");
		zToChemicalSymbolMap.put(75, "Re");
		zToChemicalSymbolMap.put(76, "Os");
		zToChemicalSymbolMap.put(77, "Ir");
		zToChemicalSymbolMap.put(78, "Pt");
		zToChemicalSymbolMap.put(79, "Au");
		zToChemicalSymbolMap.put(80, "Hg");
		zToChemicalSymbolMap.put(81, "Tl");
		zToChemicalSymbolMap.put(82, "Pb");
		zToChemicalSymbolMap.put(83, "Bi");
		zToChemicalSymbolMap.put(84, "Po");
		zToChemicalSymbolMap.put(85, "At");
		zToChemicalSymbolMap.put(86, "Rn");
		zToChemicalSymbolMap.put(87, "Fr");
		zToChemicalSymbolMap.put(88, "Ra");
		zToChemicalSymbolMap.put(89, "Ac");
		zToChemicalSymbolMap.put(90, "Th");
		zToChemicalSymbolMap.put(91, "Pa");
		zToChemicalSymbolMap.put(92, "U");
		zToChemicalSymbolMap.put(93, "Np");
		zToChemicalSymbolMap.put(94, "Pu");
		zToChemicalSymbolMap.put(95, "Am");
		zToChemicalSymbolMap.put(96, "Cm");
		zToChemicalSymbolMap.put(97, "Bk");
		zToChemicalSymbolMap.put(98, "Cf");
		zToChemicalSymbolMap.put(99, "Es");
		zToChemicalSymbolMap.put(100, "Fm");
		zToChemicalSymbolMap.put(101, "Md");
		zToChemicalSymbolMap.put(102, "No");
		zToChemicalSymbolMap.put(103, "Lr");

		// Now, generate the reverse map
		chemicalSymbolToZMap = zToChemicalSymbolMap.inverse();

	}

	/**
	 * Given a Z value (i.e. atomic number), return the chemical
	 * symbol that corresponds to this element.
	 * 
	 * @param z
	 *            Atomic number
	 * @return Chemical symbol
	 */
	public static String zToChemicalSymbol(Integer z) {

		if (zToChemicalSymbolMap.get(z) != null) {
			LOG.debug("ztoElementSymbolMap: Mapping atomic number value: " + z
					+ " to chemical symbol: " + zToChemicalSymbolMap.get(z));
			return zToChemicalSymbolMap.get(z);
		} else {
			// Catch values that don't exist
			throw new RuntimeException(
					"ztoElementSymbolMap: Chemical symbol for atomic number value: "
							+ z + " not found!");
		}

	}

	/**
	 * Given a chemical symbol, return its corresponding Z value.
	 * 
	 * @param chemicalSymbol
	 *            Chemical symbol
	 * 
	 * @return z value
	 */
	public static int chemicalSymbolToZ(String chemicalSymbol) {

		if (chemicalSymbolToZMap.get(chemicalSymbol) != null) {
			LOG.debug("ElementSymboltoZ: Mapping chemical symbol: "
					+ chemicalSymbol + " to Z value: "
					+ chemicalSymbolToZMap.get(chemicalSymbol));
			return chemicalSymbolToZMap.get(chemicalSymbol);
		} else {
			// Catch values that don't exist
			throw new RuntimeException(
					"ElementSymboltoZ: z value for chemical symbol "
							+ chemicalSymbol + " not found!");
		}

	}

	/**
	 * Give an amberAtomType, return the chemical symbol.
	 * This is really not a good approach.
	 * 
	 * @param amberAtomType
	 *            amber atom type
	 * @return Chemical symbol
	 */
	public static String amberAtomTypeToChemicalSymbol(String amberAtomType) {

		HashMap<String, String> amberAtomTypeToChemicalSymbol = new HashMap<>();

		// gaff.dat
		amberAtomTypeToChemicalSymbol.put("c", "C");
		amberAtomTypeToChemicalSymbol.put("c1", "C");
		amberAtomTypeToChemicalSymbol.put("c2", "C");
		amberAtomTypeToChemicalSymbol.put("c3", "C");
		amberAtomTypeToChemicalSymbol.put("ca", "C");
		amberAtomTypeToChemicalSymbol.put("cp", "C");
		amberAtomTypeToChemicalSymbol.put("cq", "C");
		amberAtomTypeToChemicalSymbol.put("cc", "C");
		amberAtomTypeToChemicalSymbol.put("cd", "C");
		amberAtomTypeToChemicalSymbol.put("ce", "C");
		amberAtomTypeToChemicalSymbol.put("cf", "C");
		amberAtomTypeToChemicalSymbol.put("cg", "C");
		amberAtomTypeToChemicalSymbol.put("ch", "C");
		amberAtomTypeToChemicalSymbol.put("cx", "C");
		amberAtomTypeToChemicalSymbol.put("cy", "C");
		amberAtomTypeToChemicalSymbol.put("cu", "C");
		amberAtomTypeToChemicalSymbol.put("cv", "C");
		amberAtomTypeToChemicalSymbol.put("cz", "C");

		amberAtomTypeToChemicalSymbol.put("h1", "H");
		amberAtomTypeToChemicalSymbol.put("h2", "H");
		amberAtomTypeToChemicalSymbol.put("h3", "H");
		amberAtomTypeToChemicalSymbol.put("h4", "H");
		amberAtomTypeToChemicalSymbol.put("h5", "H");
		amberAtomTypeToChemicalSymbol.put("ha", "H");
		amberAtomTypeToChemicalSymbol.put("hc", "H");
		amberAtomTypeToChemicalSymbol.put("hn", "H");
		amberAtomTypeToChemicalSymbol.put("ho", "H");
		amberAtomTypeToChemicalSymbol.put("hp", "H");
		amberAtomTypeToChemicalSymbol.put("hs", "H");
		amberAtomTypeToChemicalSymbol.put("hw", "H");
		amberAtomTypeToChemicalSymbol.put("hx", "H");

		amberAtomTypeToChemicalSymbol.put("f", "F");
		amberAtomTypeToChemicalSymbol.put("cl", "Cl");
		amberAtomTypeToChemicalSymbol.put("br", "Br");
		amberAtomTypeToChemicalSymbol.put("i", "I");

		amberAtomTypeToChemicalSymbol.put("n", "N");
		amberAtomTypeToChemicalSymbol.put("n1", "N");
		amberAtomTypeToChemicalSymbol.put("n2", "N");
		amberAtomTypeToChemicalSymbol.put("n3", "N");
		amberAtomTypeToChemicalSymbol.put("n4", "N");
		amberAtomTypeToChemicalSymbol.put("na", "N");
		amberAtomTypeToChemicalSymbol.put("nb", "N");
		amberAtomTypeToChemicalSymbol.put("nc", "N");
		amberAtomTypeToChemicalSymbol.put("nd", "N");
		amberAtomTypeToChemicalSymbol.put("ne", "N");
		amberAtomTypeToChemicalSymbol.put("nf", "N");
		amberAtomTypeToChemicalSymbol.put("nh", "N");
		amberAtomTypeToChemicalSymbol.put("no", "N");

		amberAtomTypeToChemicalSymbol.put("o", "O");
		amberAtomTypeToChemicalSymbol.put("oh", "O");
		amberAtomTypeToChemicalSymbol.put("os", "O");
		amberAtomTypeToChemicalSymbol.put("ow", "O");

		amberAtomTypeToChemicalSymbol.put("p2", "P");
		amberAtomTypeToChemicalSymbol.put("p3", "P");
		amberAtomTypeToChemicalSymbol.put("p4", "P");
		amberAtomTypeToChemicalSymbol.put("p5", "P");
		amberAtomTypeToChemicalSymbol.put("pb", "P");
		amberAtomTypeToChemicalSymbol.put("pc", "P");
		amberAtomTypeToChemicalSymbol.put("pd", "P");
		amberAtomTypeToChemicalSymbol.put("pe", "P");
		amberAtomTypeToChemicalSymbol.put("pf", "P");
		amberAtomTypeToChemicalSymbol.put("px", "P");
		amberAtomTypeToChemicalSymbol.put("py", "P");

		amberAtomTypeToChemicalSymbol.put("s", "S");
		amberAtomTypeToChemicalSymbol.put("s2", "S");
		amberAtomTypeToChemicalSymbol.put("s4", "S");
		amberAtomTypeToChemicalSymbol.put("s6", "S");
		amberAtomTypeToChemicalSymbol.put("sh", "S");
		amberAtomTypeToChemicalSymbol.put("ss", "S");
		amberAtomTypeToChemicalSymbol.put("sx", "S");
		amberAtomTypeToChemicalSymbol.put("sy", "S");

		amberAtomTypeToChemicalSymbol.put("fe", "Fe");

		// parm99.dat
		amberAtomTypeToChemicalSymbol.put("C", "C");
		amberAtomTypeToChemicalSymbol.put("CA", "C");
		amberAtomTypeToChemicalSymbol.put("CB", "C");
		amberAtomTypeToChemicalSymbol.put("CC", "C");
		amberAtomTypeToChemicalSymbol.put("CD", "C");
		amberAtomTypeToChemicalSymbol.put("CK", "C");
		amberAtomTypeToChemicalSymbol.put("CM", "C");
		amberAtomTypeToChemicalSymbol.put("CN", "C");
		amberAtomTypeToChemicalSymbol.put("CQ", "C");
		amberAtomTypeToChemicalSymbol.put("CR", "C");
		amberAtomTypeToChemicalSymbol.put("CT", "C");
		amberAtomTypeToChemicalSymbol.put("CV", "C");
		amberAtomTypeToChemicalSymbol.put("CW", "C");
		amberAtomTypeToChemicalSymbol.put("C*", "C");
		amberAtomTypeToChemicalSymbol.put("CY", "C");
		amberAtomTypeToChemicalSymbol.put("CZ", "C");

		// frcmod.phosaa10
		amberAtomTypeToChemicalSymbol.put("OP", "O");
		amberAtomTypeToChemicalSymbol.put("OQ", "O");
		amberAtomTypeToChemicalSymbol.put("OR", "O");
		amberAtomTypeToChemicalSymbol.put("OT", "O");
		amberAtomTypeToChemicalSymbol.put("OV", "O");
		amberAtomTypeToChemicalSymbol.put("OX", "O");
		amberAtomTypeToChemicalSymbol.put("OZ", "O");

		// ?
		// amberAtomTypeToElementSymbol.put(C0 40.08

		amberAtomTypeToChemicalSymbol.put("H", "H");
		amberAtomTypeToChemicalSymbol.put("HC", "H");
		amberAtomTypeToChemicalSymbol.put("H1", "H");
		amberAtomTypeToChemicalSymbol.put("H2", "H");
		amberAtomTypeToChemicalSymbol.put("H3", "H");
		amberAtomTypeToChemicalSymbol.put("HA", "H");
		amberAtomTypeToChemicalSymbol.put("H4", "H");
		amberAtomTypeToChemicalSymbol.put("H5", "H");
		amberAtomTypeToChemicalSymbol.put("HO", "H");
		amberAtomTypeToChemicalSymbol.put("HS", "H");
		amberAtomTypeToChemicalSymbol.put("HW", "H");
		amberAtomTypeToChemicalSymbol.put("HP", "H");
		amberAtomTypeToChemicalSymbol.put("HZ", "H");

		amberAtomTypeToChemicalSymbol.put("F", "F");
		amberAtomTypeToChemicalSymbol.put("Cl", "Cl");
		amberAtomTypeToChemicalSymbol.put("Br", "Br");
		amberAtomTypeToChemicalSymbol.put("I", "I");

		amberAtomTypeToChemicalSymbol.put("MG", "Mg");

		amberAtomTypeToChemicalSymbol.put("N", "N");
		amberAtomTypeToChemicalSymbol.put("NA", "N");
		amberAtomTypeToChemicalSymbol.put("NB", "N");
		amberAtomTypeToChemicalSymbol.put("NC", "N");
		amberAtomTypeToChemicalSymbol.put("N2", "N");
		amberAtomTypeToChemicalSymbol.put("N3", "N");
		amberAtomTypeToChemicalSymbol.put("NT", "N");
		amberAtomTypeToChemicalSymbol.put("N*", "N");
		amberAtomTypeToChemicalSymbol.put("NY", "N");

		amberAtomTypeToChemicalSymbol.put("O", "O");
		amberAtomTypeToChemicalSymbol.put("O2", "O");
		amberAtomTypeToChemicalSymbol.put("OW", "O");
		amberAtomTypeToChemicalSymbol.put("OH", "O");
		amberAtomTypeToChemicalSymbol.put("OS", "O");

		amberAtomTypeToChemicalSymbol.put("oa", "O");

		amberAtomTypeToChemicalSymbol.put("P", "P");

		amberAtomTypeToChemicalSymbol.put("S", "S");
		amberAtomTypeToChemicalSymbol.put("SH", "S");

		amberAtomTypeToChemicalSymbol.put("CU", "Cu");
		amberAtomTypeToChemicalSymbol.put("FE", "Fe");
		amberAtomTypeToChemicalSymbol.put("Li", "Li");

		amberAtomTypeToChemicalSymbol.put("Na", "Na");
		amberAtomTypeToChemicalSymbol.put("K", "K");
		amberAtomTypeToChemicalSymbol.put("Rb", "Rb");
		amberAtomTypeToChemicalSymbol.put("Cs", "Cs");
		amberAtomTypeToChemicalSymbol.put("Zn", "Zn");

		if (amberAtomTypeToChemicalSymbol.get(amberAtomType) != null) {
			LOG.debug("amberAtomTypeToChemicalSymbol: Mapping amberAtomType: "
					+ amberAtomType + " to chemical symbol: "
					+ amberAtomTypeToChemicalSymbol.get(amberAtomType));
			return amberAtomTypeToChemicalSymbol.get(amberAtomType);
		} else {
			// Catch values that don't exist
			throw new RuntimeException(
					"amberAtomTypeToChemicalSymbol: Chemical symbol for amberAtomType: "
							+ amberAtomType + " not found!");
		}

	}
}
