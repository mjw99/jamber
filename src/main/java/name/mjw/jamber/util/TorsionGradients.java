package name.mjw.jamber.util;

import javax.vecmath.Vector3d;

public final class TorsionGradients {
	private Vector3d i;
	private Vector3d j;
	private Vector3d k;
	private Vector3d l;

	public Vector3d getI() {
		return i;
	}

	public void setI(Vector3d i) {
		this.i = i;
	}

	public Vector3d getJ() {
		return j;
	}

	public void setJ(Vector3d j) {
		this.j = j;
	}

	public Vector3d getK() {
		return k;
	}

	public void setK(Vector3d k) {
		this.k = k;
	}

	public Vector3d getL() {
		return l;
	}

	public void setL(Vector3d l) {
		this.l = l;
	}

	public String toString() {
		return 	i.toString() + " kcal/(A mol)\n" +
				j.toString() + " kcal/(A mol)\n" +
				k.toString() + " kcal/(A mol)\n" +
				l.toString() + " kcal/(A mol)";
	}
}
