package name.mjw.jamber;

/**
 * General type for force / energy object
 * 
 * @author mjw
 * 
 */
abstract class ForceFieldTerm {

	/**
	 * Calculate the potential energy contribution from this term.
	 */
	abstract public double getPotentialEnergy();
	
	/**
	 * Returns the current (scalar) gradient of the potential energy term
	 * This is the analytical derivative of the potential energy at this point
	 */
	abstract public double getAnalyticalGradient();

	/**
	 * Update the force of the atoms involved in this term.
	 */
	abstract public void evaluateForce();

	/**
	 * Short hand for the square operation
	 */
    double square(double n) {

		return java.lang.Math.pow(n, 2);
	}
}
