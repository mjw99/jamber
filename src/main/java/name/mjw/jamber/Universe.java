package name.mjw.jamber;

import org.apache.log4j.Logger;

import name.mjw.jamber.io.amber.Inpcrd;
import name.mjw.jamber.io.amber.Prmtop;

import java.util.ArrayList;

/**
 * An object that contains all atoms and their associated force field terms.
 * 
 * @author mjw
 * 
 */
public class Universe {

	private final Logger LOG = Logger.getLogger(Universe.class);

	/**
	 * @param args
	 */

	/**
	 * List of all atoms existing in this Universe
	 */
	private final ArrayList<Atom> atoms;

	/**
	 * List of all bonds existing in this Universe
	 */
	final ArrayList<Bond> bonds;

	/**
	 * List of all angles existing in this Universe
	 */
	final ArrayList<Angle> angles;

	/**
	 * List of all dihedrals existing in this Universe
	 */
	private final ArrayList<Dihedral> dihedrals;

	/**
	 * List of all vdw terms existing in this Universe
	 */
	private final ArrayList<LJTwelveSix> vdws;

	/**
	 * List of all 1-4 vdw terms existing in this Universe
	 */
	private final ArrayList<LJTwelveSix> vdws14;

	/**
	 * List of all Electrostatic interactions
	 */
	private final ArrayList<ElectroStatic> ees;

	/**
	 * List of all 1-4 Electrostatic interactions
	 */
	private final ArrayList<ElectroStatic> ees14;

	// Energies

	/**
	 * Total kinetic energy of universe in kcal.
	 */
	private double kineticEnergy;

	/**
	 * Non-bonded cut off distance
	 */
	private double cutoff = 999.0;

	public Universe() {
		this.atoms = new ArrayList<>();
		this.bonds = new ArrayList<>();
		this.angles = new ArrayList<>();
		this.dihedrals = new ArrayList<>();

		this.vdws = new ArrayList<>();
		this.vdws14 = new ArrayList<>();

		this.ees = new ArrayList<>();
		this.ees14 = new ArrayList<>();
	}

	/**
	 * Construct an Universe from a pair of Prmtop and Inpcrd objects. This
	 * extracts atoms, coordinates and associated FF parameters from this pair
	 * and adds them to the Universe object.
	 * 
	 * @param prmtop
	 *            AMBER topology to be read in
	 * @param inpcrd
	 *            Corresponding coordinates to prmtop
	 */
	public Universe(Prmtop prmtop, Inpcrd inpcrd) {

		this.atoms = new ArrayList<>();

		// Add atoms + their respective atom properties
		for (int i = 0; i < prmtop.getNumberOfAtoms(); i++) {
			Atom atom = new Atom(inpcrd.getPositions().get(i));

			// Set the index
			atom.setIndex(i + 1);

			// Gather atom AMBER types
			atom.setAMBERAtomType(prmtop.getAmberAtomType().get(i));

			// Gather atom name
			atom.setName(prmtop.getAtomName().get(i));

			// Gather masses
			atom.setMass(prmtop.getMass().get(i));

			// Gather charges
			atom.setCharge((prmtop.getCharges().get(i)) / 18.2223);

			/*
			 * Get the VDW parameters
			 * 
			 * ======================
			 * 
			 * The 12-6 Lennard Jones interaction is a function of the position
			 * of two atoms and their atomTypes. Associated with each atom type
			 * are two parameters:
			 * 
			 * radius6_12VDW = Characteristic distance (A)
			 * 
			 * wellDepth6_12VDW = Well depth (kcal)
			 * 
			 * Hence for the atom pair for c3-c3:
			 * 
			 * c3 1.9080 0.1094 OPLS
			 * 
			 * radius6_12VDW = 1.9080 wellDepth6_12VDW =0.1094
			 * 
			 * Hence rij = 3.816 eij = 0.1094
			 * 
			 * Therefore, LENNARD_JONES_{A,B}COEF are: A = 1043080.2307 B =
			 * 675.6122
			 */

			/*
			 * The general look up mechanism for the lookup of
			 * LENNARD_JONES_{A,B}COEF values for a pair of atoms i and j in a
			 * prmtop file is: CN{1,2}( ICO( ((IAC(i)-1)*NTYPES) + IAC(j) ) )
			 * 
			 * It would initially appear that by calculating the composite
			 * LENNARD_JONES_{A,B}COEF values, one has lost the value of each
			 * atom's r_i and e_i**However**, the every self interaction term
			 * (e.g. c3-c3, ) is also in the LENNARD_JONES_{A,B}COEF list, hence
			 * we can back calculate the r_i and e_i as a function of atomType
			 * by using the relation that :
			 * 
			 * r_i = ((2A/B)/^(1/6))/2 e_i = (B^2)/4A
			 * 
			 * and we look up the self interaction of an atom using: CN{1,2}(
			 * ICO( ((IAC(i)-1)*NTYPES) + IAC(i) ) )
			 */

			Integer ntypes = prmtop.getNumberOfAtomsTypes();

			/*
			 * 1) Look up the current atom in the ATOM_TYPE_INDEX ( IAC() )
			 * 
			 * %FLAG ATOM_TYPE_INDEX
			 * 
			 * %FORMAT(10I8)
			 * 
			 * 1 2 3 3 3 4 5 6
			 */

			Integer atomTypeIndex = (prmtop.getAtomTypeIndex().get(i));
			// CN{1,2}( ICO( ((IAC(i)-1)*NTYPES) + IAC(i) ) )
			Integer finalLookup = (atomTypeIndex - 1) * ntypes + atomTypeIndex;

			/*
			 * ICO()
			 * 
			 * %FLAG NONBONDED_PARM_INDEX
			 * 
			 * %FORMAT(10I8)
			 * 
			 * 1 2 4 7 11 16 2 3 5 8
			 * 
			 * 12 17 4 5 6 9 13 18 7 8
			 * 
			 * 9 10 14 19 11 12 13 14 15 20
			 * 
			 * 16 17 18 19 20 21
			 */

			// Look up into NONBONDED_PARM_INDEX (i.e. ICO() )
			Integer index = prmtop.getNonbondedParmIndex().get(finalLookup - 1);

			// Look up into LENNARD_JONES_ACOEF (i.e. CN1() )
			Double lennardJonesAcoef = prmtop.getLennardJonesAcoef().get(
					index - 1);
			// Look up into LENNARD_JONES_BCOEF (i.e. CN2() )
			Double lennardJonesBcoef = prmtop.getLennardJonesBcoef().get(
					index - 1);

			// r_i e_i A B LENNARD_JONES_ACOEF position NONBONDED_PARM_INDEX
			// position
			// c3-c3 1.9080 0.1094 1043080.230 675.6122 1 1
			// c -c 1.9080 0.0860 819971.6621 531.1028 3 8
			// hc-hc 1.4870 0.0157 7516.077 21.72578 6 15
			// o -o 1.6612 0.2100 379876.398 564.8859 10 22
			// oh-oh 1.7210 0.2104 581803.229 699.7468 15 29 NOTE THE GAP OF 7!
			// ho-ho 0.0000 0.0000 0.0 0.0 ?

			// %FLAG LENNARD_JONES_ACOEF
			// %FORMAT(5E16.8)
			// 1.04308023E+06 9.24822270E+05 8.19971662E+05 9.71708117E+04
			// 8.61541883E+04
			// 7.51607703E+03 6.47841731E+05 5.74393458E+05 5.44261042E+04
			// 3.79876399E+05
			// 7.91544157E+05 7.01803794E+05 6.82786631E+04 4.71003287E+05
			// 5.81803229E+05
			// 0.00000000E+00 0.00000000E+00 0.00000000E+00 0.00000000E+00
			// 0.00000000E+00
			// 0.00000000E+00
			// %FLAG LENNARD_JONES_BCOEF
			// %FORMAT(5E16.8)
			// 6.75612247E+02 5.99015525E+02 5.31102864E+02 1.26919150E+02
			// 1.12529845E+02
			// 2.17257828E+01 6.26720080E+02 5.55666448E+02 1.11805549E+02
			// 5.64885984E+02
			// 6.93079947E+02 6.14502845E+02 1.25287818E+02 6.29300710E+02
			// 6.99746810E+02
			// 0.00000000E+00 0.00000000E+00 0.00000000E+00 0.00000000E+00
			// 0.00000000E+00
			// 0.00000000E+00

			Double rI;
			Double eI;

			if (lennardJonesAcoef == 0.0) {

				atom.setRadius6_12VDW(0.0);
				atom.setWellDepth6_12VDW(0.0);

			} else {
				rI = (Math.pow(((2.0 * lennardJonesAcoef) / lennardJonesBcoef),
						(1.0 / 6.0))) / 2.0;
				eI = (Math.pow(lennardJonesBcoef, 2)) / (4 * lennardJonesAcoef);

				atom.setRadius6_12VDW(rI);
				atom.setWellDepth6_12VDW(eI);

			}

			LOG.debug("Adding atom");
			LOG.debug(atom.toString());

			this.addAtom(atom);

		}

		this.bonds = new ArrayList<>();

		/*
		 * Add in bonds i) Add bonds with hydrogen
		 */
		for (int i = 0; i < prmtop.getNumberOfBondsContainingHydrogen() * 3; i = i + 3) {

			/*
			 * NOTE: the atom numbers in the following arrays that describe
			 * bonds, angles, and dihedrals are coordinate array indexes for
			 * runtime speed. The true atom number equals the absolute value of
			 * the number divided by three, plus one. In the case of the
			 * dihedrals, if the fourth atom is negative, this implies that the
			 * dihedral is an improper. If the third atom is negative, this
			 * implies that the end group interations are to be ignored. End
			 * group interactions are ignored, for example, in dihedrals of
			 * various ring systems (to prevent double counting of 1-4
			 * interactions) and in multiterm dihedrals.
			 */

			// %FLAG BONDS_INC_HYDROGEN
			// %FORMAT(10I8)
			// 3 6 3
			// i j index into Rk and Req

			int AtomI = (prmtop.getBondsIncHydrogen().get(i)) / 3 + 1;
			int AtomJ = (prmtop.getBondsIncHydrogen().get(i + 1)) / 3 + 1;

			Bond bond = new Bond(this.getAtom(AtomI - 1),
					this.getAtom(AtomJ - 1),

					prmtop.getBondEquilValue().get(
							prmtop.getBondsIncHydrogen().get(i + 2) - 1),
					prmtop.getBondForceConstant().get(
							prmtop.getBondsIncHydrogen().get(i + 2) - 1));

			LOG.debug("Adding bond with hydrogen");
			LOG.debug(bond.toString());
			this.addBond(bond);

		}

		// ii) Add bonds without hydrogen
		for (int i = 0; i < prmtop.getNumberOfBondsNotContainingHydrogen() * 3; i = i + 3) {

			int AtomI = (prmtop.getBondsWithoutHydrogen().get(i)) / 3 + 1;
			int AtomJ = (prmtop.getBondsWithoutHydrogen().get(i + 1)) / 3 + 1;

			Bond bond = new Bond(this.getAtom(AtomI - 1),
					this.getAtom(AtomJ - 1),

					prmtop.getBondEquilValue().get(
							prmtop.getBondsWithoutHydrogen().get(i + 2) - 1),
					prmtop.getBondForceConstant().get(
							prmtop.getBondsWithoutHydrogen().get(i + 2) - 1));

			LOG.debug("Adding bond without hydrogen");
			LOG.debug(bond.toString());
			this.addBond(bond);

		}

		// Add in Angles
		this.angles = new ArrayList<>();

		// i) Add angles with hydrogen
		for (int i = 0; i < prmtop.getNumberOfAnglesContainingHydrogen() * 4; i = i + 4) {

			int AtomI = (prmtop.getAnglesIncHydrogen().get(i)) / 3 + 1;
			int AtomJ = (prmtop.getAnglesIncHydrogen().get(i + 1)) / 3 + 1;
			int AtomK = (prmtop.getAnglesIncHydrogen().get(i + 2)) / 3 + 1;

			Angle angle = new Angle(this.getAtom(AtomI - 1),
					this.getAtom(AtomJ - 1), this.getAtom(AtomK - 1),

					Math.toDegrees(prmtop.getAngleEquilValue().get(
							prmtop.getAnglesIncHydrogen().get(i + 3) - 1)),
					prmtop.getAngleForceConstant().get(
							prmtop.getAnglesIncHydrogen().get(i + 3) - 1));

			LOG.debug("Adding angle without hydrogen");
			LOG.debug(angle.toString());
			this.addAngle(angle);

		}

		// ii) Add angles without hydrogen
		for (int i = 0; i < prmtop.getNumberOfAnglesNotContainingHydrogen() * 4; i = i + 4) {

			int AtomI = (prmtop.getAnglesWithoutHydrogen().get(i)) / 3 + 1;
			int AtomJ = (prmtop.getAnglesWithoutHydrogen().get(i + 1)) / 3 + 1;
			int AtomK = (prmtop.getAnglesWithoutHydrogen().get(i + 2)) / 3 + 1;

			Angle angle = new Angle(this.getAtom(AtomI - 1),
					this.getAtom(AtomJ - 1), this.getAtom(AtomK - 1),

					Math.toDegrees(prmtop.getAngleEquilValue().get(
							prmtop.getAnglesWithoutHydrogen().get(i + 3) - 1)),
					prmtop.getAngleForceConstant().get(
							prmtop.getAnglesWithoutHydrogen().get(i + 3) - 1));

			LOG.debug("Adding angle with hydrogen");
			LOG.debug(angle.toString());
			this.addAngle(angle);

		}

		// Add in Dihedrals
		this.dihedrals = new ArrayList<>();

		// i) Add Dihedrals with hydrogen
		for (int i = 0; i < prmtop.getNumberOfDihedralsContainingHydrogen() * 5; i = i + 5) {

			int AtomI = (prmtop.getDihedralsIncHydrogen().get(i)) / 3 + 1;
			int AtomJ = (prmtop.getDihedralsIncHydrogen().get(i + 1)) / 3 + 1;
			int AtomK = (prmtop.getDihedralsIncHydrogen().get(i + 2)) / 3 + 1;
			int AtomL = (prmtop.getDihedralsIncHydrogen().get(i + 3)) / 3 + 1;

			int index = prmtop.getDihedralsIncHydrogen().get(i + 4);

			Dihedral dihedral = new Dihedral(this.getAtom(Math.abs(AtomI - 1)),
					this.getAtom(Math.abs(AtomJ - 1)), this.getAtom(Math
							.abs(AtomK - 1)),
					this.getAtom(Math.abs(AtomL - 1)), prmtop
							.getDihedralForceConstant().get(index - 1), prmtop
							.getDihedralPhase().get(index - 1), prmtop
							.getDihedralPeriodicity().get(index - 1));

			// Do not use the 14 information here for 14 NB lists
			if ((AtomK - 1) < 0) {
				dihedral.setContributeTo14NonBondedTerms(false);
			}

			// Is the torsion an improper?
			if ((AtomL - 1) < 0) {
				dihedral.setImproper();
			}

			// TODO
			// We've not got multiterm information from this process.

			LOG.debug("Adding dihedral with hydrogen");
			LOG.debug(dihedral.toString());
			this.addDihedral(dihedral);

		}

		// Dihedrals without hydrogen
		for (int i = 0; i < prmtop.getNumberOfDihedralsNotContainingHydrogen() * 5; i = i + 5) {

			int AtomI = (prmtop.getDihedralsWithoutHydrogen().get(i)) / 3 + 1;
			int AtomJ = (prmtop.getDihedralsWithoutHydrogen().get(i + 1)) / 3 + 1;
			int AtomK = (prmtop.getDihedralsWithoutHydrogen().get(i + 2)) / 3 + 1;
			int AtomL = (prmtop.getDihedralsWithoutHydrogen().get(i + 3)) / 3 + 1;

			int index = prmtop.getDihedralsWithoutHydrogen().get(i + 4);

			Dihedral dihedral = new Dihedral(this.getAtom(Math.abs(AtomI - 1)),
					this.getAtom(Math.abs(AtomJ - 1)), this.getAtom(Math
							.abs(AtomK - 1)),
					this.getAtom(Math.abs(AtomL - 1)), prmtop
							.getDihedralForceConstant().get(index - 1), prmtop
							.getDihedralPhase().get(index - 1), prmtop
							.getDihedralPeriodicity().get(index - 1));

			// Do not use the 14 information here for 14 NB lists
			if ((AtomK - 1) < 0) {
				dihedral.setContributeTo14NonBondedTerms(false);
			}

			// Is the torsion an improper?
			if ((AtomL - 1) < 0) {
				dihedral.setImproper();
			}

			LOG.debug("Adding dihedral without hydrogen");
			LOG.debug(dihedral.toString());
			this.addDihedral(dihedral);

		}

		// Initialise Vdws + EEs
		this.vdws = new ArrayList<>();
		this.vdws14 = new ArrayList<>();

		this.ees = new ArrayList<>();
		this.ees14 = new ArrayList<>();

		calulateNeighbourList();
		// Remove excluded atoms
		// TODO, this needs to go over to prmtop as a method.
		removeExcludedAtoms(prmtop);

	}

	/**
	 * For each atom, calculate a list of other atoms within a cutoff distance
	 * and then add that list of atoms to the atom's neighbour list.
	 */
	private void calulateNeighbourList() {

		for (Atom atomI : atoms) {
			// For atomI here, construct a local array of atoms
			// that are within the cutoff distance of the Universe
			ArrayList<Atom> foundAtoms = new ArrayList<>();

			for (Atom atomJ : atoms) {
				double distance = atomI.DistanceFrom(atomJ);

				// Prevent an atom from being on its own neighbour list
				if ((distance <= this.cutoff) && (distance != 0.0)) {

					foundAtoms.add(atomJ);

				}
				// Add the atom array of neighbours to atomI
				atomI.setNeighbours(foundAtoms);

			}

			if (LOG.isDebugEnabled()) {

				for (int i = 0; i < atomI.getNeighbours().size(); i++) {
					LOG.debug("Atom  " + atomI.getName() + "("
							+ atomI.getIndex() + ")" + " has neighbours "
							+ atomI.getNeighbours().get(i).getName() + "("
							+ atomI.getNeighbours().get(i).getIndex() + ")");

				}
			}

		}

		// Clean up duplicates in the neighbour list
		//
		// Iterate over all atoms
		for (Atom atomI : atoms) {
			// Iterate over atom I's neighbours
			for (Atom neighbour : atomI.getNeighbours()) {

				// Look at each neighbour's neighbour list and see if it
				// contains atomI
				if (neighbour.getNeighbours().contains(atomI)) {

					if (LOG.isDebugEnabled()) {

						LOG.debug("Applying neighbour list duplication removal, for atom: "
								+ neighbour.getName()
								+ "("
								+ neighbour.getIndex()
								+ ")"
								+ ", removing duplicate neighbour atom "
								+ atomI.getName()
								+ "("
								+ atomI.getIndex()
								+ ")");

					}

					neighbour.getNeighbours().remove(atomI);

				}

			}

		}

	}

	// TODO, this needs to go over to prmtop as a method.
	/**
	 * Operates on the atoms list in Universe, specifically, the the Atom array
	 * atoms.neighbours. Removes any atoms that are considered to be already
	 * accounted for by other terms, i.e. they may already be in a bond, angle
	 * or dihedral term. This is determined using the arrays
	 * NUMBER_EXCLUDED_ATOMS and EXCLUDED_ATOMS_LIST in the prmtop file.
	 */
	private void removeExcludedAtoms(Prmtop prmtop) {

		Integer numberOfExcludedAtomsForThisAtom;
		Integer excludedAtomsListPointer = 0;

		for (int i = 0; i < prmtop.getNumberOfAtoms(); i++) {

			// %FLAG NUMBER_EXCLUDED_ATOMS
			// For the NATOMS atom in the system, the value here tells us how
			// many excluded atoms there are for this specific atom.
			numberOfExcludedAtomsForThisAtom = prmtop.getExcludedAtomsIndex()
					.get(i);
			// System.out.println("Atom index: " + (i + 1) + " has "
			// + numberOfExcludedAtomsForThisAtom
			// + " excluded neighbours:");

			// Look at the excluded atoms for the current atom (i)

			for (int k = excludedAtomsListPointer; k < (numberOfExcludedAtomsForThisAtom + excludedAtomsListPointer); k++) {
				// %FLAG EXCLUDED_ATOMS_LIST
				Integer excludedAtomIndex = prmtop.getExcludedAtomsList()
						.get(k);
				// System.out.println("        Atom index: " + excludedAtom
				// + " is an excluded neighbour");

				// Walk the current atom i's neighbour list
				for (int n = 0; n < this.atoms.get(i).getNeighbours().size(); n++) {

					// Check if the excluded atom for this atom i is in atom i's
					// neighbour list
					if (excludedAtomIndex == this.atoms.get(i).getNeighbours()
							.get(n).getIndex()) {

						if (LOG.isDebugEnabled()) {

							LOG.debug("Applying PRMTOP exclusions, for atom: "
									+ this.atoms.get(i).getName()
									+ "("
									+ this.atoms.get(i).getIndex()
									+ ")"
									+ ", removing neighbour atom "
									+ this.atoms.get(i).getNeighbours().get(n)
											.getName()
									+ "("
									+ this.atoms.get(i).getNeighbours().get(n)
											.getIndex() + ")");
						}

						// Remove atom n from atom i's neighbour list
						this.atoms.get(i).getNeighbours().remove(n);

					}

				}

			}

			excludedAtomsListPointer += numberOfExcludedAtomsForThisAtom;

		}

	}

	public double getPotentialEnergy() {

		LOG.info("Calculating Universe Potential Energy");

		// TODO
		// long startTime = System.nanoTime();
		// ... the code being measured ...
		// long estimatedTime = System.nanoTime() - startTime;

		/*
		 * Total potential energy of universe in kcal.
		 */
		double potentialEnergy = 0.0;

		// Bonded interactions
		potentialEnergy += getBondPotentialEnergy();
		potentialEnergy += getAnglePotentialEnergy();
		potentialEnergy += getDihedralPotentialEnergy();

		// Non-bonded interactions
		potentialEnergy += getVdwPotentialEnergy();
		potentialEnergy += get14VdwPotentialEnergy();

		potentialEnergy += getEEPotentialEnergy();
		potentialEnergy += get14EEPotentialEnergy();

		return potentialEnergy;
	}

	/**
	 * Returns the total bonded energy in the Universe in kcal/mol
	 * 
	 * @return total bond energy in the Universe in kcal/mol
	 */
	public double getBondPotentialEnergy() {
		double bondPotentialEnergy = 0.0;

		for (Bond bond : bonds) {
			bondPotentialEnergy += bond.getPotentialEnergy();
		}

		return bondPotentialEnergy;
	}

	/**
	 * Returns the total angle energy in the Universe in kcal/mol
	 * 
	 * @return total angle energy in the Universe in kcal/mol
	 */
	public double getAnglePotentialEnergy() {
		double AnglePotentialEnergy = 0.0;

		for (Angle angle : angles) {
			AnglePotentialEnergy += angle.getPotentialEnergy();
		}

		return AnglePotentialEnergy;
	}

	/**
	 * Returns the total dihedral energy in the Universe in kcal/mol
	 * 
	 * @return total dihedral energy in the Universe in kcal/mol
	 */
	public double getDihedralPotentialEnergy() {
		double dihedralPotentialEnergy = 0.0;

		for (Dihedral dihedral : dihedrals) {
			dihedralPotentialEnergy += dihedral.getPotentialEnergy();
		}

		return dihedralPotentialEnergy;
	}

	/**
	 * Returns the total VDW energy in the Universe in kcal/mol
	 * 
	 * @return total VDW energy in the Universe in kcal/mol
	 */
	public double getVdwPotentialEnergy() {
		double vdwPotentialEnergy = 0.0;
		this.vdws.clear();

		for (Atom atomI : atoms) {
			// Walk the neighbour list and recreate the vdw objects
			if (atomI.getNeighbours() != null) {
				for (Atom atomJ : atomI.getNeighbours()) {
					LJTwelveSix lJTwelveSix = new LJTwelveSix(atomI,
							atomI.getRadius6_12VDW(),
							atomI.getWellDepth6_12VDW(), atomJ,
							atomJ.getRadius6_12VDW(),
							atomJ.getWellDepth6_12VDW(), 1.0);
					this.addVdw(lJTwelveSix);
				}
			}

		}

		for (LJTwelveSix lJTwelveSix : vdws) {
			vdwPotentialEnergy += lJTwelveSix.getPotentialEnergy();
		}

		return vdwPotentialEnergy;
	}

	/**
	 * Returns the total VDW energy from 1-4 interactions in the Universe in
	 * kcal/mol
	 * 
	 * @return total VDW energy from 1-4 interactions in the Universe in
	 *         kcal/mol
	 */
	public double get14VdwPotentialEnergy() {
		double vdw14PotentialEnergy = 0.0;
		this.vdws14.clear();
		double scnb = 2.0;

		for (Dihedral dihedral : dihedrals) {

			if (dihedral.isContributeTo14NonBondedTerms()
					&& !(dihedral.isImproper())) {

				Atom atomI = dihedral.getAtomI();
				Atom atomL = dihedral.getAtomL();

				LJTwelveSix lJTwelveSix = new LJTwelveSix(atomI,
						atomI.getRadius6_12VDW(), atomI.getWellDepth6_12VDW(),
						atomL, atomL.getRadius6_12VDW(),
						atomL.getWellDepth6_12VDW(), scnb);

				this.addVdw14(lJTwelveSix);

			}

		}

		for (LJTwelveSix lJTwelveSix : vdws14) {
			vdw14PotentialEnergy += lJTwelveSix.getPotentialEnergy();
		}

		return vdw14PotentialEnergy;

	}

	/**
	 * Returns the total electrostatic energy in the Universe in kcal/mol
	 * 
	 * @return total electrostatic energy in the Universe in kcal/mol
	 */
	public double getEEPotentialEnergy() {
		double EEPotentialEnergy = 0.0;
		this.ees.clear();

		for (Atom atomI : atoms) {
			// Walk the neighbour list and recreate the ee objects
			if (atomI.getNeighbours() != null) {
				for (Atom atomJ : atomI.getNeighbours()) {
					ElectroStatic electroStatic = new ElectroStatic(atomI,
							atomJ, 1.0);

					this.addEE(electroStatic);
				}
			}

		}

		for (ElectroStatic electroStatic : ees) {
			EEPotentialEnergy += electroStatic.getPotentialEnergy();
		}

		return EEPotentialEnergy;
	}

	/**
	 * Returns the total electrostatic energy from 1-4 interactions in the
	 * Universe in kcal/mol
	 * 
	 * @return total electrostatic energy from 1-4 interactions in the Universe
	 *         in kcal/mol
	 */
	public double get14EEPotentialEnergy() {
		double EE14PotentialEnergy = 0.0;
		double scee = 1.2;
		this.ees14.clear();

		for (Dihedral dihedral : dihedrals) {

			if (dihedral.isContributeTo14NonBondedTerms()
					&& !(dihedral.isImproper())) {

				Atom atomI = dihedral.getAtomI();
				Atom atomL = dihedral.getAtomL();

				ElectroStatic electroStatic = new ElectroStatic(atomI, atomL, scee);

				this.addEE14(electroStatic);

			}

		}

		for (ElectroStatic electroStatic : ees14) {
			EE14PotentialEnergy += electroStatic.getPotentialEnergy() ;
		}

		return EE14PotentialEnergy;
	}

	public void evaluateForce() {
		// TODO Rework into the same form as getPotentialEnergy

		for (Bond bond : bonds) {
			bond.evaluateForce();
		}

		for (Angle angle : angles) {
			angle.evaluateForce();
		}

		for (Dihedral dihedral : dihedrals) {
			dihedral.evaluateForce();
		}
//		
//		for (LJTwelveSix vdw : vdws) {
//			vdw.evaluateForce();
//		}
//		
//		for (LJTwelveSix vdw14 : vdws14) {
//			vdw14.evaluateForce();
//		}
//		
//		for (ElectroStatic ee : ees) {
//			ee.evaluateForce();
//		}
//				
//		for (ElectroStatic ee14 : ees14) {
//			ee14.evaluateForce();
//		}
		
	}

	public void setKineticEnergy(double kineticalEnergy) {
		this.kineticEnergy = kineticalEnergy;
	}

	public double getKineticEnergy() {
		return kineticEnergy;
	}

	public void addAtom(Atom atom) {
		this.atoms.add(atom);
	}

	public Atom getAtom(int index) {
		return this.atoms.get(index);
	}
	
	public ArrayList<Atom> getAtoms() {
		return this.atoms;
	}

	public void addBond(Bond bond) {
		this.bonds.add(bond);

	}

	public Bond getBond(int index) {
		return this.bonds.get(index);

	}

	public void addAngle(Angle angle) {
		this.angles.add(angle);
	}

	public Angle getAngle(int index) {
		return this.angles.get(index);
	}

	void addDihedral(Dihedral dihedral) {
		this.dihedrals.add(dihedral);
	}

	public Dihedral getDihedral(int index) {
		return this.dihedrals.get(index);
	}

	void addVdw(LJTwelveSix lJTwelveSix) {
		this.vdws.add(lJTwelveSix);
	}

	public LJTwelveSix getVdw(int index) {
		return this.vdws.get(index);
	}

	void addVdw14(LJTwelveSix lJTwelveSix) {
		this.vdws14.add(lJTwelveSix);
	}

	public LJTwelveSix getVdw14(int index) {
		return this.vdws14.get(index);
	}

	public void addEE(ElectroStatic electroStatic) {
		this.ees.add(electroStatic);
	}

	public ElectroStatic getEE(int index) {
		return this.ees.get(index);
	}

	void addEE14(ElectroStatic electroStatic) {
		this.ees14.add(electroStatic);
	}

	public ElectroStatic getEE14(int index) {
		return this.ees14.get(index);
	}

	public void setCutoff(double cutoff) {
		this.cutoff = cutoff;

		// TODO Broken. There is a long dependency here that cannot be met,
		// due to the current program structure,
		// hence this will not be updated correctly.

		// this.calulateNeighbourList();
	}

	public double getCutoff() {
		return cutoff;
	}

	/**
	 * Summary of universe
	 */
	@Override
	public String toString() {

		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");
		result.append("Universe potential energy is ")
				.append(this.getPotentialEnergy()).append(" kcal/mol")
				.append(NEW_LINE);
		result.append("Universe bond energy is ")
				.append(this.getBondPotentialEnergy()).append(" kcal/mol")
				.append(NEW_LINE);
		result.append("Universe angle energy is ")
				.append(this.getAnglePotentialEnergy()).append(" kcal/mol")
				.append(NEW_LINE);
		result.append("Universe dihedral energy is ")
				.append(this.getDihedralPotentialEnergy()).append(" kcal/mol")
				.append(NEW_LINE);
		result.append("Universe Vdw potential energy is ")
				.append(this.getVdwPotentialEnergy()).append(" kcal/mol")
				.append(NEW_LINE);
		result.append("Universe 14Vdw potential energy is ")
				.append(this.get14VdwPotentialEnergy()).append(" kcal/mol")
				.append(NEW_LINE);
		result.append("Universe ee potential energy is ")
				.append(this.getEEPotentialEnergy()).append(" kcal/mol")
				.append(NEW_LINE);
		result.append("Universe 14ee potential energy is ")
				.append(this.get14EEPotentialEnergy()).append(" kcal/mol")
				.append(NEW_LINE);

		return result.toString();
	}

	/**
	 * Dump all energy terms in the Universe
	 */
	public void dumpAllterms() {

		System.out.println("");
		System.out.println("==============================");
		System.out.println("ATOMS" + " (" + this.atoms.size() + " terms)");
		System.out.println("==============================");

		for (Atom atom : this.atoms) {
			System.out.println(atom);

		}

		System.out.println("");
		System.out.println("==============================");
		System.out.println("BONDS" + " (" + this.bonds.size() + " terms)");
		System.out.println("==============================");

		for (Bond bond : this.bonds) {
			System.out.println(bond);
		}

		System.out.println("");
		System.out.println("==============================");
		System.out.println("ANGLES" + " (" + this.angles.size() + " terms)");
		System.out.println("==============================");

		for (Angle angle : this.angles) {
			System.out.println(angle);

		}

		System.out.println("");
		System.out.println("==============================");
		System.out.println("DIHEDRALS" + " (" + this.dihedrals.size()
				+ " terms)");
		System.out.println("==============================");

		for (Dihedral dihedral : this.dihedrals) {
			System.out.println(dihedral);

		}

		System.out.println("");
		System.out.println("==============================");
		System.out.println("VDW" + " (" + this.vdws.size() + " terms)");
		System.out.println("==============================");

		for (LJTwelveSix lJTwelveSix : this.vdws) {
			System.out.println(lJTwelveSix);

		}

		System.out.println("");
		System.out.println("==============================");
		System.out.println("VDW14" + " (" + this.vdws14.size() + " terms)");
		System.out.println("==============================");

		for (LJTwelveSix lJTwelveSix : this.vdws14) {
			System.out.println(lJTwelveSix);

		}

		System.out.println("");
		System.out.println("==============================");
		System.out.println("EE" + " (" + this.ees.size() + " terms)");
		System.out.println("==============================");

		for (ElectroStatic electrostatic : this.ees) {
			System.out.println(electrostatic);

		}

		System.out.println("");
		System.out.println("==============================");
		System.out.println("EE14" + " (" + this.ees14.size() + " terms)");
		System.out.println("==============================");

		for (ElectroStatic electrostatic : this.ees14) {
			System.out.println(electrostatic);

		}
	}

}
