package name.mjw.jamber;

import name.mjw.jamber.util.ChemicalSymbol;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;

/**
 * Atom object
 * 
 * @author mjw
 * 
 */
public class Atom {

	private int index;
	private int z;
	private double mass;
	private double charge;
	
	/** Atom name as in PDB */
	private String name;
	
	/** AMBER atom type */
	private String amberAtomType;

	/** The atom's van der Waals radius in the 6-12 VDW equation (Angstroms) */
	private double radius6_12VDW;
	/** The atom's van der Waals well depth in the 6-12 VDW equation (kcal/mol) */
	private double wellDepth6_12VDW;

	private Point3d position;

	private Vector3d velocity;
	
	/**
	 * Force is kcal / (A mol)
	 */
	private Vector3d force;

	/**
	 * Nearest neighbours within in a defined cutoff.
	 */
	private ArrayList<Atom> neighbours;

	public ArrayList<Atom> getNeighbours() {
		return neighbours;
	}

	public void setNeighbours(ArrayList<Atom> neighbours) {
		this.neighbours = neighbours;
	}

	/**
	 * Excluded neighbours
	 */
	private ArrayList<Atom> excludedNeighbours;

	public ArrayList<Atom> getExcludedNeighbours() {
		return excludedNeighbours;
	}

	public void setExcludedNeighbours(ArrayList<Atom> excludedNeighbours) {
		this.excludedNeighbours = excludedNeighbours;
	}

	/**
	 * Atom
	 * 
	 * @param position
	 *            is cartesian space.
	 */
	public Atom(Point3d position) {
		this.position = position;
		this.velocity = new Vector3d();
		this.force = new Vector3d();
	}

	/**
	 * Sets the Atomic number of the atom.
	 */
	public void setZ(int z) {

		this.z = z;
	}

	/**
	 * Gets the Atomic number of the atom.
	 */
	public int getZ() {
		return z;
	}

	/**
	 * Sets the mass of the atom.
	 */
	public void setMass(Double mass) {
		this.mass = mass;
	}

	/**
	 * Gets the mass of the atom.
	 */
	public double getMass() {
		return mass;
	}

	/**
	 * Sets the Cartesian position of the atom.
	 */
	public void setPosition(Point3d position) {
		this.position = position;
	}

	/**
	 * Gets the Cartesian position of the atom.
	 */
	public Point3d getPosition() {
		return position;
	}

	/**
	 * Sets the velocity of the atom.
	 */
	public void setVelocity(Vector3d velocity) {
		this.velocity = velocity;
	}

	/**
	 * Gets the velocity of the atom.
	 */
	public Vector3d getVelocity() {
		return velocity;
	}

	/**
	 * Sets the force on an atom, in kcal / (A mol).
	 */
	public void setForce(Vector3d force) {
		this.force = force;
	}

	/**
	 * Gets the force on an atom, kcal / (A mol).
	 */
	public Vector3d getForce() {
		return force;
	}

	/**
	 * Adds another force to the current force on the atom.
	 */
	public void addForce(Vector3d forceToAdd) {
		this.force.x += forceToAdd.x;
		this.force.y += forceToAdd.y;
		this.force.z += forceToAdd.z;
	}

	/**
	 * Subtracts another force from the current force on the atom.
	 */
	public void subForce(Vector3d forceToSub) {
		this.force.x -= forceToSub.x;
		this.force.y -= forceToSub.y;
		this.force.z -= forceToSub.z;
	}

	/**
	 * Sets the charge on the atom.
	 */
	public void setCharge(double charge) {
		this.charge = charge;
	}

	/**
	 * Gets the charge on the atom.
	 */
	public double getCharge() {
		return charge;
	}

	/**
	 * Sets the AMBER atom type
	 */
	public void setAMBERAtomType(String atomType) {
		this.amberAtomType = atomType;
	}

	/**
	 * Gets the AMBER atom type
	 */
	public String getAMBERAtomType() {
		return amberAtomType;
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");
		result.append(this.getClass().getName()).append(" Object {")
				.append(NEW_LINE);
		result.append(" mass :          ").append(mass).append(NEW_LINE);
		result.append(" charge :        ").append(charge).append(NEW_LINE);
		result.append(" name :          ").append(name).append(NEW_LINE);
		result.append(" amberAtomType : ").append(amberAtomType)
				.append(NEW_LINE);
		result.append(" x, y, z :       ").append(position.x).append(" ")
				.append(position.y).append(" ").append(position.z)
				.append(NEW_LINE);
		result.append("}");

		return result.toString();
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	/** Returns the distance of the atom from the atom (Angstroms). */ 
	public double DistanceFrom(Atom atom) {

		Vector3d dist = new Vector3d();
		dist.sub(this.getPosition(), atom.getPosition());

		return dist.length();
	}

	/** Gets the atom's van der Waals radius in the 6-12 VDW equation (Angstroms). */
	public double getRadius6_12VDW() {
		return radius6_12VDW;
	}

	/** Sets the  atom's van der Waals radius in the 6-12 VDW equation (Angstroms). */
	public void setRadius6_12VDW(double ri) {
		this.radius6_12VDW = ri;
	}

	/** Gets the atom's van der Waals well depth in the 6-12 VDW equation (kcal/mol). */
	public double getWellDepth6_12VDW() {
		return wellDepth6_12VDW;
	}

	/** Sets the atom's van der Waals well depth in the 6-12 VDW equation (kcal/mol). */
	public void setWellDepth6_12VDW(double ei) {
		this.wellDepth6_12VDW = ei;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	/** Returns the Chemical symbol of this atom. This is a function of amberAtomType. */
	public String getElement() {
		return ChemicalSymbol.amberAtomTypeToChemicalSymbol(this.amberAtomType);

	}

}
