package name.mjw.jamber.io.amber;

public abstract class AtomIdentifier {

	private String name;

	public String getName() {
		return name;

	}

	void setName(String name) {
		this.name = name;
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");
		result.append(this.getClass().getName()).append(" Object {")
				.append(NEW_LINE);
		result.append("AtomType  :          ").append(name).append(NEW_LINE);

		result.append("}");

		return result.toString();
	}

}
