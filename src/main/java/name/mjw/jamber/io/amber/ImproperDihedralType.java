package name.mjw.jamber.io.amber;

/**
 * Representation of an improper dihedral parameter found in a parm file.
 * 
 * @author mjw
 * 
 */
public final class ImproperDihedralType extends DihedralType {

	public ImproperDihedralType(AtomIdentifier i, AtomIdentifier j,
			AtomIdentifier k, AtomIdentifier l, Integer multiplicity,
			Double barrierHeight, Double phase, Double periodicity) {
		super(i, j, k, l, multiplicity, barrierHeight, phase, periodicity);
	}

}
