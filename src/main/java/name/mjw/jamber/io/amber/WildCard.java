package name.mjw.jamber.io.amber;

final class WildCard extends AtomIdentifier {

	public WildCard() {
		super.setName("X");
	}

	// We want the Wildcard to match anything
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			throw new NullPointerException();
        }

		if (o instanceof AtomType) {
			return true;
		}
        return false;
	}

	@Override
	public int hashCode() {

		return 0;

	}

	@Override
	public String toString() {
		// TODO Make this more portable
		// OpenMM treats an empty atomType as a wildCard
		return "";
	}

}
