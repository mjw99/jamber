package name.mjw.jamber.io.amber;

import com.google.common.base.Objects;

/**
 * AMBER BondType.
 * <p>
 * An abstraction of bond types found within AMBER's Force field parameter file
 * specification <a href="http://ambermd.org/formats.html#parm.dat"> here</a>.
 * 
 * @author mjw
 * 
 */
public final class BondType {

	private AtomIdentifier atomI;
	private AtomIdentifier atomJ;
	private Double forceConstant;
	private Double equilibriumLength;

	public BondType(AtomIdentifier atomI, AtomIdentifier atomJ,
			double forceConstant, double equilibriumDistance) {
		super();
		this.setAtomI(atomI);
		this.setAtomJ(atomJ);
		this.setForceConstant(forceConstant);
		this.setEquilibriumLength(equilibriumDistance);
	}

	public AtomIdentifier getAtomI() {
		return atomI;
	}

	private void setAtomI(AtomIdentifier atomI) {
		this.atomI = atomI;
	}

	public AtomIdentifier getAtomB() {
		return atomJ;
	}

	private void setAtomJ(AtomIdentifier atomJ) {
		this.atomJ = atomJ;
	}

	public Double getForceConstant() {
		return forceConstant;
	}

	private void setForceConstant(Double forceConstant) {
		this.forceConstant = forceConstant;
	}

	public Double getEquilibriumLength() {
		return equilibriumLength;
	}

	private void setEquilibriumLength(Double equilibriumLength) {
		this.equilibriumLength = equilibriumLength;
	}

	/*
	 * We want the BondType to match only if the AtomIdentifier in both the same
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof BondType) {
			final BondType other = (BondType) obj;

			return Objects.equal(atomI.getName(), other.atomI.getName())
					&& Objects.equal(atomJ.getName(), other.atomJ.getName());

		}
		return false;
	}

	@Override
	public int hashCode() {

		return Objects.hashCode(atomI.getName(), atomJ.getName());

	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");
		result.append(this.getClass().getName()).append(" Object {")
				.append(NEW_LINE);

		result.append(
				String.format(" AtomTypes:\t\t\t\t\t%-2s-%-2s",
						atomI.getName(), atomJ.getName())).append(NEW_LINE);

		result.append(" forceConstant (kcal/(mol Angstrom**2)): \t")
				.append(forceConstant).append(NEW_LINE);
		result.append(" equilibriumLength (A): \t\t\t")
				.append(equilibriumLength).append(NEW_LINE);

		result.append("}");

		return result.toString();
	}

}