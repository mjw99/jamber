package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * Better way to parse in an AMBER Prmtop file... still WIP
 * 
 * @author mjw
 */

public class NewPrmtop {

	public enum FlagDescriptor {
		TITLE("20a4"), 
		POINTERS("10i8"), 
		ATOM_NAME("20a4"),
		CHARGE("5E16.8"), 
		MASS("5E16.8"), 
		ATOM_TYPE_INDEX("10I8"), 
		NUMBER_EXCLUDED_ATOMS("10I8"), 
		NONBONDED_PARM_INDEX("10I8"), 
		RESIDUE_LABEL("20a4"), 
		RESIDUE_POINTER("10i8"), 
		BOND_FORCE_CONSTANT("5E16.8"), 
		BOND_EQUIL_VALUE("5E16.8"), 
		ANGLE_FORCE_CONSTANT("5E16.8"), 
		ANGLE_EQUIL_VALUE("5E16.8"), 
		DIHEDRAL_FORCE_CONSTANT("5E16.8"),
		DIHEDRAL_PERIODICITY("5E16.8"),
		DIHEDRAL_PHASE("5E16.8"), 
		SCEE_SCALE_FACTOR("5E16.8"), 
		SCNB_SCALE_FACTOR("5E16.8"),
		// Two missing here
		SOLTY("5E16.8"), 
		LENNARD_JONES_ACOEF("5E16.8"), 
		LENNARD_JONES_BCOEF("5E16.8"),
		BONDS_INC_HYDROGEN("10I8"), 
		BONDS_WITHOUT_HYDROGEN("10I8"), 
		ANGLES_INC_HYDROGEN("10I8"), 
		ANGLES_WITHOUT_HYDROGEN("10I8"), 
		DIHEDRALS_INC_HYDROGEN("10I8"), 
		DIHEDRALS_WITHOUT_HYDROGEN("10I8"),
		EXCLUDED_ATOMS_LIST("10I8"), 
		HBOND_ACOEF("5E16.8"),
		HBOND_BCOEF("5E16.8"), 
		HBCUT("5E16.8"), 
		AMBER_ATOM_TYPE("20a4"), 
		TREE_CHAIN_CLASSIFICATION("20a4"), 
		JOIN_ARRAY("10i8"), 
		IROTAT("10i8"), 
		SOLVENT_POINTERS("3i8"), 
		ATOMS_PER_MOLECULE("10i8"), 
		BOX_DIMENSIONS("5E16.8"), 
		RADII("5E16.8"), 
		SCREEN("5E16.8");

        private static final Logger LOG = Logger.getLogger(NewPrmtop.class);

		/*
		 * The corresponding Fortran format of that flag
		 */
		private final String flagFormat;

		/**
		 * Instantiates a prmtop descriptor.
		 * 
		 * @param flagFormat Expected format for this flag
		 */
		private FlagDescriptor(final String flagFormat) {
			this.flagFormat = flagFormat;
		}

		public String getFlagFormat() {
			return flagFormat;
		}

		public static void main() {

			for (FlagDescriptor expectedFlagList : FlagDescriptor.values())
				System.out.printf("%%FLAG %s\n%%FORMAT %s\n", expectedFlagList,
						expectedFlagList.getFlagFormat());
		}
		
		/*
		 * Parsed form of a Flag
		 */
		@SuppressWarnings("unused")
		private class Unit {

			private final FlagDescriptor type;

			public Unit(final FlagDescriptor type) {
				this.type = type;

			}

		}

		// Example approach to parse a prmtop and barf if it does not find a
		// required FLAG value/
		//
		public static void read(String fileName) throws ParseException,
				IOException {

			BufferedReader in = new BufferedReader(new FileReader(fileName));

			Pattern FlagPattern = Pattern.compile("%FLAG");

			// Skip over Version
			in.readLine();

			String flagName = in.readLine();
			System.out.println(flagName);

			Scanner s = null;

			try {
				s = new Scanner(in);

				s.findInLine(FlagPattern);

				while (s.hasNext()) {
					System.out.println(s.next());

				}

			} finally {
				if (s != null) {
					s.close();
				}
			}

			try {
				// This should fail if the flag does not exist
				@SuppressWarnings("unused")
				FlagDescriptor expectedFlagList = FlagDescriptor
						.valueOf(flagName);

				// Now, examine the format of the flag and check it against what
				// is expected.
				String flagFormat = in.readLine();

                if (!flagFormat.equals(FlagDescriptor.valueOf(flagName)
                        .getFlagFormat())) {
                    //LOG.debug("Unexpected %FLAG:" + flagName);
                    LOG.debug("Unexpected %FLAG:");

                }

				// Proceed...
				// FortranFormat fortranFormat = new FortranFormat(flagFormat);

			} catch (IllegalArgumentException e) {
				// flagName was not the name of a member of the enum
			}

		}

	}

}
