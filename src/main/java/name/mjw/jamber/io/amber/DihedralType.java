package name.mjw.jamber.io.amber;

import com.google.common.base.Objects;

import java.util.ArrayList;

/**
 * AMBER DihedralType.
 * <p>
 * An abstraction of dihedral types found within AMBER's Force field parameter
 * file specification <a href="http://ambermd.org/formats.html#parm.dat">
 * here</a>.
 * 
 * @author mjw
 * 
 */
public abstract class DihedralType {

	DihedralType(AtomIdentifier i, AtomIdentifier j, AtomIdentifier k,
			AtomIdentifier l, Integer multiplicity, Double barrierHeight,
			Double phase, Double periodicity) {

		super();

		this.barrierHeight = new ArrayList<>();
		this.phase = new ArrayList<>();
		this.periodicity = new ArrayList<>();

		this.setI(i);
		this.setAtomJ(j);
		this.setAtomK(k);
		this.setAtomL(l);
		this.setMultiplicity(multiplicity);
		this.setBarrierHeight(barrierHeight);
		this.setPhase(phase);
		this.setPeriodicity(periodicity);

	}

	/**
	 * k--l / i--j
	 * 
	 * Atom i in dihedral term.
	 */
	private AtomIdentifier atomI;
	/**
	 * Atom j in dihedral term.
	 */
	private AtomIdentifier atomJ;
	/**
	 * Atom k in dihedral term.
	 */
	private AtomIdentifier atomK;
	/**
	 * Atom l in dihedral term.
	 */
	private AtomIdentifier atomL;

	/**
	 * The factor by which the torsional barrier is divided. Also know as IDIVF.
	 */
	private int multiplicity;

	/**
	 * Barrier height, also known as PK.
	 */

	private final ArrayList<Double> barrierHeight;

	/**
	 * Phase, the phase shift angle in the torsional function, in degrees.
	 */
	private final ArrayList<Double> phase;

	/**
	 * Number of repeats of the cosine function in the range -180 to 180 degrees
	 * Also known as PN.
	 */
	private final ArrayList<Integer> periodicity;

	public AtomIdentifier getAtomI() {
		return atomI;
	}

	private void setI(AtomIdentifier i) {
		this.atomI = i;
	}

	public AtomIdentifier getAtomJ() {
		return atomJ;
	}

	private void setAtomJ(AtomIdentifier j) {
		this.atomJ = j;
	}

	public AtomIdentifier getAtomK() {
		return atomK;
	}

	private void setAtomK(AtomIdentifier k) {
		this.atomK = k;
	}

	public AtomIdentifier getAtomL() {
		return atomL;
	}

	private void setAtomL(AtomIdentifier l) {
		this.atomL = l;
	}

	public ArrayList<Double> getBarrierHeight() {
		return barrierHeight;
	}

	void setBarrierHeight(double barrierHeight) {
		this.barrierHeight.add(barrierHeight);
	}

	public ArrayList<Double> getPhase() {
		return phase;
	}

	void setPhase(Double phase) {
		this.phase.add(phase);
	}

	public ArrayList<Integer> getPeriodicity() {
		return periodicity;
	}

	void setPeriodicity(Double periodicity) {
		// Always set this to being positive
		this.periodicity.add((int) Math.abs(periodicity));
	}

	public double getMultiplicity() {
		return multiplicity;
	}

	private void setMultiplicity(int multiplicity) {
		this.multiplicity = multiplicity;
	}

	/*
	 * It seems that with all impropers in converting from parm to OpenMMXML:
	 * 
	 * 1) Atomtypes in positions 1 and 3 are swapped
	 * 
	 * 2) Then Atomtypes in positions 2 and 3 are swapped
	 * 
	 * C -CT-N -O 1.1 180. 2. Junmei et al.1999
	 * 
	 * C -CT-N -O => N -CT-C -O => N -C -CT-O
	 * 
	 * <Improper class1="N" class2="C" class3="CT" class4="O" periodicity1="2"
	 * phase1="3.14159265359" k1="4.6024"/>
	 */
	public void reorderForOpenMM() {
		AtomIdentifier temp;

		// positions 1 and 3 are swapped
		temp = atomK;
		atomK = atomI;
		atomI = temp;

		// positions 2 and 3 are swapped
		temp = atomJ;
		atomJ = atomK;
		atomK = temp;

	}

	/*
	 * We want the DihedralType to match only if the AtomIdentifier in both are
	 * the same.
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof DihedralType) {
			final DihedralType other = (DihedralType) obj;

			return Objects.equal(getAtomI().getName(), other.getAtomI().getName())
					&& Objects.equal(getAtomJ().getName(), other.getAtomJ().getName())
					&& Objects.equal(getAtomK().getName(), other.getAtomK().getName())
					&& Objects.equal(getAtomL().getName(), other.getAtomL().getName());

		}
		return false;
	}

	@Override
	public int hashCode() {

		return Objects.hashCode(getAtomI().getName(), getAtomJ().getName(),
				getAtomK().getName(), getAtomL().getName());

	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");
		result.append(this.getClass().getName()).append(" Object {")
				.append(NEW_LINE);

		result.append(
				String.format(" AtomTypes:\t\t\t\t\t%-2s-%-2s-%-2s-%-2s",
						atomI.getName(), atomJ.getName(), atomK.getName(),
						atomL.getName())).append(NEW_LINE);

		for (int i = 0; i < barrierHeight.size(); i++) {
			result.append(" barrierHeight: " + "\t\t\t\t")
					.append(barrierHeight.get(i) * 2).append(NEW_LINE);
			result.append(" phase (radians): " + "\t\t\t\t")
					.append(phase.get(i)).append(NEW_LINE);
			result.append(" periodicity: " + "\t\t\t\t\t")
					.append(periodicity.get(i)).append(NEW_LINE);
		}

		result.append("}");

		return result.toString();
	}

}
