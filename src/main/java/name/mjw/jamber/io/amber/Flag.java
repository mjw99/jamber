package name.mjw.jamber.io.amber;

import java.util.ArrayList;

/**
 * Abstraction of a 
 * <p>
 * %FLAG foo  <p>
 * %FORMAT(bar)<p>
 * pairing in an AMBER prmtop <a
 * href="http://ambermd.org/formats.html#topology"> file </a> and associated
 * data within that region.
 * 
 * @author mjw
 * 
 */
public final class Flag {

	private String name;

	private String format;

	private ArrayList<Object> contents;

	Flag(String name) {
		setName(name);
		contents = new ArrayList<>();

	}

	public String getName() {
		return name;
	}

	void setName(String name) {
		this.name = name.toUpperCase().trim();
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		// FortranFormat needs the format in UPPER case
		this.format = format.toUpperCase().trim();
	}

	public ArrayList<Object> getContents() {
		return contents;
	}

	public void setContents(ArrayList<Object> contents) {
		this.contents = contents;
	}

	public void addContent(Object content) {
		this.contents.add(content);
	}

	@Override
	public String toString() {

		return name;

	}
}
