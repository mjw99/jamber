package name.mjw.jamber.io.amber;

import com.google.common.base.Objects;

/**
 * AMBER AngleType.
 * <p>
 * An abstraction of angle types found within AMBER's Force field parameter file
 * specification <a href="http://ambermd.org/formats.html#parm.dat"> here</a>.
 * 
 * @author mjw
 * 
 */
public final class AngleType {

	private AtomIdentifier atomI;
	private AtomIdentifier atomJ;
	private AtomIdentifier atomK;
	private Double forceConstant;
	private Double equilibriumAngle;

	public AngleType(AtomIdentifier atomI, AtomIdentifier atomJ,
			AtomIdentifier atomK, Double forceConstant, Double equilibriumAngle) {
		super();
		this.setAtomI(atomI);
		this.setAtomJ(atomJ);
		this.setAtomK(atomK);
		this.setForceConstant(forceConstant);
		this.setEquilibriumAngle(equilibriumAngle);
	}

	public AtomIdentifier getAtomI() {
		return atomI;
	}

	private void setAtomI(AtomIdentifier atomI) {
		this.atomI = atomI;
	}

	public AtomIdentifier getAtomJ() {
		return atomJ;
	}

	private void setAtomJ(AtomIdentifier atomJ) {
		this.atomJ = atomJ;
	}

	public AtomIdentifier getAtomK() {
		return atomK;
	}

	private void setAtomK(AtomIdentifier atomK) {
		this.atomK = atomK;
	}

	public Double getForceConstant() {
		return forceConstant;
	}

	private void setForceConstant(Double forceConstant) {
		this.forceConstant = forceConstant;
	}

	public Double getEquilibriumAngle() {
		return equilibriumAngle;
	}

	private void setEquilibriumAngle(Double equilibriumAngle) {
		this.equilibriumAngle = equilibriumAngle;

	}

	/*
	 * We want the AngleType to match only if the AtomIdentifier in both are the
	 * same.
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof AngleType) {
			final AngleType other = (AngleType) obj;

			return Objects.equal(atomI.getName(), other.getAtomI().getName())
					&& Objects.equal(atomJ.getName(), other.getAtomJ()
							.getName())
					&& Objects.equal(atomK.getName(), other.getAtomK()
							.getName());
		}
		return false;
	}

	@Override
	public int hashCode() {

		return Objects.hashCode(atomI.getName(), atomJ.getName(),
				atomK.getName());

	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");
		result.append(this.getClass().getName()).append(" Object {")
				.append(NEW_LINE);

		result.append(
				String.format(" AtomTypes:\t\t\t\t\t%-2s-%-2s-%-2s",
						atomI.getName(), atomJ.getName(), atomK.getName()))
				.append(NEW_LINE);

		result.append(" forceConstant ( kcal/mol/(rad**2) ) : \t\t")
				.append(forceConstant).append(NEW_LINE);
		result.append(" equilibriumAngle (degrees): \t\t\t")
				.append(equilibriumAngle).append(NEW_LINE);

		result.append("}");

		return result.toString();
	}
}