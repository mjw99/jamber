package name.mjw.jamber.io.amber;

import com.google.common.base.Objects;

import javax.vecmath.Point3d;

/**
 * Abstraction of an Atom that is found within a residue an AMBER Lib file.
 * 
 * @author mw529
 * 
 */
public final class Atom {
	private String name;
	private String type;
	private Integer z;
	private Double charge;
	private Integer indexWithinCurrentResidue;

	private Point3d position;

	/**
	 * Atom object used within AMBER's lib format
	 * 
	 * @param name
	 *            PDB name
	 * @param type
	 *            AMBER type
	 * @param z
	 *            Zvalue
	 * @param charge
	 *            Charge
	 */
	public Atom(String name, String type, Integer z, Double charge) {
		setName(name);
		setType(type);
		setZ(z);
		setCharge(charge);

	}

	/**
	 * Atom object used within AMBER's lib format
	 * 
	 * @param name
	 *            PDB name
	 * @param type
	 *            AMBER type
	 * @param charge
	 *            Charge
	 */
	public Atom(String name, String type, Double charge) {
		setName(name);
		setType(type);
		setCharge(charge);

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

		this.name = name;

	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getCharge() {
		return charge;
	}

	public void setCharge(Double charge) {
		this.charge = charge;
	}

	public Point3d getPosition() {
		return position;
	}

	public void setPosition(Point3d position) {
		this.position = position;
	}

	public Integer getZ() {
		return z;
	}

	public void setZ(Integer z) {
		this.z = z;
	}

	@Override
	public String toString() {
		return name;
	}

	public Integer getIndexWithinCurrentResidue() {
		return indexWithinCurrentResidue;
	}

	public void setIndexWithinCurrentResidue(Integer indexWithinCurrentResidue) {
		this.indexWithinCurrentResidue = indexWithinCurrentResidue;
	}

	/*
	 * We want the Atom to match only if the name & charge in both, are the
	 * same.
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof Atom) {
			final Atom other = (Atom) obj;

			return Objects.equal(getName(), other.getName())
					&& Objects.equal(getCharge(), other.getCharge());

		}
		return false;
	}

	@Override
	public int hashCode() {

		return Objects.hashCode(getName());

	}

}