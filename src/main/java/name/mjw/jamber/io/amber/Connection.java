package name.mjw.jamber.io.amber;

public final class Connection {
	private Atom i;
	private Atom j;

	public Connection(Atom i, Atom j) {
		setI(i);
		setJ(j);
	}

	public Atom getI() {
		return i;
	}

	void setI(Atom i) {
		this.i = i;
	}

	public Atom getJ() {
		return j;
	}

	void setJ(Atom j) {
		this.j = j;
	}

	@Override
	public String toString() {
		return i + "-" + j;
	}

}