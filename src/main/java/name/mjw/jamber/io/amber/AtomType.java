package name.mjw.jamber.io.amber;

import com.google.common.base.Objects;

/**
 * AMBER AtomType.
 * <p>
 * An abstraction of atom types found within AMBER's Force field parameter file
 * specification <a href="http://ambermd.org/formats.html#parm.dat"> here</a>.
 * 
 * @author mjw
 * 
 */
public final class AtomType extends AtomIdentifier {

	private Double mass;

	/**
	 * Van der Waal radius in Angstrom.
	 */
	private Double VdwRadius = -1.0;
	/**
	 * Van der Waal well depth in kcal/mol.
	 */
	private Double VdwWellDepth = -1.0;

	private AtomIdentifier SixTwelvePotentialParametersEquivalence;

	public AtomIdentifier getSixTwelvePotentialParametersEquivalence() {
		return SixTwelvePotentialParametersEquivalence;
	}

	public void setSixTwelvePotentialParametersEquivalence(
			AtomIdentifier sixTwelvePotentialParametersEquivalence) {
		SixTwelvePotentialParametersEquivalence = sixTwelvePotentialParametersEquivalence;
	}

	public AtomType(String name, Double mass) {
		super();
		this.setName(name);
		this.setMass(mass);
	}

	public AtomType(String name) {
		super();
		this.setName(name);
	}

	public Double getMass() {
		return mass;
	}

	public void setName(String name) {
		super.setName(name);
	}

	private void setMass(Double mass) {
		this.mass = mass;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public Double getVdwRadius() {
		return VdwRadius;
	}

	public void setVdwRadius(Double vdwRadius) {
		VdwRadius = vdwRadius;
	}

	public Double getVdwWellDepth() {
		return VdwWellDepth;
	}

	public void setVdwWellDepth(Double vdwWellDepth) {
		VdwWellDepth = vdwWellDepth;
	}

	/*
	 * We want the AtomType to match only if the name in both are the same.
	 */
	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof AtomType) {
			final AtomType other = (AtomType) obj;

			return Objects.equal(getName(), other.getName());
		}
		return false;
	}

	@Override
	public int hashCode() {

		return Objects.hashCode(getName());

	}

}
