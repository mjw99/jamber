package name.mjw.jamber.io.amber;

class AtomIdentifierFactory {

	public static AtomIdentifier getAtomIdentifier(String name, double mass) {

		if (name.equals("X") || name.equals(" X")) {

			return new WildCard();

		} else {

			return new AtomType(name, mass);

		}

	}

	public static AtomIdentifier getAtomIdentifier(String name) {

		if (name.equals("X") || name.equals(" X")) {

			return new WildCard();

		} else {

			return new AtomType(name);

		}

	}

}
