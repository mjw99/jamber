package name.mjw.jamber.io.amber;

import java.util.ArrayList;

/**
 * A representation of a residue topology, comprising of {@link Atom}s,
 * {@link Connection}s and {@link Atom}s which link to an external residue.
 * <p>
 * This is the core abstract type representing the contents of a AMBER Lib <a
 * href="http://ambermd.org/doc/OFF_file_format.txt">file</a> and AnteChamber
 * generated Mol2 files.
 * 
 * 
 * @author mw529
 * 
 */
public final class Residue {
	private String name;

	ArrayList<Atom> atoms = new ArrayList<>();
	ArrayList<Connection> connections = new ArrayList<>();
	ArrayList<Atom> externalBonds = new ArrayList<>();

	public Residue(String name) {
		setName(name);

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {

		this.name = name;

	}

	public ArrayList<Atom> getAtoms() {
		return atoms;
	}

	public void setAtoms(ArrayList<Atom> atoms) {
		this.atoms = atoms;
	}

	public void addAtom(Atom atom) {
		this.atoms.add(atom);
	}

	@Override
	public String toString() {

		return getName();
	}

	public ArrayList<Connection> getConnections() {
		return connections;
	}

	public void setConnections(ArrayList<Connection> connection) {
		this.connections = connection;
	}

	public void addConnection(Connection connection) {
		this.connections.add(connection);
	}

	public ArrayList<Atom> getExternalBonds() {
		return externalBonds;
	}

	public void setExternalBonds(ArrayList<Atom> externalBond) {
		this.externalBonds = externalBond;
	}

	public void appendExternalBond(Atom externalBond) {
		this.externalBonds.add(externalBond);
	}
}