package name.mjw.jamber.io.amber;

import com.google.common.collect.HashBasedTable;
import name.mjw.fortranformat.FortranFormat;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * AMBER PARMeter and TOPology file.
 * <p>
 * This is a representation of the AMBER parm file <a
 * href="http://ambermd.org/formats.html#topology">format</a>.
 * 
 * @author mjw
 */
public final class Prmtop {

	public Prmtop() {

	}

	public Prmtop(String fileName) {
		try {
			read(fileName);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}
    }

	private final Logger LOG = Logger.getLogger(Prmtop.class);

	/**
	 * %FLAGs found in the prmtop.
	 */
	private ArrayList<Flag> flagsPresent;

	/**
	 * HashTable map between %FLAGs, %FORMAT and the content read in. This is
	 * used by internal methods for parsing specific FLAGs.
	 */
	private HashBasedTable<String, String, ArrayList<Object>> table;

	private int version;
	private String title;

	private int numberOfAtoms;
	private int numberOfAtomsTypes;
	private int numberOfBondsContainingHydrogen;
	private int numberOfBondsNotContainingHydrogen;
	private int numberOfAnglesContainingHydrogen;
	private int numberOfAnglesNotContainingHydrogen;
	private int numberOfDihedralsContainingHydrogen;
	private int numberOfDihedralsNotContainingHydrogen;
	private int createdByAddles;
	private int numberOfExcludedAtoms;

	private int numberOfResidues;
	private int numberOfConstrainedBonds;
	private int numberOfConstrainedAngles;
	private int numberOfConstrainedDihedrals;
	private int numberOfUniqueBondTypes;
	private int numberOfUniqueAnglesTypes;
	private int numberOfUniqueDihedralsTypes;
	private int numberOfAtomTypesInPrmtop;
	private int numberOf1012HydrogenBondPairTypes;

	private int readInPerturbation;
	private int numberOfBondsToBePerturbed;
	private int numberOfAnglesToBePerturbed;
	private int numberOfDihedralsToBePerturbed;
	private int numberOfBondsWithAtomsInPerturbedGroup;
	private int numberOfAnglesWithAtomsInPerturbedGroup;
	private int numberOfDihedralsWithAtomsInPerturbedGroup;
	private int boxType;
	private int numberOfAtomsInLargestResidue;
	private int cap;
	private int numberOfExtraPoints;
	private int numberPIMDSlices;

	private List<String> atomName = new ArrayList<>();
	private List<Double> charges = new ArrayList<>();
	private List<Double> mass = new ArrayList<>();
	// private final List<Integer> atomicNumber = new ArrayList<Integer>();
	private List<Integer> atomTypeIndex = new ArrayList<>();
	private List<Integer> excludedAtomsIndex = new ArrayList<>();
	private List<Integer> nonbondedParmIndex = new ArrayList<>();
	private List<String> residueLabel = new ArrayList<>();
	private List<Integer> residuePointer = new ArrayList<>();

	private List<Double> bondForceConstant = new ArrayList<>();
	private List<Double> bondEquilValue = new ArrayList<>();

	private List<Double> angleForceConstant = new ArrayList<>();
	private List<Double> angleEquilValue = new ArrayList<>();

	private List<Double> dihedralForceConstant = new ArrayList<>();
	private List<Double> dihedralPeriodicity = new ArrayList<>();
	private List<Double> dihedralPhase = new ArrayList<>();

	private List<Double> solty = new ArrayList<>();

	private List<Double> lennardJonesAcoef = new ArrayList<>();
	private List<Double> lennardJonesBcoef = new ArrayList<>();

	private List<Integer> bondsIncHydrogen = new ArrayList<>();
	private List<Integer> bondsWithoutHydrogen = new ArrayList<>();

	private List<Integer> anglesIncHydrogen = new ArrayList<>();
	private List<Integer> anglesWithoutHydrogen = new ArrayList<>();

	private List<Integer> dihedralsIncHydrogen = new ArrayList<>();
	private List<Integer> dihedralsWithoutHydrogen = new ArrayList<>();

	private List<Integer> excludedAtomsList = new ArrayList<>();

	private List<Double> hbondAcoef = new ArrayList<>();
	private List<Double> hbondBcoef = new ArrayList<>();
	private List<Double> hBCut = new ArrayList<>();

	private List<String> amberAtomType = new ArrayList<>();
	private List<String> treeChainClassification = new ArrayList<>();

	private List<Integer> joinArray = new ArrayList<>();
	private List<Integer> irotat = new ArrayList<>();
	private String radiusSet = "";
	private List<Double> radii = new ArrayList<>();
	private List<Double> screen = new ArrayList<>();

	/*
	 * private final List<Double> SCEE_ScaleFactor = new ArrayList<Double>();
	 * private final List<Double> SCNB_ScaleFactor = new ArrayList<Double>();
	 * 
	 * private final List<Integer> ipol = new ArrayList<Integer>();
	 */

	// Getters and Setters

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}

	public int getNumberOfAtoms() {
		return numberOfAtoms;
	}

	public void setNumberOfAtoms(int numberOfAtoms) {
		this.numberOfAtoms = numberOfAtoms;
	}

	public int getNumberOfAtomsTypes() {
		return numberOfAtomsTypes;
	}

	public void setNumberOfAtomsTypes(int numberOfAtomsTypes) {
		this.numberOfAtomsTypes = numberOfAtomsTypes;
	}

	public int getNumberOfBondsContainingHydrogen() {
		return numberOfBondsContainingHydrogen;
	}

	public void setNumberOfBondsContainingHydrogen(
			int numberOfBondsContainingHydrogen) {
		this.numberOfBondsContainingHydrogen = numberOfBondsContainingHydrogen;
	}

	public int getNumberOfBondsNotContainingHydrogen() {
		return numberOfBondsNotContainingHydrogen;
	}

	public void setNumberOfBondsNotContainingHydrogen(
			int numberOfBondsNotContainingHydrogen) {
		this.numberOfBondsNotContainingHydrogen = numberOfBondsNotContainingHydrogen;
	}

	public int getNumberOfAnglesContainingHydrogen() {
		return numberOfAnglesContainingHydrogen;
	}

	public void setNumberOfAnglesContainingHydrogen(
			int numberOfAnglesContainingHydrogen) {
		this.numberOfAnglesContainingHydrogen = numberOfAnglesContainingHydrogen;
	}

	public int getNumberOfAnglesNotContainingHydrogen() {
		return numberOfAnglesNotContainingHydrogen;
	}

	public void setNumberOfAnglesNotContainingHydrogen(
			int numberOfAnglesNotContainingHydrogen) {
		this.numberOfAnglesNotContainingHydrogen = numberOfAnglesNotContainingHydrogen;
	}

	public int getNumberOfDihedralsContainingHydrogen() {
		return numberOfDihedralsContainingHydrogen;
	}

	public void setNumberOfDihedralsContainingHydrogen(
			int numberOfDihedralsContainingHydrogen) {
		this.numberOfDihedralsContainingHydrogen = numberOfDihedralsContainingHydrogen;
	}

	public int getNumberOfDihedralsNotContainingHydrogen() {
		return numberOfDihedralsNotContainingHydrogen;
	}

	public void setNumberOfDihedralsNotContainingHydrogen(
			int numberOfDihedralsNotContainingHydrogen) {
		this.numberOfDihedralsNotContainingHydrogen = numberOfDihedralsNotContainingHydrogen;
	}

	public int getCreatedByAddles() {
		return createdByAddles;
	}

	public void setCreatedByAddles(int createdByAddles) {
		this.createdByAddles = createdByAddles;
	}

	public int getNumberOfExcludedAtoms() {
		return numberOfExcludedAtoms;
	}

	public void setNumberOfExcludedAtoms(int numberOfExcludedAtoms) {
		this.numberOfExcludedAtoms = numberOfExcludedAtoms;
	}

	public int getNumberOfResidues() {
		return numberOfResidues;
	}

	public void setNumberOfResidues(int numberOfResidues) {
		this.numberOfResidues = numberOfResidues;
	}

	public int getNumberOfConstrainedBonds() {
		return numberOfConstrainedBonds;
	}

	public void setNumberOfConstrainedBonds(int numberOfConstrainedBonds) {
		this.numberOfConstrainedBonds = numberOfConstrainedBonds;
	}

	public int getNumberOfConstrainedAngles() {
		return numberOfConstrainedAngles;
	}

	public void setNumberOfConstrainedAngles(int numberOfConstrainedAngles) {
		this.numberOfConstrainedAngles = numberOfConstrainedAngles;
	}

	public int getNumberOfConstrainedDihedrals() {
		return numberOfConstrainedDihedrals;
	}

	public void setNumberOfConstrainedDihedrals(int numberOfConstrainedDihedrals) {
		this.numberOfConstrainedDihedrals = numberOfConstrainedDihedrals;
	}

	public int getNumberOfUniqueBondTypes() {
		return numberOfUniqueBondTypes;
	}

	public void setNumberOfUniqueBondTypes(int numberOfUniqueBondTypes) {
		this.numberOfUniqueBondTypes = numberOfUniqueBondTypes;
	}

	public int getNumberOfUniqueAnglesTypes() {
		return numberOfUniqueAnglesTypes;
	}

	public void setNumberOfUniqueAnglesTypes(int numberOfUniqueAnglesTypes) {
		this.numberOfUniqueAnglesTypes = numberOfUniqueAnglesTypes;
	}

	public int getNumberOfUniqueDihedralsTypes() {
		return numberOfUniqueDihedralsTypes;
	}

	public void setNumberOfUniqueDihedralsTypes(int numberOfUniqueDihedralsTypes) {
		this.numberOfUniqueDihedralsTypes = numberOfUniqueDihedralsTypes;
	}

	public int getNumberOfAtomTypesInPrmtop() {
		return numberOfAtomTypesInPrmtop;
	}

	public void setNumberOfAtomTypesInPrmtop(int numberOfAtomTypesInPrmtop) {
		this.numberOfAtomTypesInPrmtop = numberOfAtomTypesInPrmtop;
	}

	public int getNumberOf1012HydrogenBondPairTypes() {
		return numberOf1012HydrogenBondPairTypes;
	}

	public void setNumberOf1012HydrogenBondPairTypes(
			int numberOf1012HydrogenBondPairTypes) {
		this.numberOf1012HydrogenBondPairTypes = numberOf1012HydrogenBondPairTypes;
	}

	public int getReadInPerturbation() {
		return readInPerturbation;
	}

	public void setReadInPerturbation(int readInPerturbation) {
		this.readInPerturbation = readInPerturbation;
	}

	public int getNumberOfBondsToBePerturbed() {
		return numberOfBondsToBePerturbed;
	}

	public void setNumberOfBondsToBePerturbed(int numberOfBondsToBePerturbed) {
		this.numberOfBondsToBePerturbed = numberOfBondsToBePerturbed;
	}

	public int getNumberOfAnglesToBePerturbed() {
		return numberOfAnglesToBePerturbed;
	}

	public void setNumberOfAnglesToBePerturbed(int numberOfAnglesToBePerturbed) {
		this.numberOfAnglesToBePerturbed = numberOfAnglesToBePerturbed;
	}

	public int getNumberOfDihedralsToBePerturbed() {
		return numberOfDihedralsToBePerturbed;
	}

	public void setNumberOfDihedralsToBePerturbed(
			int numberOfDihedralsToBePerturbed) {
		this.numberOfDihedralsToBePerturbed = numberOfDihedralsToBePerturbed;
	}

	public int getNumberOfBondsWithAtomsInPerturbedGroup() {
		return numberOfBondsWithAtomsInPerturbedGroup;
	}

	public void setNumberOfBondsWithAtomsInPerturbedGroup(
			int numberOfBondsWithAtomsInPerturbedGroup) {
		this.numberOfBondsWithAtomsInPerturbedGroup = numberOfBondsWithAtomsInPerturbedGroup;
	}

	public int getNumberOfAnglesWithAtomsInPerturbedGroup() {
		return numberOfAnglesWithAtomsInPerturbedGroup;
	}

	public void setNumberOfAnglesWithAtomsInPerturbedGroup(
			int numberOfAnglesWithAtomsInPerturbedGroup) {
		this.numberOfAnglesWithAtomsInPerturbedGroup = numberOfAnglesWithAtomsInPerturbedGroup;
	}

	public int getNumberOfDihedralsWithAtomsInPerturbedGroup() {
		return numberOfDihedralsWithAtomsInPerturbedGroup;
	}

	public void setNumberOfDihedralsWithAtomsInPerturbedGroup(
			int numberOfDihedralsWithAtomsInPerturbedGroup) {
		this.numberOfDihedralsWithAtomsInPerturbedGroup = numberOfDihedralsWithAtomsInPerturbedGroup;
	}

	public int getBoxType() {
		return boxType;
	}

	public void setBoxType(int boxType) {
		this.boxType = boxType;
	}

	public int getNumberOfAtomsInLargestResidue() {
		return numberOfAtomsInLargestResidue;
	}

	public void setNumberOfAtomsInLargestResidue(
			int numberOfAtomsInLargestResidue) {
		this.numberOfAtomsInLargestResidue = numberOfAtomsInLargestResidue;
	}

	public int getCap() {
		return cap;
	}

	public void setCap(int cap) {
		this.cap = cap;
	}

	public int getNumberOfExtraPoints() {
		return numberOfExtraPoints;
	}

	public void setNumberOfExtraPoints(int numberOfExtraPoints) {
		this.numberOfExtraPoints = numberOfExtraPoints;
	}

	public int getNumberPIMDSlices() {
		return numberPIMDSlices;
	}

	public void setNumberPIMDSlices(int numberPIMDSlices) {
		this.numberPIMDSlices = numberPIMDSlices;
	}

	public List<String> getAtomName() {
		return atomName;
	}

	public void setAtomName(List<String> atomName) {
		this.atomName = atomName;
	}

	public List<Double> getCharges() {
		return charges;
	}

	public void setCharges(List<Double> charges) {
		this.charges = charges;
	}

	public List<Double> getMass() {
		return mass;
	}

	public void setMass(List<Double> mass) {
		this.mass = mass;
	}

	public List<Integer> getAtomTypeIndex() {
		return atomTypeIndex;
	}

	public void setAtomTypeIndex(List<Integer> atomTypeIndex) {
		this.atomTypeIndex = atomTypeIndex;
	}

	public List<Integer> getExcludedAtomsIndex() {
		return excludedAtomsIndex;
	}

	public void setExcludedAtomsIndex(List<Integer> excludedAtomsIndex) {
		this.excludedAtomsIndex = excludedAtomsIndex;
	}

	public List<Integer> getNonbondedParmIndex() {
		return nonbondedParmIndex;
	}

	public void setNonbondedParmIndex(List<Integer> nonbondedParmIndex) {
		this.nonbondedParmIndex = nonbondedParmIndex;
	}

	public List<String> getResidueLabel() {
		return residueLabel;
	}

	public void setResidueLabel(List<String> residueLabel) {
		this.residueLabel = residueLabel;
	}

	public List<Integer> getResiduePointer() {
		return residuePointer;
	}

	public void setResiduePointer(List<Integer> residuePointer) {
		this.residuePointer = residuePointer;
	}

	public List<Double> getBondForceConstant() {
		return bondForceConstant;
	}

	public void setBondForceConstant(List<Double> bondForceConstant) {
		this.bondForceConstant = bondForceConstant;
	}

	public List<Double> getBondEquilValue() {
		return bondEquilValue;
	}

	public void setBondEquilValue(List<Double> bondEquilValue) {
		this.bondEquilValue = bondEquilValue;
	}

	public List<Double> getAngleForceConstant() {
		return angleForceConstant;
	}

	public void setAngleForceConstant(List<Double> angleForceConstant) {
		this.angleForceConstant = angleForceConstant;
	}

	public List<Double> getAngleEquilValue() {
		return angleEquilValue;
	}

	public void setAngleEquilValue(List<Double> angleEquilValue) {
		this.angleEquilValue = angleEquilValue;
	}

	public List<Double> getDihedralForceConstant() {
		return dihedralForceConstant;
	}

	public void setDihedralForceConstant(List<Double> dihedralForceConstant) {
		this.dihedralForceConstant = dihedralForceConstant;
	}

	public List<Double> getDihedralPeriodicity() {
		return dihedralPeriodicity;
	}

	public void setDihedralPeriodicity(List<Double> dihedralPeriodicity) {
		this.dihedralPeriodicity = dihedralPeriodicity;
	}

	public List<Double> getDihedralPhase() {
		return dihedralPhase;
	}

	public void setDihedralPhase(List<Double> dihedralPhase) {
		this.dihedralPhase = dihedralPhase;
	}

	public List<Double> getSolty() {
		return solty;
	}

	public void setSolty(List<Double> solty) {
		this.solty = solty;
	}

	public List<Double> getLennardJonesAcoef() {
		return lennardJonesAcoef;
	}

	public void setLennardJonesAcoef(List<Double> lennardJonesAcoef) {
		this.lennardJonesAcoef = lennardJonesAcoef;
	}

	public List<Double> getLennardJonesBcoef() {
		return lennardJonesBcoef;
	}

	public void setLennardJonesBcoef(List<Double> lennardJonesBcoef) {
		this.lennardJonesBcoef = lennardJonesBcoef;
	}

	public List<Integer> getBondsIncHydrogen() {
		return bondsIncHydrogen;
	}

	public void setBondsIncHydrogen(List<Integer> bondsIncHydrogen) {
		this.bondsIncHydrogen = bondsIncHydrogen;
	}

	public List<Integer> getBondsWithoutHydrogen() {
		return bondsWithoutHydrogen;
	}

	public void setBondsWithoutHydrogen(List<Integer> bondsWithoutHydrogen) {
		this.bondsWithoutHydrogen = bondsWithoutHydrogen;
	}

	public List<Integer> getAnglesIncHydrogen() {
		return anglesIncHydrogen;
	}

	public void setAnglesIncHydrogen(List<Integer> anglesIncHydrogen) {
		this.anglesIncHydrogen = anglesIncHydrogen;
	}

	public List<Integer> getAnglesWithoutHydrogen() {
		return anglesWithoutHydrogen;
	}

	public void setAnglesWithoutHydrogen(List<Integer> anglesWithoutHydrogen) {
		this.anglesWithoutHydrogen = anglesWithoutHydrogen;
	}

	public List<Integer> getDihedralsIncHydrogen() {
		return dihedralsIncHydrogen;
	}

	public void setDihedralsIncHydrogen(List<Integer> dihedralsIncHydrogen) {
		this.dihedralsIncHydrogen = dihedralsIncHydrogen;
	}

	public List<Integer> getDihedralsWithoutHydrogen() {
		return dihedralsWithoutHydrogen;
	}

	public void setDihedralsWithoutHydrogen(
			List<Integer> dihedralsWithoutHydrogen) {
		this.dihedralsWithoutHydrogen = dihedralsWithoutHydrogen;
	}

	public List<Integer> getExcludedAtomsList() {
		return excludedAtomsList;
	}

	public void setExcludedAtomsList(List<Integer> excludedAtomsList) {
		this.excludedAtomsList = excludedAtomsList;
	}

	public List<Double> getHbondAcoef() {
		return hbondAcoef;
	}

	public void setHbondAcoef(List<Double> hbondAcoef) {
		this.hbondAcoef = hbondAcoef;
	}

	public List<Double> getHbondBcoef() {
		return hbondBcoef;
	}

	public void setHbondBcoef(List<Double> hbondBcoef) {
		this.hbondBcoef = hbondBcoef;
	}

	public List<Double> gethBCut() {
		return hBCut;
	}

	public void sethBCut(List<Double> hBCut) {
		this.hBCut = hBCut;
	}

	public List<String> getAmberAtomType() {
		return amberAtomType;
	}

	public void setAmberAtomType(List<String> amberAtomType) {
		this.amberAtomType = amberAtomType;
	}

	public List<String> getTreeChainClassification() {
		return treeChainClassification;
	}

	public void setTreeChainClassification(List<String> treeChainClassification) {
		this.treeChainClassification = treeChainClassification;
	}

	public List<Integer> getJoinArray() {
		return joinArray;
	}

	public void setJoinArray(List<Integer> joinArray) {
		this.joinArray = joinArray;
	}

	public List<Integer> getIrotat() {
		return irotat;
	}

	public void setIrotat(List<Integer> irotat) {
		this.irotat = irotat;
	}

	public String getRadiusSet() {
		return radiusSet;
	}

	void setRadiusSet(String radiusSet) {
		this.radiusSet = radiusSet;
	}

	public List<Double> getRadii() {
		return radii;
	}

	public void setRadii(List<Double> radii) {
		this.radii = radii;
	}

	public List<Double> getScreen() {
		return screen;
	}

	public void setScreen(List<Double> screen) {
		this.screen = screen;
	}

	private void parseTitle20A4(ArrayList<Object> objects) {

		String title = "";
		// Concatenate the strings

		for (Object object : objects) {

			title += (((String) object));

		}

		LOG.debug("parseTitle20A4(): " + title);

		setTitle(title);

	}

	private void parsePointers10I8(ArrayList<Object> objects) {

		numberOfAtoms = (Integer) objects.get(0);
		numberOfAtomsTypes = (Integer) objects.get(1);
		numberOfBondsContainingHydrogen = (Integer) objects.get(2);
		numberOfBondsNotContainingHydrogen = (Integer) objects.get(3);
		numberOfAnglesContainingHydrogen = (Integer) objects.get(4);
		numberOfAnglesNotContainingHydrogen = (Integer) objects.get(5);
		numberOfDihedralsContainingHydrogen = (Integer) objects.get(6);
		numberOfDihedralsNotContainingHydrogen = (Integer) objects.get(7);
		// NHPARM : currently not used
		createdByAddles = (Integer) objects.get(9);

		numberOfExcludedAtoms = (Integer) objects.get(10);
		numberOfResidues = (Integer) objects.get(11);
		numberOfConstrainedBonds = (Integer) objects.get(12);
		numberOfConstrainedAngles = (Integer) objects.get(13);
		numberOfConstrainedDihedrals = (Integer) objects.get(14);
		numberOfUniqueBondTypes = (Integer) objects.get(15);
		numberOfUniqueAnglesTypes = (Integer) objects.get(16);
		numberOfUniqueDihedralsTypes = (Integer) objects.get(17);
		numberOfAtomTypesInPrmtop = (Integer) objects.get(18);
		numberOf1012HydrogenBondPairTypes = (Integer) objects.get(19);

		readInPerturbation = (Integer) objects.get(20);
		numberOfBondsToBePerturbed = (Integer) objects.get(21);
		numberOfAnglesToBePerturbed = (Integer) objects.get(22);
		numberOfDihedralsToBePerturbed = (Integer) objects.get(23);
		numberOfBondsWithAtomsInPerturbedGroup = (Integer) objects.get(24);
		numberOfAnglesWithAtomsInPerturbedGroup = (Integer) objects.get(25);
		numberOfDihedralsWithAtomsInPerturbedGroup = (Integer) objects.get(26);
		boxType = (Integer) objects.get(27);
		numberOfAtomsInLargestResidue = (Integer) objects.get(28);
		cap = (Integer) objects.get(29);

		// May or may not be present
		// numberOfExtraPoints = (Integer) objects.get(30);
		// numberPIMDSlices = (Integer) objects.get(31);

		LOG.debug("parsePointers10I8(): ");

	}

	private void parseAtomName20A4(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				atomName.add((String) object);
			}
		}

		LOG.debug("parseAtomName20A4(): " + atomName);

	}

	private void parseCharge5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				charges.add((Double) object);
			}
		}

		LOG.debug("parseCharge5E16_8(): " + charges);

	}

	private void parseMass5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				mass.add((Double) object);
			}
		}

		LOG.debug("parseMass5E16_8(): " + mass);

	}

	/*
	 * private void parseAtomicNumber10I8(ArrayList<Object> objects) {
	 * 
	 * for (Object object : objects) { if (object != null) {
	 * atomicNumber.add((Integer) object); } }
	 * 
	 * LOG.debug("parseAtomicNumber10I8(): " + atomicNumber); }
	 */

	private void parseAtomicTypeIndex10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				atomTypeIndex.add((Integer) object);
			}
		}

		LOG.debug("parseAtomicTypeIndex10I8(): " + atomTypeIndex);
	}

	private void parseNumberExcludedAtoms10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				excludedAtomsIndex.add((Integer) object);
			}
		}

		LOG.debug("parseNumberExcludedAtoms10I8(): " + excludedAtomsIndex);
	}

	private void parseNonbondedParmIndex10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				nonbondedParmIndex.add((Integer) object);
			}
		}

		LOG.debug("parseNonbondedParmIndex10I8(): " + nonbondedParmIndex);
	}

	private void parseResidueLabel20A4(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				residueLabel.add((String) object);
			}
		}

		LOG.debug("parseResidueLabel20A4(): " + residueLabel);

	}

	private void parseResiduePointer10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				residuePointer.add((Integer) object);
			}
		}

		LOG.debug("parseResiduePointer10I8(): " + residuePointer);
	}

	private void parseBondForceConstant5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				bondForceConstant.add((Double) object);
			}
		}

		LOG.debug("parseBondForceConstant5E16_8(): " + bondForceConstant);

	}

	private void parseBondEquilValue5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				bondEquilValue.add((Double) object);
			}
		}

		LOG.debug("parseBondEquilValue5E16_8(): " + bondEquilValue);

	}

	private void parseAngleForceConstant5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				angleForceConstant.add((Double) object);
			}
		}

		LOG.debug("parseAngleForceConstant5E16_8(): " + angleForceConstant);

	}

	private void parseAngleEquilValue5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				angleEquilValue.add((Double) object);
			}
		}

		LOG.debug("parseAngleEquilValue5E16_8(): " + angleEquilValue);

	}

	private void parseDihedralForceConstant5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				dihedralForceConstant.add((Double) object);
			}
		}

		LOG.debug("parseDihedralForceConstant5E16_8(): "
				+ dihedralForceConstant);

	}

	private void parseDihedralPeriodicity5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				dihedralPeriodicity.add((Double) object);
			}
		}

		LOG.debug("parseDihedralPeriodicity5E16_8(): " + dihedralPeriodicity);

	}

	private void parseDihedralPhase5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				dihedralPhase.add((Double) object);
			}
		}

		LOG.debug("parseDihedralPhase5E16_8(): " + dihedralPhase);

	}

	/*
	 * private void parseSCEE_ScaleFactor5E16_8(ArrayList<Object> objects) {
	 * 
	 * for (Object object : objects) { if (object != null) {
	 * SCEE_ScaleFactor.add((Double) object); } }
	 * 
	 * LOG.debug("parseSCEE_ScaleFactor5E16_8(): " + SCEE_ScaleFactor);
	 * 
	 * }
	 */

	/*
	 * private void parseSCNB_ScaleFactor5E16_8(ArrayList<Object> objects) {
	 * 
	 * for (Object object : objects) { if (object != null) {
	 * SCNB_ScaleFactor.add((Double) object); } }
	 * 
	 * LOG.debug("parseSCNB_ScaleFactor5E16_8(): " + SCNB_ScaleFactor);
	 * 
	 * }
	 */

	private void parseSolty5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				solty.add((Double) object);
			}
		}

		LOG.debug("parseSolty5E16_8(): " + solty);

	}

	private void parseLennardJonesAcoef5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				lennardJonesAcoef.add((Double) object);
			}
		}

		LOG.debug("parseLennardJonesAcoef5E16_8(): " + lennardJonesAcoef);

	}

	private void parseLennardJonesBcoef5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				lennardJonesBcoef.add((Double) object);
			}
		}

		LOG.debug("parseLennardJonesBcoef5E16_8(): " + lennardJonesBcoef);

	}

	private void parseBondsIncHydrogen10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				bondsIncHydrogen.add((Integer) object);
			}
		}

		LOG.debug("parseBondsIncHydrogen10I8(): " + bondsIncHydrogen);
	}

	private void parseBondsWithoutHydrogen10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				bondsWithoutHydrogen.add((Integer) object);
			}
		}

		LOG.debug("parseBondsWithouHydrogen10I8(): " + bondsWithoutHydrogen);
	}

	private void parseAnglesIncHydrogen10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				anglesIncHydrogen.add((Integer) object);
			}
		}

		LOG.debug("parseAnglesIncHydrogen10I8(): " + anglesIncHydrogen);
	}

	private void parseAnglesWithoutHydrogen10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				anglesWithoutHydrogen.add((Integer) object);
			}
		}

		LOG.debug("parseAnglesWithoutHydrogen10I8(): " + anglesWithoutHydrogen);
	}

	private void parseDihedralsIncHydrogen10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				dihedralsIncHydrogen.add((Integer) object);
			}
		}

		LOG.debug("parseDihedralsIncHydrogen10I8(): " + dihedralsIncHydrogen);
	}

	private void parseDihedralsWithoutHydrogen10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				dihedralsWithoutHydrogen.add((Integer) object);
			}
		}

		LOG.debug("parseDihedralsWithoutHydrogen10I8(): "
				+ dihedralsWithoutHydrogen);
	}

	private void parseExcludedAtomsList10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				excludedAtomsList.add((Integer) object);
			}
		}

		LOG.debug("parseExcludedAtomsList10I8(): " + excludedAtomsList);
	}

	private void parseHbondAcoef5E16_8(ArrayList<Object> objects) {

		if (objects != null) {

			for (Object object : objects) {
				if (object != null) {
					LOG.debug("Attempting to add object" + object);
					hbondAcoef.add((Double) object);
				}
			}
		}

		LOG.debug("parseHbondAcoef5E16_8(): " + hbondAcoef);

	}

	private void parseHbondBcoef5E16_8(ArrayList<Object> objects) {

		if (objects != null) {
			for (Object object : objects) {
				if (object != null) {
					hbondBcoef.add((Double) object);
				}
			}
		}

		LOG.debug("parseHbondBcoef5E16_8(): " + hbondBcoef);

	}

	private void parseHbondCut5E16_8(ArrayList<Object> objects) {

		if (objects != null) {
			for (Object object : objects) {
				if (object != null) {
					hBCut.add((Double) object);
				}
			}
		}

		LOG.debug("parseHbondCut5E16_8(): " + hBCut);

	}

	private void parseAmberAtomType20A4(ArrayList<Object> objects) {

		LOG.debug("parseAmberAtomType20A4(): objects passed " + objects);
		for (Object object : objects) {
			if (object != null) {
				amberAtomType.add((String) object);
			}
		}

		LOG.debug("parseAmberAtomType20A4(): " + amberAtomType);

	}

	private void parseTreeChainClassification20A4(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				treeChainClassification.add((String) object);
			}
		}

		LOG.debug("parseTreeChainClassification20A4(): "
				+ treeChainClassification);

	}

	private void parseJoinArray10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				joinArray.add((Integer) object);
			}
		}

		LOG.debug("parseJoinArray10I8(): " + joinArray);
	}

	private void parseIrotat10I8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				irotat.add((Integer) object);
			}
		}

		LOG.debug("parseIrotat10I8(): " + irotat);
	}

	private void parseRadiusSet1A80(ArrayList<Object> objects) {

		String radiusSet = "";
		// Concatenate the strings

		for (Object object : objects) {

			radiusSet += (((String) object));

		}

		LOG.debug("parseRadiusSet1A80(): " + radiusSet);

		setRadiusSet(radiusSet);

	}

	private void parseRadii5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				radii.add((Double) object);
			}
		}

		LOG.debug("parseRadii5E16_8(): " + radii);

	}

	private void parseScreen5E16_8(ArrayList<Object> objects) {

		for (Object object : objects) {
			if (object != null) {
				screen.add((Double) object);
			}
		}

		LOG.debug("parseScreen5E16_8(): " + screen);

	}

	/*
	 * private void parseIpol1I8(ArrayList<Object> objects) {
	 * 
	 * for (Object object : objects) { if (object != null) { ipol.add((Integer)
	 * object); } }
	 * 
	 * LOG.debug("parseIpol1I8(): " + ipol);
	 * 
	 * }
	 */

	/**
	 * Read in a prmtop file.
	 * 
	 * @param fileName
	 *            prmtop filename.
	 * @throws ParseException
	 * @throws IOException
	 */
	public void read(String fileName) throws ParseException, IOException {

		// Harvest all %FLAGs in the prmtop file
		flagsPresent = getAllFlags(fileName);

		// Now, for a given list of %FLAGs, read them in
		for (Flag flag : flagsPresent) {
			readFlag(flag, fileName);
			LOG.debug("FLAG contents are:");
			LOG.debug(flag.getContents());
		}

		// Generate mapping between (Flag.name, Flag.format)-->Contents
		generateHashMap(flagsPresent);

		/*
		 * Now, for each %FLAG, furnish the prmtop variables with their
		 * respective FLAG contents.
		 * 
		 * This is in-place because the order or existence of %FLAGs cannot be
		 * guaranteed in a prmtop file.
		 * 
		 * TODO call respective parse methods as a function of %FLAGS in
		 * flagsPresent
		 */
		parseTitle20A4(table.get("TITLE", "(20A4)"));

		parsePointers10I8(table.get("POINTERS", "(10I8)"));

		parseAtomName20A4(table.get("ATOM_NAME", "(20A4)"));

		parseCharge5E16_8(table.get("CHARGE", "(5E16.8)"));

		// Only in AMBER12 generated prmtops
		// parseAtomicNumber10I8(table.get("ATOMIC_NUMBER", "(10I8)"));

		parseMass5E16_8(table.get("MASS", "(5E16.8)"));

		parseAtomicTypeIndex10I8(table.get("ATOM_TYPE_INDEX", "(10I8)"));

		parseNumberExcludedAtoms10I8(table.get("NUMBER_EXCLUDED_ATOMS",
				"(10I8)"));

		parseNonbondedParmIndex10I8(table.get("NONBONDED_PARM_INDEX", "(10I8)"));

		parseResidueLabel20A4(table.get("RESIDUE_LABEL", "(20A4)"));

		parseResiduePointer10I8(table.get("RESIDUE_POINTER", "(10I8)"));

		parseBondForceConstant5E16_8(table.get("BOND_FORCE_CONSTANT",
				"(5E16.8)"));

		parseBondEquilValue5E16_8(table.get("BOND_EQUIL_VALUE", "(5E16.8)"));

		parseAngleForceConstant5E16_8(table.get("ANGLE_FORCE_CONSTANT",
				"(5E16.8)"));

		parseAngleEquilValue5E16_8(table.get("ANGLE_EQUIL_VALUE", "(5E16.8)"));

		parseDihedralForceConstant5E16_8(table.get("DIHEDRAL_FORCE_CONSTANT",
				"(5E16.8)"));

		parseDihedralPeriodicity5E16_8(table.get("DIHEDRAL_PERIODICITY",
				"(5E16.8)"));

		parseDihedralPhase5E16_8(table.get("DIHEDRAL_PHASE", "(5E16.8)"));

		// Only in AMBER12 generated prmtops
		// parseSCEE_ScaleFactor5E16_8(table.get("SCEE_SCALE_FACTOR",
		// "(5E16.8)"));
		// parseSCNB_ScaleFactor5E16_8(table.get("SCNB_SCALE_FACTOR",
		// "(5E16.8)"));

		parseSolty5E16_8(table.get("SOLTY", "(5E16.8)"));

		parseLennardJonesAcoef5E16_8(table.get("LENNARD_JONES_ACOEF",
				"(5E16.8)"));

		parseLennardJonesBcoef5E16_8(table.get("LENNARD_JONES_BCOEF",
				"(5E16.8)"));

		parseBondsIncHydrogen10I8(table.get("BONDS_INC_HYDROGEN", "(10I8)"));

		parseBondsWithoutHydrogen10I8(table.get("BONDS_WITHOUT_HYDROGEN",
				"(10I8)"));

		parseAnglesIncHydrogen10I8(table.get("ANGLES_INC_HYDROGEN", "(10I8)"));

		parseAnglesWithoutHydrogen10I8(table.get("ANGLES_WITHOUT_HYDROGEN",
				"(10I8)"));

		parseDihedralsIncHydrogen10I8(table.get("DIHEDRALS_INC_HYDROGEN",
				"(10I8)"));

		parseDihedralsWithoutHydrogen10I8(table.get(
				"DIHEDRALS_WITHOUT_HYDROGEN", "(10I8)"));

		parseExcludedAtomsList10I8(table.get("EXCLUDED_ATOMS_LIST", "(10I8)"));

		parseHbondAcoef5E16_8(table.get("HBOND_ACOEF", "(5E16.8)"));

		parseHbondBcoef5E16_8(table.get("HBOND_BCOEF", "(5E16.8)"));

		parseHbondCut5E16_8(table.get("HBCUT", "(5E16.8)"));

		parseAmberAtomType20A4(table.get("AMBER_ATOM_TYPE", "(20A4)"));

		parseTreeChainClassification20A4(table.get("TREE_CHAIN_CLASSIFICATION",
				"(20A4)"));

		parseJoinArray10I8(table.get("JOIN_ARRAY", "(10I8)"));

		parseIrotat10I8(table.get("IROTAT", "(10I8)"));

		parseRadiusSet1A80(table.get("RADIUS_SET", "(1A80)"));

		parseRadii5E16_8(table.get("RADII", "(5E16.8)"));

		parseScreen5E16_8(table.get("SCREEN", "(5E16.8)"));

		// Only in AMBER12 generated prmtops
		// parseIpol1I8(table.get("IPOL", "(1I8)"));

	}

	private void generateHashMap(ArrayList<Flag> flags) {

		table = HashBasedTable.create();

		for (Flag flag : flags) {

			table.put(flag.getName(), flag.getFormat(), flag.getContents());

		}

	}

	/**
	 * Scan a prmtop and harvest all the %FLAGs present, then return an array of
	 * Flag objects. Note, the %FLAGs contents have not been populated yet.
	 * 
	 * @param fileName
	 *            prmtop filename
	 * @return Array of Flag objects harvested from the prmtop file.
	 * @throws IOException
	 * 
	 */
	private ArrayList<Flag> getAllFlags(String fileName) throws IOException,
			ParseException {

		LOG.debug("getAllFlags() : Scanning " + fileName + " for %FLAGS");

		ArrayList<Flag> flags = new ArrayList<>();
		String line;
		String words[];

		// read it with BufferedReader
		BufferedReader br = new BufferedReader(new FileReader(fileName));

		while ((line = br.readLine()) != null) {

			if (line.matches("^%FLAG.*")) {
				LOG.debug("Found %FLAG line " + line);

				words = line.split(" ");

				// TODO Check to see if there is a words[2] and throw an
				// exception

				Flag currentFlag = new Flag(words[1]);

				// Read next line and grab the Fortran format.
				line = br.readLine();

				// Skip over a comment line since this is a valid
				// possibility
				if (line.matches("^%COMMENT.*")) {
					LOG.debug("Found COMMENT ");
					line = br.readLine();
				}

				if (line.matches("^%FORMAT.*")) {
					LOG.debug("Found FORMAT line " + line);

					line = line.replace("%FORMAT", "");
					line = line.trim();

					LOG.debug("\t%FORMAT is " + line);
					currentFlag.setFormat(line);

				}

				flags.add(currentFlag);

			}

		}

		// close the BufferedReaderer
		br.close();

		LOG.debug("getAllFlags() found these FLAGS: " + flags);

		return flags;
	}

	/**
	 * Given a Flag, search through a prmtop file and poulate the flag with the
	 * corresponding %FLAG contents.
	 * 
	 * @param flag
	 *            Flag to be populated
	 * @param fileName
	 *            prmtop filename
	 * @throws IOException
	 * @throws ParseException
	 */
	private void readFlag(Flag flag, String fileName) throws IOException,
			ParseException {

		String line;

		// Prepare the Fortran parser
		LOG.debug("readFlag(): attempting to read %FLAG: " + flag.getName()
				+ " with %FORMAT: " + flag.getFormat());

		FortranFormat formatter = new FortranFormat(flag.getFormat());

		// read it with BufferedReader
		BufferedReader br = new BufferedReader(new FileReader(fileName));

		// We could have a FLAG with no contents
		while ((line = br.readLine()) != null) {

			if (line.matches("^%FLAG " + flag.getName() + ".*")) {
				LOG.debug("readFlag(): Found %FLAG: " + flag.getName());

				// Move to next line which will be the %FORMAT
				// TODO Check if this is what is expected
				br.readLine();

				// We could be EOF
				while ((line = br.readLine()) != null) {

					// Keep reading content until we hit the next %FLAG or we
					// hit EOF
					if (!line.matches("^%FLAG.*")) {

						LOG.debug("readFlag(): " + line);

						ArrayList<Object> lineObjects = formatter.parse(line);

						// Contents of FLAG in file could be empty
						if (lineObjects != null) {
							for (Object lineObject : lineObjects) {
								if (lineObject != null) {

									LOG.debug("readFlag(): lineObject "
											+ lineObject);

									flag.addContent(lineObject);

								}

							}
						}
					} else {
						break;
					}

				}

				break;
			}

		}

		br.close();

	}
}
