# Load GAFF 
source leaprc.gaff

# Load the preparsed PDB
AEO = loadMol2 1_aminoethan_1_one.mol2

# Save a pdb for convenience of debugging
savepdb AEO snap.pdb

# Generate the AMBER parameter and coordinate files
saveamberparm AEO prmtop inpcrd

quit
