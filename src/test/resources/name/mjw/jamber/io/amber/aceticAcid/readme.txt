The files in this directory are the result of an automated
process to parameterise acetic acid using the AMBER tool
ANTECHAMBER. The aim is to generate an example AMBER topology/
parameter assignment.

The process starts with a hand drawn molecule in the ./raw
directory and results in an AMBER prmtop and inpcrd. The
assignment of charges and atomtypes is done using Antechamber
and the system is built using tleap. Antechambe assigns 
GAFF parameters which are compatible with parm94.
Finally, the energy of the system is calculated using SANDER.

The entire process is controlled by a Make file.




This is the final topology of the molecule:


GAFF atom types		Atom ids
--------------          --------

hc         o                H2        O
  \       /                  \       /
   \     /                    \     /
hc--c3--c                   H--C--C1
   /     \                    /     \
  /       \                  /       \
hc         oh--ho          H1         O1-H3



The antechamber generated mol2 file contains GAFF atomtype
assignments and charges:


@<TRIPOS>MOLECULE
ACE
    8     7     1     0     0
SMALL
bcc


@<TRIPOS>ATOM
      1 C          -3.9150   -0.4930    0.0000 c3        1 ACE     -0.200100
      2 C1         -2.7230    0.4150    0.0490 c         1 ACE      0.623100
      3 H          -4.8240    0.1060   -0.0980 hc        1 ACE      0.086700
      4 H1         -3.9740   -1.0750    0.9240 hc        1 ACE      0.068700
      5 H2         -3.8440   -1.1540   -0.8680 hc        1 ACE      0.068700
      6 O          -2.7880    1.6330    0.0830 o         1 ACE     -0.485000
      7 O1         -1.5520   -0.2590    0.0650 oh        1 ACE     -0.584100
      8 H3         -1.6580   -1.2280    0.0390 ho        1 ACE      0.422000
@<TRIPOS>BOND
     1    1    2 1
     2    1    3 1
     3    1    4 1
     4    1    5 1
     5    6    2 2
     6    2    7 1
     7    7    8 1
@<TRIPOS>SUBSTRUCTURE
     1 ACE         1 TEMP              0 ****  ****    0 ROOT


Dissecting the prmtop and inpcrd files, one can see that the following
parameters were used:


Atom 
-----

    MASS          ATPOL
c  12.01         0.616               Sp2 C carbonyl group
c3 12.01         0.878               Sp3 C
hc 1.008         0.135               H bonded to aliphatic carbon without electrwd. group
o  16.00         0.434               Oxygen with one connected atom
oh 16.00         0.465               Oxygen in hydroxyl group
ho 1.008         0.135               Hydroxyl group


         AMASS      Atomic mass of the center having the symbol "KNDSYM".

         ATPOL      The atomic polarizability for each atom (in A**3)
                    This is the type of polarizability used in sander
                    and gibbs. No parameters are supplied for this since
                    the feature is still in development (Amber 4.1).


Bond
-----
         RK       REQ
c3-hc  337.3    1.0920       SOURCE3    2815    0.0059
ho-oh  369.6    0.9740       SOURCE3     367    0.0105
c -c3  328.3    1.5080       SOURCE1    2949    0.0060
c -o   648.0    1.2140       SOURCE1    3682    0.0165
c -oh  466.4    1.3060       SOURCE1     271    0.0041

Where

         RK         The harmonic force constant for the bond "IBT"-"JBT".
                    The unit is kcal/mol/(A**2).

         REQ        The equilibrium bond length for the above bond in Angstroms

Angle
-----

             TK         TEQ
c -c3-hc   47.200     109.680   SOURCE3          614    0.6426
c -oh-ho   51.190     107.370   SOURCE3           34    1.6830
hc-c3-hc   39.430     108.350   SOURCE3         2380    0.9006
c3-c -o    68.030     123.110   SOURCE3          267    3.0977
c3-c -oh   69.840     112.200   SOURCE3           14    1.8324
o -c -oh   77.380     122.880   SOURCE3           33    2.1896

Where:

         TK         The harmonic force constants for the angle "ITT"-"JTT"-
                    "KTT" in units of kcal/mol/(rad**2) (radians are the
                    traditional unit for angle parameters in force fields).

         TEQ        The equilibrium bond angle for the above angle in degrees.


Dihedrals
---------
[c3-c -oh-ho]

             IDIVF  PK           PHASE             PN 
X -c -oh-X    2    4.600       180.000           2.000      Junmei et al, 1999

[hc-c3-c -o ]
hc-c3-c -o    1    0.80          0.0            -1.         Junmei et al, 1999
hc-c3-c -o    1    0.08        180.0             3.         Junmei et al, 1999


[o-c-oh-ho]
ho-oh-c -o    1    2.30        180.0            -2.         Junmei et al, 1999
ho-oh-c -o    1    1.90          0.0             1.         Junmei et al, 1999

[c3-o-c-oh]
c3-o -c -oh         1.1          180.          2.

where:

         IDIVF      The factor by which the torsional barrier is divided.
                    Consult Weiner, et al., JACS 106:765 (1984) p. 769 for
                    details. Basically, the actual torsional potential is

                           (PK/IDIVF) * (1 + cos(PN*phi - PHASE))

         PK         The barrier height divided by a factor of 2.

         PHASE      The phase shift angle in the torsional function.

                    The unit is degrees.

         PN         The periodicity of the torsional barrier.
                    NOTE: If PN .lt. 0.0 then the torsional potential
                          is assumed to have more than one term, and the
                          values of the rest of the terms are read from the
                          next cards until a positive PN is encountered.  The
                          negative value of pn is used only for identifying
                          the existence of the next term and only the
                          absolute value of PN is kept.


Note for more information about dihedrals, please see page 254
of http://ambermd.org/doc8/amber8.pdf


VDW
===
		R      EDEP
  hc          1.4870  0.0157             OPLS
  c3          1.9080  0.1094             OPLS
  c           1.9080  0.0860             OPLS
  o           1.6612  0.2100             OPLS
  oh          1.7210  0.2104             OPLS
  ho          0.0000  0.0000             OPLS Jorgensen, JACS,110,(1988),1657

where: 
	R 	= The van der Waals radius of the atoms having the symbol (angstroms)
	EDEP	= The 6-12 potential well depth. (kcal/mol)


Refs
====
http://ambermd.org/formats.html#parm.dat
http://ambermd.org/formats.html#topology



Full JAMBER output
===================

Universe potential energy is -35.45806073405118 kcal/mol
Universe bond energy is 1.0058029411602138 kcal/mol
Universe angle energy is 0.7179959272817384 kcal/mol
Universe dihedral energy is 2.4323562765011393 kcal/mol
Universe Vdw potential energy is 0.0 kcal/mol
Universe 14Vdw potential energy is 0.2593454785090933 kcal/mol
Universe ee potential energy is 11.474374693059954 kcal/mol
Universe 14ee potential energy is -51.34793605056332 kcal/mol


==============================
ATOMS (8 terms)
==============================
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.2001
 name :          C
 amberAtomType : c3
 x, y, z :       -3.915 -0.493 0.0
}
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.6230999983536656
 name :          C1
 amberAtomType : c
 x, y, z :       -2.723 0.415 0.049
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0867
 name :          H
 amberAtomType : hc
 x, y, z :       -4.824 0.106 -0.098
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0687
 name :          H1
 amberAtomType : hc
 x, y, z :       -3.974 -1.075 0.924
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0687
 name :          H2
 amberAtomType : hc
 x, y, z :       -3.844 -1.154 -0.868
}
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.485
 name :          O
 amberAtomType : o
 x, y, z :       -2.788 1.633 0.083
}
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5840999983536654
 name :          O1
 amberAtomType : oh
 x, y, z :       -1.552 -0.259 0.065
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.422
 name :          H3
 amberAtomType : ho
 x, y, z :       -1.658 -1.228 0.039
}

==============================
BONDS (7 terms)
==============================
name.mjw.jamber.Bond Object {
 Atoms:						C -H 
 AtomTypes:					c3-hc
 forceConstant (kcal/(mol Angstrom**2)): 		337.3
 equilibriumLength (A): 			1.092
 current length (A) : 				1.0930169257609872
 current potential contribution (kcal/mol): 	3.4881474853306146E-4
}
name.mjw.jamber.Bond Object {
 Atoms:						C -H1
 AtomTypes:					c3-hc
 forceConstant (kcal/(mol Angstrom**2)): 		337.3
 equilibriumLength (A): 			1.092
 current length (A) : 				1.093609162361033
 current potential contribution (kcal/mol): 	8.734058019547514E-4
}
name.mjw.jamber.Bond Object {
 Atoms:						C -H2
 AtomTypes:					c3-hc
 forceConstant (kcal/(mol Angstrom**2)): 		337.3
 equilibriumLength (A): 			1.092
 current length (A) : 				1.093337093489469
 current potential contribution (kcal/mol): 	6.030313485584436E-4
}
name.mjw.jamber.Bond Object {
 Atoms:						O1-H3
 AtomTypes:					oh-ho
 forceConstant (kcal/(mol Angstrom**2)): 		369.6
 equilibriumLength (A): 			0.9740000000000001
 current length (A) : 				0.9751271711935833
 current potential contribution (kcal/mol): 	4.69582306908329E-4
}
name.mjw.jamber.Bond Object {
 Atoms:						C -C1
 AtomTypes:					c3-c 
 forceConstant (kcal/(mol Angstrom**2)): 		328.3
 equilibriumLength (A): 			1.508
 current length (A) : 				1.4992428088872063
 current potential contribution (kcal/mol): 	0.025176800467861582
}
name.mjw.jamber.Bond Object {
 Atoms:						C1-O 
 AtomTypes:					c -o 
 forceConstant (kcal/(mol Angstrom**2)): 		648.0
 equilibriumLength (A): 			1.214
 current length (A) : 				1.2202069496605892
 current potential contribution (kcal/mol): 	0.024964993209729367
}
name.mjw.jamber.Bond Object {
 Atoms:						C1-O1
 AtomTypes:					c -oh
 forceConstant (kcal/(mol Angstrom**2)): 		466.4
 equilibriumLength (A): 			1.306
 current length (A) : 				1.351211678457524
 current potential contribution (kcal/mol): 	0.9533663132766683
}

==============================
ANGLES (10 terms)
==============================
name.mjw.jamber.Angle Object {
 Atoms:						C1-C -H 
 AtomTypes:					c -c3-hc
 forceConstant ( kcal/mol/(rad**2) ) : 		47.199999999999996
 equilibriumAngle (degrees): 			109.68004677699744
 current angle (degrees): 			109.40468169172212
 current potential contribution (kcal/mol): 	0.0010902208425706353
}
name.mjw.jamber.Angle Object {
 Atoms:						C1-C -H1
 AtomTypes:					c -c3-hc
 forceConstant ( kcal/mol/(rad**2) ) : 		47.199999999999996
 equilibriumAngle (degrees): 			109.68004677699744
 current angle (degrees): 			109.73012182356307
 current potential contribution (kcal/mol): 	3.605283802960485E-5
}
name.mjw.jamber.Angle Object {
 Atoms:						C1-C -H2
 AtomTypes:					c -c3-hc
 forceConstant ( kcal/mol/(rad**2) ) : 		47.199999999999996
 equilibriumAngle (degrees): 			109.68004677699744
 current angle (degrees): 			109.90540537635435
 current potential contribution (kcal/mol): 	7.302053378644499E-4
}
name.mjw.jamber.Angle Object {
 Atoms:						C1-O1-H3
 AtomTypes:					c -oh-ho
 forceConstant ( kcal/mol/(rad**2) ) : 		51.19
 equilibriumAngle (degrees): 			107.37004595887494
 current angle (degrees): 			113.65044473873573
 current potential contribution (kcal/mol): 	0.6150554991491265
}
name.mjw.jamber.Angle Object {
 Atoms:						H -C -H1
 AtomTypes:					hc-c3-hc
 forceConstant ( kcal/mol/(rad**2) ) : 		39.43
 equilibriumAngle (degrees): 			108.3500467226538
 current angle (degrees): 			108.8164101048744
 current potential contribution (kcal/mol): 	0.002612344201351104
}
name.mjw.jamber.Angle Object {
 Atoms:						H -C -H2
 AtomTypes:					hc-c3-hc
 forceConstant ( kcal/mol/(rad**2) ) : 		39.43
 equilibriumAngle (degrees): 			108.3500467226538
 current angle (degrees): 			108.30917725710965
 current potential contribution (kcal/mol): 	2.006224035189696E-5
}
name.mjw.jamber.Angle Object {
 Atoms:						H1-C -H2
 AtomTypes:					hc-c3-hc
 forceConstant ( kcal/mol/(rad**2) ) : 		39.43
 equilibriumAngle (degrees): 			108.3500467226538
 current angle (degrees): 			110.64238063719264
 current potential contribution (kcal/mol): 	0.06311568088896967
}
name.mjw.jamber.Angle Object {
 Atoms:						C -C1-O 
 AtomTypes:					c3-c -o 
 forceConstant ( kcal/mol/(rad**2) ) : 		68.03
 equilibriumAngle (degrees): 			123.1100525900646
 current angle (degrees): 			124.27054909464154
 current potential contribution (kcal/mol): 	0.027908910280067686
}
name.mjw.jamber.Angle Object {
 Atoms:						C -C1-O1
 AtomTypes:					c3-c -oh
 forceConstant ( kcal/mol/(rad**2) ) : 		69.84
 equilibriumAngle (degrees): 			112.20004808619127
 current angle (degrees): 			112.7876393738684
 current potential contribution (kcal/mol): 	0.007345302086210987
}
name.mjw.jamber.Angle Object {
 Atoms:						O -C1-O1
 AtomTypes:					o -c -oh
 forceConstant ( kcal/mol/(rad**2) ) : 		77.38000000000001
 equilibriumAngle (degrees): 			122.88005243419641
 current angle (degrees): 			122.93890763073236
 current potential contribution (kcal/mol): 	8.164941719589573E-5
}

==============================
DIHEDRALS (15 terms)
==============================
name.mjw.jamber.Dihedral Object {
 Atoms:						C -C1-O1-H3
 AtomTypes:					c3-c -oh-ho
 barrierHeight: 				4.6
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		0.21889212421419355
 current potential contribution: (kcal/mol) 	6.711470035548616E-5
 contributeTo14NonBondedTerms:					true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H -C -C1-O 
 AtomTypes:					hc-c3-c -o 
 barrierHeight: 				1.6
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		5.451331916035959
 current potential contribution: (kcal/mol) 	1.596381801919403
 contributeTo14NonBondedTerms:					true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H -C -C1-O 
 AtomTypes:					hc-c3-c -o 
 barrierHeight: 				0.16
 phase (radians): 				3.141594
 periodicity: 					3.0
 current dihedral angle (degrees) 		5.451331916035959
 current potential contribution: (kcal/mol) 	0.0032367405122166204
 contributeTo14NonBondedTerms:					false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H -C -C1-O1
 AtomTypes:					hc-c3-c -oh
 barrierHeight: 				0.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		-175.154101888635
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:					true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H1-C -C1-O 
 AtomTypes:					hc-c3-c -o 
 barrierHeight: 				1.6
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		-113.86435124481288
 current potential contribution: (kcal/mol) 	0.4763418636650807
 contributeTo14NonBondedTerms:					true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H1-C -C1-O 
 AtomTypes:					hc-c3-c -o 
 barrierHeight: 				0.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		-113.86435124481288
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:					false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H1-C -C1-O 
 AtomTypes:					hc-c3-c -o 
 barrierHeight: 				0.16
 phase (radians): 				3.141594
 periodicity: 					3.0
 current dihedral angle (degrees) 		-113.86435124481288
 current potential contribution: (kcal/mol) 	0.004092947036576513
 contributeTo14NonBondedTerms:					false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H1-C -C1-O1
 AtomTypes:					hc-c3-c -oh
 barrierHeight: 				0.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		65.53021495051607
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:					true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H2-C -C1-O 
 AtomTypes:					hc-c3-c -o 
 barrierHeight: 				1.6
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		124.2528158915286
 current potential contribution: (kcal/mol) 	0.3497235591412857
 contributeTo14NonBondedTerms:					true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H2-C -C1-O 
 AtomTypes:					hc-c3-c -o 
 barrierHeight: 				0.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		124.2528158915286
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:					false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H2-C -C1-O 
 AtomTypes:					hc-c3-c -o 
 barrierHeight: 				0.16
 phase (radians): 				3.141594
 periodicity: 					3.0
 current dihedral angle (degrees) 		124.2528158915286
 current potential contribution: (kcal/mol) 	0.0019751945857652497
 contributeTo14NonBondedTerms:					false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H2-C -C1-O1
 AtomTypes:					hc-c3-c -oh
 barrierHeight: 				0.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		-56.35261791314245
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:					true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						O -C1-O1-H3
 AtomTypes:					o -c -oh-ho
 barrierHeight: 				3.8
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		179.62273797926912
 current potential contribution: (kcal/mol) 	4.118726411211915E-5
 contributeTo14NonBondedTerms:					true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						O -C1-O1-H3
 AtomTypes:					o -c -oh-ho
 barrierHeight: 				4.6
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		179.62273797926912
 current potential contribution: (kcal/mol) 	1.99471688426589E-4
 contributeTo14NonBondedTerms:					false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -O -C1-O1
 AtomTypes:					c3-o -c -oh
 barrierHeight: 				2.2
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		-179.33490697695808
 current potential contribution: (kcal/mol) 	2.9639598791725286E-4
 contributeTo14NonBondedTerms:					false
 improper:					true
}

==============================
VDW (3 terms)
==============================
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -H3
 AtomTypes:					hc-ho
 ri: 1.4869999997254757
 ei: 0.01570000002623629
 rj: 0.0
 ej: 0.0
 current length: 3.4382962350559616
 current potential contribution: 0.0
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H1-H3
 AtomTypes:					hc-ho
 ri: 1.4869999997254757
 ei: 0.01570000002623629
 rj: 0.0
 ej: 0.0
 current length: 2.4840471010027168
 current potential contribution: 0.0
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H2-H3
 AtomTypes:					hc-ho
 ri: 1.4869999997254757
 ei: 0.01570000002623629
 rj: 0.0
 ej: 0.0
 current length: 2.3678515578473243
 current potential contribution: 0.0
}

==============================
VDW14 (8 terms)
==============================
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -H3
 AtomTypes:					c3-ho
 ri: 1.9080000000152688
 ei: 0.10939999991572773
 rj: 0.0
 ej: 0.0
 current length: 2.373982940124044
 current potential contribution: 0.0
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -O 
 AtomTypes:					hc-o 
 ri: 1.4869999997254757
 ei: 0.01570000002623629
 rj: 1.661200000277935
 ej: 0.20999999984182244
 current length: 2.551428227483579
 current potential contribution: 0.3098786826629917
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -O1
 AtomTypes:					hc-oh
 ri: 1.4869999997254757
 ei: 0.01570000002623629
 rj: 1.7209999997862235
 ej: 0.21040000024869926
 current length: 3.2963279569848627
 current potential contribution: -0.05617440344968181
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H1-O 
 AtomTypes:					hc-o 
 ri: 1.4869999997254757
 ei: 0.01570000002623629
 rj: 1.661200000277935
 ej: 0.20999999984182244
 current length: 3.0736201782263213
 current potential contribution: -0.056045173318214556
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H1-O1
 AtomTypes:					hc-oh
 ri: 1.4869999997254757
 ei: 0.01570000002623629
 rj: 1.7209999997862235
 ej: 0.21040000024869926
 current length: 2.696260558625594
 current potential contribution: 0.13644281793498447
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H2-O 
 AtomTypes:					hc-o 
 ri: 1.4869999997254757
 ei: 0.01570000002623629
 rj: 1.661200000277935
 ej: 0.20999999984182244
 current length: 3.12840310701802
 current potential contribution: -0.05733406933429257
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H2-O1
 AtomTypes:					hc-oh
 ri: 1.4869999997254757
 ei: 0.01570000002623629
 rj: 1.7209999997862235
 ej: 0.21040000024869926
 current length: 2.6314972924173796
 current potential contribution: 0.24192310252239946
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -H3
 AtomTypes:					o -ho
 ri: 1.661200000277935
 ei: 0.20999999984182244
 rj: 0.0
 ej: 0.0
 current length: 3.07638700426328
 current potential contribution: 0.0
}

==============================
EE (3 terms)
==============================
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -H3
 AtomTypes:					hc-ho
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0867
 name :          H
 amberAtomType : hc
 x, y, z :       -4.824 0.106 -0.098
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.422
 name :          H3
 amberAtomType : ho
 x, y, z :       -1.658 -1.228 0.039
} charge on i: 0.0867
 charge on j: 0.422
 current length: 3.4382962350559616
 current potential contribution: 3.5334149429519077
}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H1-H3
 AtomTypes:					hc-ho
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0687
 name :          H1
 amberAtomType : hc
 x, y, z :       -3.974 -1.075 0.924
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.422
 name :          H3
 amberAtomType : ho
 x, y, z :       -1.658 -1.228 0.039
} charge on i: 0.0687
 charge on j: 0.422
 current length: 2.4840471010027168
 current potential contribution: 3.875392962051846
}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H2-H3
 AtomTypes:					hc-ho
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0687
 name :          H2
 amberAtomType : hc
 x, y, z :       -3.844 -1.154 -0.868
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.422
 name :          H3
 amberAtomType : ho
 x, y, z :       -1.658 -1.228 0.039
} charge on i: 0.0687
 charge on j: 0.422
 current length: 2.3678515578473243
 current potential contribution: 4.065566788056201
}

==============================
EE14 (8 terms)
==============================
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -H3
 AtomTypes:					c3-ho
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.2001
 name :          C
 amberAtomType : c3
 x, y, z :       -3.915 -0.493 0.0
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.422
 name :          H3
 amberAtomType : ho
 x, y, z :       -1.658 -1.228 0.039
} charge on i: -0.2001
 charge on j: 0.422
 current length: 2.373982940124044
 current potential contribution: -11.811045172137996
}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -O 
 AtomTypes:					hc-o 
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0867
 name :          H
 amberAtomType : hc
 x, y, z :       -4.824 0.106 -0.098
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.485
 name :          O
 amberAtomType : o
 x, y, z :       -2.788 1.633 0.083
} charge on i: 0.0867
 charge on j: -0.485
 current length: 2.551428227483579
 current potential contribution: -5.472475988527964
}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -O1
 AtomTypes:					hc-oh
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0867
 name :          H
 amberAtomType : hc
 x, y, z :       -4.824 0.106 -0.098
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5840999983536654
 name :          O1
 amberAtomType : oh
 x, y, z :       -1.552 -0.259 0.065
} charge on i: 0.0867
 charge on j: -0.5840999983536654
 current length: 3.2963279569848627
 current potential contribution: -5.101316547646064
}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H1-O 
 AtomTypes:					hc-o 
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0687
 name :          H1
 amberAtomType : hc
 x, y, z :       -3.974 -1.075 0.924
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.485
 name :          O
 amberAtomType : o
 x, y, z :       -2.788 1.633 0.083
} charge on i: 0.0687
 charge on j: -0.485
 current length: 3.0736201782263213
 current potential contribution: -3.5996034684779716
}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H1-O1
 AtomTypes:					hc-oh
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0687
 name :          H1
 amberAtomType : hc
 x, y, z :       -3.974 -1.075 0.924
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5840999983536654
 name :          O1
 amberAtomType : oh
 x, y, z :       -1.552 -0.259 0.065
} charge on i: 0.0687
 charge on j: -0.5840999983536654
 current length: 2.696260558625594
 current potential contribution: -4.94183758257348
}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H2-O 
 AtomTypes:					hc-o 
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0687
 name :          H2
 amberAtomType : hc
 x, y, z :       -3.844 -1.154 -0.868
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.485
 name :          O
 amberAtomType : o
 x, y, z :       -2.788 1.633 0.083
} charge on i: 0.0687
 charge on j: -0.485
 current length: 3.12840310701802
 current potential contribution: -3.536569129952478
}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H2-O1
 AtomTypes:					hc-oh
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0687
 name :          H2
 amberAtomType : hc
 x, y, z :       -3.844 -1.154 -0.868
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5840999983536654
 name :          O1
 amberAtomType : oh
 x, y, z :       -1.552 -0.259 0.065
} charge on i: 0.0687
 charge on j: -0.5840999983536654
 current length: 2.6314972924173796
 current potential contribution: -5.0634601826955405
}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -H3
 AtomTypes:					o -ho
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.485
 name :          O
 amberAtomType : o
 x, y, z :       -2.788 1.633 0.083
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.422
 name :          H3
 amberAtomType : ho
 x, y, z :       -1.658 -1.228 0.039
} charge on i: -0.485
 charge on j: 0.422
 current length: 3.07638700426328
 current potential contribution: -22.091215188664485
}

