Universe potential energy is -2.6272730062806957 kcal/mol
Universe bond energy is 5.636627369168951 kcal/mol
Universe angle energy is 8.204617382650161 kcal/mol
Universe dihedral energy is 11.365695936660277 kcal/mol
Universe Vdw potential energy is -1.0455001421313517 kcal/mol
Universe 14Vdw potential energy is 4.461456783944155 kcal/mol
Universe ee potential energy is -79.99451627132315 kcal/mol
Universe 14ee potential energy is 48.744345934750264 kcal/mol


==============================
ATOMS (22 terms)
==============================
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}

==============================
BONDS (21 terms)
==============================
name.mjw.jamber.Bond Object {
 Atoms:						CH3-HH32
 AtomTypes:					CT-HC
 forceConstant (kcal/(mol Angstrom**2)): 	340.0
 equilibriumLength (A): 			1.09
 current length (A) : 				1.114590667353778
 current potential contribution (kcal/mol): 	0.2055983131074139
}
name.mjw.jamber.Bond Object {
 Atoms:						CH3-HH33
 AtomTypes:					CT-HC
 forceConstant (kcal/(mol Angstrom**2)): 	340.0
 equilibriumLength (A): 			1.09
 current length (A) : 				1.100052791302754
 current potential contribution (kcal/mol): 	0.03435992841208658
}
name.mjw.jamber.Bond Object {
 Atoms:						HH31-CH3
 AtomTypes:					HC-CT
 forceConstant (kcal/(mol Angstrom**2)): 	340.0
 equilibriumLength (A): 			1.09
 current length (A) : 				1.1036719024691803
 current potential contribution (kcal/mol): 	0.06355311182310432
}
name.mjw.jamber.Bond Object {
 Atoms:						CB-HB1
 AtomTypes:					CT-HC
 forceConstant (kcal/(mol Angstrom**2)): 	340.0
 equilibriumLength (A): 			1.09
 current length (A) : 				1.0856680374661312
 current potential contribution (kcal/mol): 	0.0063804057942469595
}
name.mjw.jamber.Bond Object {
 Atoms:						CB-HB2
 AtomTypes:					CT-HC
 forceConstant (kcal/(mol Angstrom**2)): 	340.0
 equilibriumLength (A): 			1.09
 current length (A) : 				1.0708597826580888
 current potential contribution (kcal/mol): 	0.1245582927645046
}
name.mjw.jamber.Bond Object {
 Atoms:						CB-HB3
 AtomTypes:					CT-HC
 forceConstant (kcal/(mol Angstrom**2)): 	340.0
 equilibriumLength (A): 			1.09
 current length (A) : 				1.0881910120383
 current potential contribution (kcal/mol): 	0.0011126287314957226
}
name.mjw.jamber.Bond Object {
 Atoms:						CA-HA
 AtomTypes:					CT-H1
 forceConstant (kcal/(mol Angstrom**2)): 	340.0
 equilibriumLength (A): 			1.09
 current length (A) : 				1.0989668536731443
 current potential contribution (kcal/mol): 	0.0273375180304971
}
name.mjw.jamber.Bond Object {
 Atoms:						N -H 
 AtomTypes:					N -H 
 forceConstant (kcal/(mol Angstrom**2)): 	434.0
 equilibriumLength (A): 			1.01
 current length (A) : 				0.9790331068128086
 current potential contribution (kcal/mol): 	0.41618363757144294
}
name.mjw.jamber.Bond Object {
 Atoms:						CH3-HH31
 AtomTypes:					CT-H1
 forceConstant (kcal/(mol Angstrom**2)): 	340.0
 equilibriumLength (A): 			1.09
 current length (A) : 				1.1017050225510685
 current potential contribution (kcal/mol): 	0.04658256799314691
}
name.mjw.jamber.Bond Object {
 Atoms:						CH3-HH32
 AtomTypes:					CT-H1
 forceConstant (kcal/(mol Angstrom**2)): 	340.0
 equilibriumLength (A): 			1.09
 current length (A) : 				1.0831099878644375
 current potential contribution (kcal/mol): 	0.01614057085758805
}
name.mjw.jamber.Bond Object {
 Atoms:						CH3-HH33
 AtomTypes:					CT-H1
 forceConstant (kcal/(mol Angstrom**2)): 	340.0
 equilibriumLength (A): 			1.09
 current length (A) : 				1.092653189770775
 current potential contribution (kcal/mol): 	0.0023934014263131322
}
name.mjw.jamber.Bond Object {
 Atoms:						N -H 
 AtomTypes:					N -H 
 forceConstant (kcal/(mol Angstrom**2)): 	434.0
 equilibriumLength (A): 			1.01
 current length (A) : 				1.0482740088806928
 current potential contribution (kcal/mol): 	0.6357664940169174
}
name.mjw.jamber.Bond Object {
 Atoms:						C -O 
 AtomTypes:					C -O 
 forceConstant (kcal/(mol Angstrom**2)): 	570.0
 equilibriumLength (A): 			1.229
 current length (A) : 				1.1784110866935564
 current potential contribution (kcal/mol): 	1.4587657452303173
}
name.mjw.jamber.Bond Object {
 Atoms:						C -N 
 AtomTypes:					C -N 
 forceConstant (kcal/(mol Angstrom**2)): 	490.00000000000006
 equilibriumLength (A): 			1.335
 current length (A) : 				1.3154336282464654
 current potential contribution (kcal/mol): 	0.18759302276278308
}
name.mjw.jamber.Bond Object {
 Atoms:						CH3-C 
 AtomTypes:					CT-C 
 forceConstant (kcal/(mol Angstrom**2)): 	317.0
 equilibriumLength (A): 			1.522
 current length (A) : 				1.572110669738772
 current potential contribution (kcal/mol): 	0.7960121132688407
}
name.mjw.jamber.Bond Object {
 Atoms:						C -O 
 AtomTypes:					C -O 
 forceConstant (kcal/(mol Angstrom**2)): 	570.0
 equilibriumLength (A): 			1.229
 current length (A) : 				1.1997092744500815
 current potential contribution (kcal/mol): 	0.4890295638471739
}
name.mjw.jamber.Bond Object {
 Atoms:						C -N 
 AtomTypes:					C -N 
 forceConstant (kcal/(mol Angstrom**2)): 	490.00000000000006
 equilibriumLength (A): 			1.335
 current length (A) : 				1.3268797923608335
 current potential contribution (kcal/mol): 	0.03230950833055668
}
name.mjw.jamber.Bond Object {
 Atoms:						CA-CB
 AtomTypes:					CT-CT
 forceConstant (kcal/(mol Angstrom**2)): 	310.0
 equilibriumLength (A): 			1.526
 current length (A) : 				1.5718926185337343
 current potential contribution (kcal/mol): 	0.6529010551236838
}
name.mjw.jamber.Bond Object {
 Atoms:						CA-C 
 AtomTypes:					CT-C 
 forceConstant (kcal/(mol Angstrom**2)): 	317.0
 equilibriumLength (A): 			1.522
 current length (A) : 				1.5285857893601815
 current potential contribution (kcal/mol): 	0.013749121014447531
}
name.mjw.jamber.Bond Object {
 Atoms:						N -CA
 AtomTypes:					N -CT
 forceConstant (kcal/(mol Angstrom**2)): 	337.0
 equilibriumLength (A): 			1.449
 current length (A) : 				1.4784701857607443
 current potential contribution (kcal/mol): 	0.2926817530364241
}
name.mjw.jamber.Bond Object {
 Atoms:						N -CH3
 AtomTypes:					N -CT
 forceConstant (kcal/(mol Angstrom**2)): 	337.0
 equilibriumLength (A): 			1.449
 current length (A) : 				1.468912167375065
 current potential contribution (kcal/mol): 	0.13361861602596517
}

==============================
ANGLES (36 terms)
==============================
name.mjw.jamber.Angle Object {
 Atoms:						C -N -H 
 AtomTypes:					C -N -H 
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			120.00005142908157
 current angle (degrees): 			119.25509518999284
 current potential contribution (kcal/mol): 	0.00845252109000015
}
name.mjw.jamber.Angle Object {
 Atoms:						HH33-CH3-C 
 AtomTypes:					HC-CT-C 
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			113.18951087582654
 current potential contribution (kcal/mol): 	0.2073248124217212
}
name.mjw.jamber.Angle Object {
 Atoms:						HH32-CH3-HH33
 AtomTypes:					HC-CT-HC
 forceConstant ( kcal/mol/(rad**2) ) : 		35.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			106.12853800876172
 current potential contribution (kcal/mol): 	0.12119128927020363
}
name.mjw.jamber.Angle Object {
 Atoms:						HH32-CH3-C 
 AtomTypes:					HC-CT-C 
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			107.10982882175597
 current potential contribution (kcal/mol): 	0.08701613788996918
}
name.mjw.jamber.Angle Object {
 Atoms:						HH31-CH3-HH32
 AtomTypes:					HC-CT-HC
 forceConstant ( kcal/mol/(rad**2) ) : 		35.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			107.48680857626718
 current potential contribution (kcal/mol): 	0.043212875860526793
}
name.mjw.jamber.Angle Object {
 Atoms:						HH31-CH3-HH33
 AtomTypes:					HC-CT-HC
 forceConstant ( kcal/mol/(rad**2) ) : 		35.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			113.16690261791946
 current potential contribution (kcal/mol): 	0.14335419830855206
}
name.mjw.jamber.Angle Object {
 Atoms:						HH31-CH3-C 
 AtomTypes:					HC-CT-C 
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			109.37340837005723
 current potential contribution (kcal/mol): 	2.4426242230821355E-4
}
name.mjw.jamber.Angle Object {
 Atoms:						C -N -H 
 AtomTypes:					C -N -H 
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			120.00005142908157
 current angle (degrees): 			116.3643204939425
 current potential contribution (kcal/mol): 	0.2013298687673108
}
name.mjw.jamber.Angle Object {
 Atoms:						HB2-CB-HB3
 AtomTypes:					HC-CT-HC
 forceConstant ( kcal/mol/(rad**2) ) : 		35.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			113.50676896187574
 current potential contribution (kcal/mol): 	0.17115957835686785
}
name.mjw.jamber.Angle Object {
 Atoms:						HB1-CB-HB2
 AtomTypes:					HC-CT-HC
 forceConstant ( kcal/mol/(rad**2) ) : 		35.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			102.42362671084479
 current potential contribution (kcal/mol): 	0.5338878148109704
}
name.mjw.jamber.Angle Object {
 Atoms:						HB1-CB-HB3
 AtomTypes:					HC-CT-HC
 forceConstant ( kcal/mol/(rad**2) ) : 		35.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			107.4851161485261
 current potential contribution (kcal/mol): 	0.043285560160513326
}
name.mjw.jamber.Angle Object {
 Atoms:						HA-CA-CB
 AtomTypes:					H1-CT-CT
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			109.90861011829806
 current potential contribution (kcal/mol): 	0.0025423960755197915
}
name.mjw.jamber.Angle Object {
 Atoms:						HA-CA-C 
 AtomTypes:					H1-CT-C 
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			99.45045980242462
 current potential contribution (kcal/mol): 	1.538229652428486
}
name.mjw.jamber.Angle Object {
 Atoms:						CA-CB-HB1
 AtomTypes:					CT-CT-HC
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			109.42412442255213
 current potential contribution (kcal/mol): 	8.77941976521709E-5
}
name.mjw.jamber.Angle Object {
 Atoms:						CA-CB-HB2
 AtomTypes:					CT-CT-HC
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			113.1274797312142
 current potential contribution (kcal/mol): 	0.20041189353530628
}
name.mjw.jamber.Angle Object {
 Atoms:						CA-CB-HB3
 AtomTypes:					CT-CT-HC
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			110.38014181756705
 current potential contribution (kcal/mol): 	0.011797330244808382
}
name.mjw.jamber.Angle Object {
 Atoms:						H -N -CA
 AtomTypes:					H -N -CT
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			118.04005047448166
 current angle (degrees): 			116.90198063590316
 current potential contribution (kcal/mol): 	0.019727069150415548
}
name.mjw.jamber.Angle Object {
 Atoms:						N -CA-HA
 AtomTypes:					N -CT-H1
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			110.4216133700726
 current potential contribution (kcal/mol): 	0.01293534577877446
}
name.mjw.jamber.Angle Object {
 Atoms:						HH32-CH3-HH33
 AtomTypes:					H1-CT-H1
 forceConstant ( kcal/mol/(rad**2) ) : 		35.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			110.56213155008581
 current potential contribution (kcal/mol): 	0.012026548863979723
}
name.mjw.jamber.Angle Object {
 Atoms:						HH31-CH3-HH32
 AtomTypes:					H1-CT-H1
 forceConstant ( kcal/mol/(rad**2) ) : 		35.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			103.1721849106508
 current potential contribution (kcal/mol): 	0.4269104451822655
}
name.mjw.jamber.Angle Object {
 Atoms:						HH31-CH3-HH33
 AtomTypes:					H1-CT-H1
 forceConstant ( kcal/mol/(rad**2) ) : 		35.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			108.52486215562386
 current potential contribution (kcal/mol): 	0.010139034542888864
}
name.mjw.jamber.Angle Object {
 Atoms:						H -N -CH3
 AtomTypes:					H -N -CT
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			118.04005047448166
 current angle (degrees): 			124.08416093874511
 current potential contribution (kcal/mol): 	0.556403080310971
}
name.mjw.jamber.Angle Object {
 Atoms:						N -CH3-HH31
 AtomTypes:					N -CT-H1
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			112.02197644970632
 current potential contribution (kcal/mol): 	0.0968702967691192
}
name.mjw.jamber.Angle Object {
 Atoms:						N -CH3-HH32
 AtomTypes:					N -CT-H1
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			111.672997840868
 current potential contribution (kcal/mol): 	0.0719158421454045
}
name.mjw.jamber.Angle Object {
 Atoms:						N -CH3-HH33
 AtomTypes:					N -CT-H1
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			109.50004692903693
 current angle (degrees): 			110.62963577996267
 current potential contribution (kcal/mol): 	0.019434149262736348
}
name.mjw.jamber.Angle Object {
 Atoms:						O -C -N 
 AtomTypes:					O -C -N 
 forceConstant ( kcal/mol/(rad**2) ) : 		80.0
 equilibriumAngle (degrees): 			122.90005267195104
 current angle (degrees): 			127.43056950338779
 current potential contribution (kcal/mol): 	0.5001960047959135
}
name.mjw.jamber.Angle Object {
 Atoms:						C -N -CA
 AtomTypes:					C -N -CT
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			121.90005224337536
 current angle (degrees): 			123.60412015998024
 current potential contribution (kcal/mol): 	0.04422812610301737
}
name.mjw.jamber.Angle Object {
 Atoms:						CH3-C -O 
 AtomTypes:					CT-C -O 
 forceConstant ( kcal/mol/(rad**2) ) : 		80.0
 equilibriumAngle (degrees): 			120.40005160051183
 current angle (degrees): 			117.67288059304069
 current potential contribution (kcal/mol): 	0.18124643152257963
}
name.mjw.jamber.Angle Object {
 Atoms:						CH3-C -N 
 AtomTypes:					CT-C -N 
 forceConstant ( kcal/mol/(rad**2) ) : 		70.0
 equilibriumAngle (degrees): 			116.60004997192425
 current angle (degrees): 			114.81558293760426
 current potential contribution (kcal/mol): 	0.06790000931959836
}
name.mjw.jamber.Angle Object {
 Atoms:						O -C -N 
 AtomTypes:					O -C -N 
 forceConstant ( kcal/mol/(rad**2) ) : 		80.0
 equilibriumAngle (degrees): 			122.90005267195104
 current angle (degrees): 			119.54454218383998
 current potential contribution (kcal/mol): 	0.2743859840720262
}
name.mjw.jamber.Angle Object {
 Atoms:						C -N -CH3
 AtomTypes:					C -N -CT
 forceConstant ( kcal/mol/(rad**2) ) : 		50.0
 equilibriumAngle (degrees): 			121.90005224337536
 current angle (degrees): 			119.54608973549993
 current potential contribution (kcal/mol): 	0.08439638068299896
}
name.mjw.jamber.Angle Object {
 Atoms:						CB-CA-C 
 AtomTypes:					CT-CT-C 
 forceConstant ( kcal/mol/(rad**2) ) : 		63.0
 equilibriumAngle (degrees): 			111.10004761475803
 current angle (degrees): 			110.9526573732102
 current potential contribution (kcal/mol): 	4.169008166190656E-4
}
name.mjw.jamber.Angle Object {
 Atoms:						CA-C -O 
 AtomTypes:					CT-C -O 
 forceConstant ( kcal/mol/(rad**2) ) : 		80.0
 equilibriumAngle (degrees): 			120.40005160051183
 current angle (degrees): 			125.17618312039016
 current potential contribution (kcal/mol): 	0.55590077179164
}
name.mjw.jamber.Angle Object {
 Atoms:						CA-C -N 
 AtomTypes:					CT-C -N 
 forceConstant ( kcal/mol/(rad**2) ) : 		70.0
 equilibriumAngle (degrees): 			116.60004997192425
 current angle (degrees): 			115.24799888312907
 current potential contribution (kcal/mol): 	0.03897974373949842
}
name.mjw.jamber.Angle Object {
 Atoms:						N -CA-CB
 AtomTypes:					N -CT-CT
 forceConstant ( kcal/mol/(rad**2) ) : 		80.0
 equilibriumAngle (degrees): 			109.70004701475206
 current angle (degrees): 			117.97535636594301
 current potential contribution (kcal/mol): 	1.6688342242950391
}
name.mjw.jamber.Angle Object {
 Atoms:						N -CA-C 
 AtomTypes:					N -CT-C 
 forceConstant ( kcal/mol/(rad**2) ) : 		63.0
 equilibriumAngle (degrees): 			110.10004718618232
 current angle (degrees): 			106.50345770401202
 current potential contribution (kcal/mol): 	0.24824300766395835
}

==============================
DIHEDRALS (66 terms)
==============================
name.mjw.jamber.Dihedral Object {
 Atoms:						O -C -N -H 
 AtomTypes:					O -C -N -H 
 barrierHeight: 				4.0
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		164.52716729200773
 current potential contribution: (kcal/mol) 	0.07248588344289497
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						O -C -N -H 
 AtomTypes:					O -C -N -H 
 barrierHeight: 				5.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		164.52716729200773
 current potential contribution: (kcal/mol) 	0.3558633939876901
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CA-HA
 AtomTypes:					C -N -CT-H1
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		-67.87446233461769
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH33-CH3-C -O 
 AtomTypes:					HC-CT-C -O 
 barrierHeight: 				1.6
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		-124.23882593912676
 current potential contribution: (kcal/mol) 	0.34988503037811025
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH33-CH3-C -O 
 AtomTypes:					HC-CT-C -O 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		-124.23882593912676
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH33-CH3-C -O 
 AtomTypes:					HC-CT-C -O 
 barrierHeight: 				0.16
 phase (radians): 				3.141594
 periodicity: 					3.0
 current dihedral angle (degrees) 		-124.23882593912676
 current potential contribution: (kcal/mol) 	0.001962321505936524
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH33-CH3-C -N 
 AtomTypes:					HC-CT-C -N 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		52.734165334906294
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH32-CH3-C -O 
 AtomTypes:					HC-CT-C -O 
 barrierHeight: 				1.6
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		119.14109953495911
 current potential contribution: (kcal/mol) 	0.41043037482706785
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH32-CH3-C -O 
 AtomTypes:					HC-CT-C -O 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		119.14109953495911
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH32-CH3-C -O 
 AtomTypes:					HC-CT-C -O 
 barrierHeight: 				0.16
 phase (radians): 				3.141594
 periodicity: 					3.0
 current dihedral angle (degrees) 		119.14109953495911
 current potential contribution: (kcal/mol) 	8.08901637434012E-5
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH32-CH3-C -N 
 AtomTypes:					HC-CT-C -N 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		-63.885909191007855
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CH3-C -N -H 
 AtomTypes:					CT-C -N -H 
 barrierHeight: 				5.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		-12.096561909297472
 current potential contribution: (kcal/mol) 	0.2195781162225835
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH31-CH3-C -O 
 AtomTypes:					HC-CT-C -O 
 barrierHeight: 				1.6
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		2.9405469127208455
 current potential contribution: (kcal/mol) 	1.598946642910278
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH31-CH3-C -O 
 AtomTypes:					HC-CT-C -O 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		2.9405469127208455
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH31-CH3-C -O 
 AtomTypes:					HC-CT-C -O 
 barrierHeight: 				0.16
 phase (radians): 				3.141594
 periodicity: 					3.0
 current dihedral angle (degrees) 		2.9405469127208455
 current potential contribution: (kcal/mol) 	9.463412508226377E-4
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HH31-CH3-C -N 
 AtomTypes:					HC-CT-C -N 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		179.91353818674966
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						O -C -N -H 
 AtomTypes:					O -C -N -H 
 barrierHeight: 				4.0
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		175.06500477356823
 current potential contribution: (kcal/mol) 	0.007414121528105833
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						O -C -N -H 
 AtomTypes:					O -C -N -H 
 barrierHeight: 				5.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		175.06500477356823
 current potential contribution: (kcal/mol) 	0.03700247312612398
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CH3-HH31
 AtomTypes:					C -N -CT-H1
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		79.00165143391592
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CH3-HH32
 AtomTypes:					C -N -CT-H1
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		-165.83184317228918
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CH3-HH33
 AtomTypes:					C -N -CT-H1
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		-42.22822172952431
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HB3-CB-CA-C 
 AtomTypes:					HC-CT-CT-C 
 barrierHeight: 				0.311111112
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		160.0632397077902
 current potential contribution: (kcal/mol) 	0.07733213353653934
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HB2-CB-CA-C 
 AtomTypes:					HC-CT-CT-C 
 barrierHeight: 				0.311111112
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		31.648778986710106
 current potential contribution: (kcal/mol) 	0.1421431403237746
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HB1-CB-CA-C 
 AtomTypes:					HC-CT-CT-C 
 barrierHeight: 				0.311111112
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		-81.8460728155189
 current potential contribution: (kcal/mol) 	0.09114215129096156
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HA-CA-CB-HB1
 AtomTypes:					H1-CT-CT-HC
 barrierHeight: 				0.311111112
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		169.14745256727085
 current potential contribution: (kcal/mol) 	0.024445482521446838
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HA-CA-CB-HB2
 AtomTypes:					H1-CT-CT-HC
 barrierHeight: 				0.311111112
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		-77.35769563050012
 current potential contribution: (kcal/mol) 	0.059942433965322596
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HA-CA-CB-HB3
 AtomTypes:					H1-CT-CT-HC
 barrierHeight: 				0.311111112
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		51.05676509057994
 current potential contribution: (kcal/mol) 	0.0167452533343046
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HA-CA-C -O 
 AtomTypes:					H1-CT-C -O 
 barrierHeight: 				1.6
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		-131.65341493432018
 current potential contribution: (kcal/mol) 	0.2683015431007548
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HA-CA-C -O 
 AtomTypes:					H1-CT-C -O 
 barrierHeight: 				0.16
 phase (radians): 				3.141594
 periodicity: 					3.0
 current dihedral angle (degrees) 		-131.65341493432018
 current potential contribution: (kcal/mol) 	0.014436075457089768
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						HA-CA-C -N 
 AtomTypes:					H1-CT-C -N 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		46.29299964959107
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CA-C -N -H 
 AtomTypes:					CT-C -N -H 
 barrierHeight: 				5.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		-3.0055935699589273
 current potential contribution: (kcal/mol) 	0.01374668506362281
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H -N -CA-HA
 AtomTypes:					H -N -CT-H1
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		117.79406172900079
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H -N -CA-CB
 AtomTypes:					H -N -CT-CT
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		-114.67440525311862
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H -N -CA-C 
 AtomTypes:					H -N -CT-C 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		10.75359400343791
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						N -CA-CB-HB1
 AtomTypes:					N -CT-CT-HC
 barrierHeight: 				0.311111112
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		41.37175683905992
 current potential contribution: (kcal/mol) 	0.06831071820230755
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						N -CA-CB-HB2
 AtomTypes:					N -CT-CT-HC
 barrierHeight: 				0.311111112
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		154.86660864128893
 current potential contribution: (kcal/mol) 	0.11634522417631916
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						N -CA-CB-HB3
 AtomTypes:					N -CT-CT-HC
 barrierHeight: 				0.311111112
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		-76.71893063763105
 current potential contribution: (kcal/mol) 	0.05589283825099496
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H -N -CH3-HH31
 AtomTypes:					H -N -CT-H1
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		-100.11880208649711
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H -N -CH3-HH32
 AtomTypes:					H -N -CT-H1
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		15.047703307297777
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						H -N -CH3-HH33
 AtomTypes:					H -N -CT-H1
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		138.65132475006268
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -CA-N -H 
 AtomTypes:					C -CT-N -H 
 barrierHeight: 				2.2
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		-174.3314759363815
 current potential contribution: (kcal/mol) 	0.021463181143952095
 contributeTo14NonBondedTerms:			false
 improper:					true
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -CH3-N -H 
 AtomTypes:					C -CT-N -H 
 barrierHeight: 				2.2
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		-179.12045352041247
 current potential contribution: (kcal/mol) 	5.183496405835042E-4
 contributeTo14NonBondedTerms:			false
 improper:					true
}
name.mjw.jamber.Dihedral Object {
 Atoms:						O -C -N -CA
 AtomTypes:					O -C -N -CT
 barrierHeight: 				5.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		-9.67829629535688
 current potential contribution: (kcal/mol) 	0.14131603254641828
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CA-CB
 AtomTypes:					C -N -CT-CT
 barrierHeight: 				4.0
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		59.65707068326291
 current potential contribution: (kcal/mol) 	3.01034877618695
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CA-CB
 AtomTypes:					C -N -CT-CT
 barrierHeight: 				4.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		59.65707068326291
 current potential contribution: (kcal/mol) 	1.020804649542467
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CA-CB
 AtomTypes:					C -N -CT-CT
 barrierHeight: 				0.8
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		59.65707068326291
 current potential contribution: (kcal/mol) 	6.447996606611106E-5
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CA-CB
 AtomTypes:					C -N -CT-CT
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					4.0
 current dihedral angle (degrees) 		59.65707068326291
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CA-C 
 AtomTypes:					C -N -CT-C 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		-174.91493006018052
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CA-C 
 AtomTypes:					C -N -CT-C 
 barrierHeight: 				0.54
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		-174.91493006018052
 current potential contribution: (kcal/mol) 	0.5357576961002825
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CA-C 
 AtomTypes:					C -N -CT-C 
 barrierHeight: 				0.8400000000000001
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		-174.91493006018052
 current potential contribution: (kcal/mol) 	0.014799370821542982
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						C -N -CA-C 
 AtomTypes:					C -N -CT-C 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					4.0
 current dihedral angle (degrees) 		-174.91493006018052
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CH3-C -N -CA
 AtomTypes:					CT-C -N -CT
 barrierHeight: 				5.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		173.6979745033379
 current potential contribution: (kcal/mol) 	0.060247394308979296
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						O -C -N -CH3
 AtomTypes:					O -C -N -CT
 barrierHeight: 				5.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		-4.121982895360644
 current potential contribution: (kcal/mol) 	0.025834248856707875
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CB-CA-C -O 
 AtomTypes:					CT-CT-C -O 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		112.66391826265307
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CB-CA-C -N 
 AtomTypes:					CT-CT-C -N 
 barrierHeight: 				0.4
 phase (radians): 				0.0
 periodicity: 					1.0
 current dihedral angle (degrees) 		-69.3896671534357
 current potential contribution: (kcal/mol) 	0.2704020907424646
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CB-CA-C -N 
 AtomTypes:					CT-CT-C -N 
 barrierHeight: 				0.4
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		-69.3896671534357
 current potential contribution: (kcal/mol) 	0.049564543809102114
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CB-CA-C -N 
 AtomTypes:					CT-CT-C -N 
 barrierHeight: 				0.8
 phase (radians): 				0.0
 periodicity: 					3.0
 current dihedral angle (degrees) 		-69.3896671534357
 current potential contribution: (kcal/mol) 	0.04737640576235807
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CB-CA-C -N 
 AtomTypes:					CT-CT-C -N 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					4.0
 current dihedral angle (degrees) 		-69.3896671534357
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CA-C -N -CH3
 AtomTypes:					CT-C -N -CT
 barrierHeight: 				5.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		177.80741876111253
 current potential contribution: (kcal/mol) 	0.007318791792948753
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						N -CA-C -O 
 AtomTypes:					N -CT-C -O 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					2.0
 current dihedral angle (degrees) 		-16.92977038304978
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						N -CA-C -N 
 AtomTypes:					N -CT-C -N 
 barrierHeight: 				0.9
 phase (radians): 				3.141594
 periodicity: 					1.0
 current dihedral angle (degrees) 		161.01664420086146
 current potential contribution: (kcal/mol) 	0.8755257033021201
 contributeTo14NonBondedTerms:			true
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						N -CA-C -N 
 AtomTypes:					N -CT-C -N 
 barrierHeight: 				3.16
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		161.01664420086146
 current potential contribution: (kcal/mol) 	0.3343793708636569
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						N -CA-C -N 
 AtomTypes:					N -CT-C -N 
 barrierHeight: 				1.1
 phase (radians): 				3.141594
 periodicity: 					3.0
 current dihedral angle (degrees) 		161.01664420086146
 current potential contribution: (kcal/mol) 	0.8499527251303323
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						N -CA-C -N 
 AtomTypes:					N -CT-C -N 
 barrierHeight: 				0.0
 phase (radians): 				0.0
 periodicity: 					4.0
 current dihedral angle (degrees) 		161.01664420086146
 current potential contribution: (kcal/mol) 	0.0
 contributeTo14NonBondedTerms:			false
 improper:					false
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CH3-N -C -O 
 AtomTypes:					CT-N -C -O 
 barrierHeight: 				21.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		176.62372920130503
 current potential contribution: (kcal/mol) 	0.07283762016120254
 contributeTo14NonBondedTerms:			false
 improper:					true
}
name.mjw.jamber.Dihedral Object {
 Atoms:						CA-N -C -O 
 AtomTypes:					CT-N -C -O 
 barrierHeight: 				21.0
 phase (radians): 				3.141594
 periodicity: 					2.0
 current dihedral angle (degrees) 		178.07059834352722
 current potential contribution: (kcal/mol) 	0.023805237415346292
 contributeTo14NonBondedTerms:			false
 improper:					true
}

==============================
VDW (133 terms)
==============================
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-H 
 AtomTypes:					HC-H 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				3.590019274986246
 current potential contribution:		-0.0011885583049487502
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-CA
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				4.653085612923427
 current potential contribution:		-0.011561633514464926
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-HA
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.94381770197836
 current potential contribution:		-0.0011885365246053245
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-CB
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				5.3102994080418995
 current potential contribution:		-0.005466711879798785
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-HB1
 AtomTypes:					HC-HC
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				5.225818071063303
 current potential contribution:		-0.0010485989934932952
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-HB2
 AtomTypes:					HC-HC
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				6.36751842056723
 current potential contribution:		-3.24260593946044E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-HB3
 AtomTypes:					HC-HC
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				5.131039918982901
 current potential contribution:		-0.0011679677444489493
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-C 
 AtomTypes:					HC-C 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9079999997313366
 ej:						0.08600000012835884
 current length:				5.745761441724308
 current potential contribution:		-0.0030608475082743313
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-O 
 AtomTypes:					HC-O 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				5.839354126961472
 current potential contribution:		-0.0027855365473954037
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-N 
 AtomTypes:					HC-N 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				6.909518161959922
 current potential contribution:		-0.0012434708389035102
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-H 
 AtomTypes:					HC-H 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				7.036381888618735
 current potential contribution:		-2.137076480107258E-5
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-CH3
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				8.112349130172817
 current potential contribution:		-4.440984016710709E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-HH31
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				8.271290829596037
 current potential contribution:		-5.521132419377866E-5
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-HH32
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				9.005850272335392
 current potential contribution:		-3.3149155784157124E-5
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-HH33
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				8.246023058796695
 current potential contribution:		-5.623329818640334E-5
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-HA
 AtomTypes:					CT-H1
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.238980236963774
 current potential contribution:		-0.01626677135687883
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-CB
 AtomTypes:					CT-CT
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				4.6929204642838895
 current potential contribution:		-0.05410565038029724
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-HB1
 AtomTypes:					CT-HC
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				4.606547316309993
 current potential contribution:		-0.01221810486727204
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-HB2
 AtomTypes:					CT-HC
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				5.7095294409710196
 current potential contribution:		-0.0035827736595269786
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-HB3
 AtomTypes:					CT-HC
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				4.720448848054136
 current potential contribution:		-0.010677843352393432
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-C 
 AtomTypes:					CT-C 
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.9079999997313366
 ej:						0.08600000012835884
 current length:				4.787048364635826
 current potential contribution:		-0.04339105377144231
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-O 
 AtomTypes:					CT-O 
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				4.784284804827676
 current potential contribution:		-0.04775556023152229
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-N 
 AtomTypes:					CT-N 
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				5.997553207049491
 current potential contribution:		-0.015373623156934989
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-H 
 AtomTypes:					CT-H 
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				6.232604056070365
 current potential contribution:		-3.511668635402796E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-CH3
 AtomTypes:					CT-CT
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				7.129326259728168
 current potential contribution:		-0.00508475382778595
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-HH31
 AtomTypes:					CT-H1
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				7.265347973879653
 current potential contribution:		-7.181064879139494E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-HH32
 AtomTypes:					CT-H1
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				8.061631976652373
 current potential contribution:		-3.855401511074383E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-HH33
 AtomTypes:					CT-H1
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				7.2237500387962825
 current potential contribution:		-7.43163863387094E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-H 
 AtomTypes:					HC-H 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				2.675743091465705
 current potential contribution:		-0.006273790303978726
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-CA
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				4.104320166499668
 current potential contribution:		-0.02229846371482168
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-HA
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.303965269218218
 current potential contribution:		-0.0026603976117016434
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-CB
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				5.225309312408246
 current potential contribution:		-0.006000741688765236
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-HB1
 AtomTypes:					HC-HC
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				5.28980963506177
 current potential contribution:		-9.759398525158522E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-HB2
 AtomTypes:					HC-HC
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				6.180104045221071
 current potential contribution:		-3.8752136498426226E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-HB3
 AtomTypes:					HC-HC
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				5.319582976038898
 current potential contribution:		-9.441227307596682E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-C 
 AtomTypes:					HC-C 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9079999997313366
 ej:						0.08600000012835884
 current length:				4.844311542294117
 current potential contribution:		-0.008191323796965015
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-O 
 AtomTypes:					HC-O 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				4.770300622233305
 current potential contribution:		-0.009096335049130525
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-N 
 AtomTypes:					HC-N 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				5.994954552141331
 current potential contribution:		-0.002890924987314065
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-H 
 AtomTypes:					HC-H 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				6.268670164318147
 current potential contribution:		-4.272834146735101E-5
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-CH3
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				7.0361074734993725
 current potential contribution:		-0.001039401609865481
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-HH31
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				7.003518368773111
 current potential contribution:		-1.4959406470623727E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-HH32
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				7.988497162219486
 current potential contribution:		-6.801248023012895E-5
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-HH33
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				7.206798206526415
 current potential contribution:		-1.2604388076505177E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-H 
 AtomTypes:					HC-H 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				2.50184763882518
 current potential contribution:		-0.008797791763345272
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-CA
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				4.199356695675863
 current potential contribution:		-0.019912538899467275
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-HA
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.7970708648479095
 current potential contribution:		-0.0014185137745193561
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-CB
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				4.966627196356567
 current potential contribution:		-0.008024542432440595
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-HB1
 AtomTypes:					HC-HC
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				4.664091881480683
 current potential contribution:		-0.002039516391940374
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-HB2
 AtomTypes:					HC-HC
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				5.936206085766695
 current potential contribution:		-4.925773029461941E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-HB3
 AtomTypes:					HC-HC
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				5.160177830637454
 current potential contribution:		-0.0011296800402599578
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-C 
 AtomTypes:					HC-C 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9079999997313366
 ej:						0.08600000012835884
 current length:				4.913833908629687
 current potential contribution:		-0.00755890433651468
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-O 
 AtomTypes:					HC-O 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				4.678697634393349
 current potential contribution:		-0.01016426602295686
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-N 
 AtomTypes:					HC-N 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				6.208171982088087
 current potential contribution:		-0.0023504485822932594
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-H 
 AtomTypes:					HC-H 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				6.591197552984567
 current potential contribution:		-3.1627108435994884E-5
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-CH3
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				7.213188564654918
 current potential contribution:		-8.961823060605773E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-HH31
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				7.371209320510816
 current potential contribution:		-1.1011696695128934E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-HH32
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				8.198069312055827
 current potential contribution:		-5.823410918322402E-5
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-HH33
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				7.142870310128205
 current potential contribution:		-1.3295090085280244E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -HB1
 AtomTypes:					C -HC
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				3.294164057437306
 current potential contribution:		-0.03530007447892526
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -HB2
 AtomTypes:					C -HC
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				4.232531155205322
 current potential contribution:		-0.016966820780156013
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -HB3
 AtomTypes:					C -HC
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				3.1834382569228556
 current potential contribution:		-0.028587945318004376
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -O 
 AtomTypes:					C -O 
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				4.006661784676363
 current potential contribution:		-0.10075336802330875
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -N 
 AtomTypes:					C -N 
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				4.78828596877327
 current potential contribution:		-0.04813327777289765
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -H 
 AtomTypes:					C -H 
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				4.877109632064271
 current potential contribution:		-0.0013464334984427764
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -CH3
 AtomTypes:					C -CT
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				6.067833005166093
 current potential contribution:		-0.011630282857760924
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -HH31
 AtomTypes:					C -H1
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				6.3521076985857245
 current potential contribution:		-0.001417761971376482
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -HH32
 AtomTypes:					C -H1
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				6.904940948701919
 current potential contribution:		-8.626410024180651E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -HH33
 AtomTypes:					C -H1
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				6.256151307318738
 current potential contribution:		-0.0015518804327312916
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -HA
 AtomTypes:					O -H1
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				3.0738234677753127
 current potential contribution:		-0.057281727381445734
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -CB
 AtomTypes:					O -CT
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				3.154731411732784
 current potential contribution:		0.03091543253599449
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -HB1
 AtomTypes:					O -HC
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				3.3416680736238007
 current potential contribution:		-0.05222374407265854
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -HB2
 AtomTypes:					O -HC
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				4.204153622558106
 current potential contribution:		-0.018463347865251157
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -HB3
 AtomTypes:					O -HC
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				2.7374978651277924
 current potential contribution:		0.041630605571105095
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -C 
 AtomTypes:					O -C 
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.9079999997313366
 ej:						0.08600000012835884
 current length:				4.317888158395876
 current potential contribution:		-0.0720646637622916
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -O 
 AtomTypes:					O -O 
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				4.872527494180181
 current potential contribution:		-0.040090666001066526
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -N 
 AtomTypes:					O -N 
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				5.259277656210541
 current potential contribution:		-0.030646706898589206
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -H 
 AtomTypes:					O -H 
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				5.126053865114389
 current potential contribution:		-8.429882020711101E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -CH3
 AtomTypes:					O -CT
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				6.6492793346272965
 current potential contribution:		-0.007164740535540998
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -HH31
 AtomTypes:					O -H1
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				7.028208998626263
 current potential contribution:		-7.617887287041079E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -HH32
 AtomTypes:					O -H1
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				7.369365806742033
 current potential contribution:		-5.736944601478946E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -HH33
 AtomTypes:					O -H1
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				6.915848773116752
 current potential contribution:		-8.388462591024099E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						N -H 
 AtomTypes:					N -H 
 ri:						1.8240000001431576
 ei:						0.169999999917667
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				3.866263327503507
 current potential contribution:		-0.006084968100305705
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						N -CH3
 AtomTypes:					N -CT
 ri:						1.8240000001431576
 ei:						0.169999999917667
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				4.807972316318277
 current potential contribution:		-0.05313059198492091
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						N -HH31
 AtomTypes:					N -H1
 ri:						1.8240000001431576
 ei:						0.169999999917667
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				5.103114448478487
 current potential contribution:		-0.0062136103277688915
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						N -HH32
 AtomTypes:					N -H1
 ri:						1.8240000001431576
 ei:						0.169999999917667
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				5.692131965728181
 current potential contribution:		-0.0032759843070689692
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						N -HH33
 AtomTypes:					N -H1
 ri:						1.8240000001431576
 ei:						0.169999999917667
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.955998404077359
 current potential contribution:		-0.007360244636640153
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -HB1
 AtomTypes:					H -HC
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				3.247913627633991
 current potential contribution:		-0.0021324417284575145
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -HB2
 AtomTypes:					H -HC
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				4.040540303636411
 current potential contribution:		-5.905879940611785E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -HB3
 AtomTypes:					H -HC
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				3.8643744512158666
 current potential contribution:		-7.694273170710309E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -O 
 AtomTypes:					H -O 
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				2.2601714789946805
 current potential contribution:		-0.0574190798297183
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -N 
 AtomTypes:					H -N 
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				3.7396171802021274
 current potential contribution:		-0.007379464963234014
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -H 
 AtomTypes:					H -H 
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				4.238699731671951
 current potential contribution:		-1.6162531889810067E-5
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -CH3
 AtomTypes:					H -CT
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				4.714793469860834
 current potential contribution:		-0.0018566489767475056
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -HH31
 AtomTypes:					H -H1
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.913402248818552
 current potential contribution:		-1.3704779595328004E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -HH32
 AtomTypes:					H -H1
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				5.6992459827685975
 current potential contribution:		-5.6340706656314735E-5
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -HH33
 AtomTypes:					H -H1
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.712292405339381
 current potential contribution:		-1.7599535913061755E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CA-HH31
 AtomTypes:					CT-H1
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.2790367340024575
 current potential contribution:		-0.01547880758256873
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CA-HH32
 AtomTypes:					CT-H1
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.525233109493576
 current potential contribution:		-0.011432562708911463
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CA-HH33
 AtomTypes:					CT-H1
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.091013042655716
 current potential contribution:		-0.019538920951327803
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HA-H 
 AtomTypes:					H1-H 
 ri:						1.3869999991698632
 ei:						0.01570000009846142
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				2.1906839389820365
 current potential contribution:		-0.01261627988403339
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HA-CH3
 AtomTypes:					H1-CT
 ri:						1.3869999991698632
 ei:						0.01570000009846142
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				3.8714404181479147
 current potential contribution:		-0.025517840815885026
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HA-HH31
 AtomTypes:					H1-H1
 ri:						1.3869999991698632
 ei:						0.01570000009846142
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.219929372582585
 current potential contribution:		-0.0024313805218279744
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HA-HH32
 AtomTypes:					H1-H1
 ri:						1.3869999991698632
 ei:						0.01570000009846142
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.479560999930037
 current potential contribution:		-0.0017208146529308155
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HA-HH33
 AtomTypes:					H1-H1
 ri:						1.3869999991698632
 ei:						0.01570000009846142
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.436377702507663
 current potential contribution:		-0.0018206271168735696
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CB-H 
 AtomTypes:					CT-H 
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				2.883862502290621
 current potential contribution:		-0.02810266855705701
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CB-CH3
 AtomTypes:					CT-CT
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				4.400612472771147
 current potential contribution:		-0.07325209684377063
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CB-HH31
 AtomTypes:					CT-H1
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				5.167353408918915
 current potential contribution:		-0.005384725434852054
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CB-HH32
 AtomTypes:					CT-H1
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.91055731925425
 current potential contribution:		-0.007220210304682956
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CB-HH33
 AtomTypes:					CT-H1
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.578645522890268
 current potential contribution:		-0.010713662762549396
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB1-O 
 AtomTypes:					HC-O 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				3.5649793927546045
 current potential contribution:		-0.04154961568109576
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB1-N 
 AtomTypes:					HC-N 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				3.6942402777636483
 current potential contribution:		-0.03967626278164457
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB1-H 
 AtomTypes:					HC-H 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				3.736819994423242
 current potential contribution:		-9.384495062313758E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB1-CH3
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				4.86722925708587
 current potential contribution:		-0.008996591644680437
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB1-HH31
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				5.669923946527397
 current potential contribution:		-5.280657997319705E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB1-HH32
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				5.451723776738775
 current potential contribution:		-6.667469822163462E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB1-HH33
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.805559247577211
 current potential contribution:		-0.0014038968458789578
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB2-O 
 AtomTypes:					HC-O 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				3.617202279359895
 current potential contribution:		-0.03906688263725952
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB2-N 
 AtomTypes:					HC-N 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				2.7360688442602337
 current potential contribution:		0.18503183694284286
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB2-H 
 AtomTypes:					HC-H 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				2.462377011038523
 current potential contribution:		-0.00948225369445906
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB2-CH3
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				3.921804536945718
 current potential contribution:		-0.027542669403131555
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB2-HH31
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.828359049184491
 current potential contribution:		-0.0013654823647770153
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB2-HH32
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.247020366131867
 current potential contribution:		-0.002870608011035763
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB2-HH33
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				4.10201720391112
 current potential contribution:		-0.0034945305225290257
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB3-O 
 AtomTypes:					HC-O 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				4.422111885397896
 current potential contribution:		-0.013978199459163556
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB3-N 
 AtomTypes:					HC-N 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				3.9136494441820484
 current potential contribution:		-0.03093958132982282
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB3-H 
 AtomTypes:					HC-H 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				3.5112175781734605
 current potential contribution:		-0.0013540530394789045
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB3-CH3
 AtomTypes:					HC-CT
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				5.312841617362754
 current potential contribution:		-0.00545158817968109
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB3-HH31
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				6.034104002671551
 current potential contribution:		-3.6444411170472117E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB3-HH32
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				5.7462404782025915
 current potential contribution:		-4.876799633940195E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB3-HH33
 AtomTypes:					HC-H1
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				5.595943727271567
 current potential contribution:		-5.709613940568358E-4
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -HH31
 AtomTypes:					O -H1
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				2.9165137259446263
 current potential contribution:		-0.052134070015331754
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -HH32
 AtomTypes:					O -H1
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				3.7044988527039906
 current potential contribution:		-0.030111630627655414
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -HH33
 AtomTypes:					O -H1
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				2.4900526140884267
 current potential contribution:		0.2637923715945086
}

==============================
VDW14 (41 terms)
==============================
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -H 
 AtomTypes:					O -H 
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				3.0735346136292963
 current potential contribution:		-0.01676568337205455
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -HA
 AtomTypes:					C -H1
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				2.84937561965346
 current potential contribution:		0.03438306312847378
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-O 
 AtomTypes:					HC-O 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				3.132840512481728
 current potential contribution:		-0.057368588800890866
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH33-N 
 AtomTypes:					HC-N 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				2.7433712994824324
 current potential contribution:		0.1741408860043595
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-O 
 AtomTypes:					HC-O 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				3.0489303732435964
 current potential contribution:		-0.054839782156709094
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH32-N 
 AtomTypes:					HC-N 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				2.730117451201063
 current potential contribution:		0.19425392868797753
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-H 
 AtomTypes:					CT-H 
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				2.530385682398355
 current potential contribution:		-0.04133197638447724
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-O 
 AtomTypes:					HC-O 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				2.486082259335306
 current potential contribution:		0.5028305262687758
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HH31-N 
 AtomTypes:					HC-N 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				3.346271953858809
 current potential contribution:		-0.051466325157797144
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -H 
 AtomTypes:					O -H 
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				3.0997037944571844
 current potential contribution:		-0.016002195806479533
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -HH31
 AtomTypes:					C -H1
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				2.891140220806125
 current potential contribution:		0.015410327755418812
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -HH32
 AtomTypes:					C -H1
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				3.3114007743545844
 current potential contribution:		-0.03671341181109258
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -HH33
 AtomTypes:					C -H1
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				2.630799333223469
 current potential contribution:		0.2638501405185267
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB3-C 
 AtomTypes:					HC-C 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9079999997313366
 ej:						0.08600000012835884
 current length:				3.471811762234163
 current potential contribution:		-0.036165243130594334
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB2-C 
 AtomTypes:					HC-C 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9079999997313366
 ej:						0.08600000012835884
 current length:				2.657304200854422
 current potential contribution:		0.3753842903233601
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HB1-C 
 AtomTypes:					HC-C 
 ri:						1.4869999997254757
 ei:						0.01570000002623629
 rj:						1.9079999997313366
 ej:						0.08600000012835884
 current length:				2.969798767465494
 current potential contribution:		0.019019263245646706
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HA-HB1
 AtomTypes:					H1-HC
 ri:						1.3869999991698632
 ei:						0.01570000009846142
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				3.0849757314567534
 current potential contribution:		-0.013817715070104267
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HA-HB2
 AtomTypes:					H1-HC
 ri:						1.3869999991698632
 ei:						0.01570000009846142
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				2.6820652651237036
 current potential contribution:		-0.011553323623250893
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HA-HB3
 AtomTypes:					H1-HC
 ri:						1.3869999991698632
 ei:						0.01570000009846142
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				2.487806088150186
 current potential contribution:		0.014067389539011196
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HA-O 
 AtomTypes:					H1-O 
 ri:						1.3869999991698632
 ei:						0.01570000009846142
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				3.0513401572584233
 current potential contribution:		-0.057417331081814904
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						HA-N 
 AtomTypes:					H1-N 
 ri:						1.3869999991698632
 ei:						0.01570000009846142
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				2.448093365880993
 current potential contribution:		0.8133247528071571
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CA-H 
 AtomTypes:					CT-H 
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						0.5999999999731258
 ej:						0.015700000004219245
 current length:				2.4849872597259006
 current potential contribution:		-0.041309675448105154
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -HA
 AtomTypes:					H -H1
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				2.8238503749160757
 current potential contribution:		-0.0035799450131072524
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -CB
 AtomTypes:					H -CT
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				3.281866039154961
 current potential contribution:		-0.014865157311792283
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -C 
 AtomTypes:					H -C 
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.9079999997313366
 ej:						0.08600000012835884
 current length:				2.438289749312608
 current potential contribution:		-0.03549727749188333
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						N -HB1
 AtomTypes:					N -HC
 ri:						1.8240000001431576
 ei:						0.169999999917667
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				2.764976443314303
 current potential contribution:		0.14448896778566866
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						N -HB2
 AtomTypes:					N -HC
 ri:						1.8240000001431576
 ei:						0.169999999917667
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				3.495391082200002
 current potential contribution:		-0.04768130163611003
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						N -HB3
 AtomTypes:					N -HC
 ri:						1.8240000001431576
 ei:						0.169999999917667
 rj:						1.4869999997254757
 ej:						0.01570000002623629
 current length:				3.0209715186151374
 current potential contribution:		-0.023881546517435237
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -HH31
 AtomTypes:					H -H1
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				2.8647407214152585
 current potential contribution:		-0.003301621552297995
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -HH32
 AtomTypes:					H -H1
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				2.4724199482912708
 current potential contribution:		-0.0073205106268455535
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						H -HH33
 AtomTypes:					H -H1
 ri:						0.5999999999731258
 ei:						0.015700000004219245
 rj:						1.3869999991698632
 ej:						0.01570000009846142
 current length:				3.0153910302888898
 current potential contribution:		-0.002465488815496201
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -CA
 AtomTypes:					O -CT
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				2.870942775104311
 current potential contribution:		0.94696196504175
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -CB
 AtomTypes:					C -CT
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				3.202675430387265
 current potential contribution:		0.23906479864967112
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						C -C 
 AtomTypes:					C -C 
 ri:						1.9079999997313366
 ei:						0.08600000012835884
 rj:						1.9079999997313366
 ej:						0.08600000012835884
 current length:				3.6770394131149073
 current potential contribution:		-0.08065582936347068
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CH3-CA
 AtomTypes:					CT-CT
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				3.8534674306534162
 current potential contribution:		-0.10904532657259079
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						O -CH3
 AtomTypes:					O -CT
 ri:						1.661200000277935
 ei:						0.20999999984182244
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				2.6544667054066733
 current potential contribution:		3.502011555299573
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CB-O 
 AtomTypes:					CT-O 
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				3.4589680460272354
 current potential contribution:		-0.14507030898119055
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CB-N 
 AtomTypes:					CT-N 
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				3.068011307850882
 current potential contribution:		0.547729794840901
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						CA-CH3
 AtomTypes:					CT-CT
 ri:						1.9080000000152688
 ei:						0.10939999991572773
 rj:						1.9080000000152688
 ej:						0.10939999991572773
 current length:				3.792479333829134
 current potential contribution:		-0.10924374030056654
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						N -O 
 AtomTypes:					N -O 
 ri:						1.8240000001431576
 ei:						0.169999999917667
 rj:						1.661200000277935
 ej:						0.20999999984182244
 current length:				2.6980621539169816
 current potential contribution:		2.3223900154063344
}
name.mjw.jamber.LJTwelveSix Object {
 Atoms:						N -N 
 AtomTypes:					N -N 
 ri:						1.8240000001431576
 ei:						0.169999999917667
 rj:						1.8240000001431576
 ej:						0.169999999917667
 current length:				3.604184468349173
 current potential contribution:		-0.16903879138813688
}

==============================
EE (133 terms)
==============================
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-H 
 AtomTypes:					HC-H 
 charge on i:					0.1123
 charge on j:					0.27190000000000003
 current length:				3.590019274986246
 current potential contribution:		2.8242202856689245
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-CA
 AtomTypes:					HC-CT
 charge on i:					0.1123
 charge on j:					0.0337
 current length:				4.653085612923427
 current potential contribution:		0.270069163009553
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-HA
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0823
 current length:				4.94381770197836
 current potential contribution:		0.6207597189923753
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-CB
 AtomTypes:					HC-CT
 charge on i:					0.1123
 charge on j:					-0.1825
 current length:				5.3102994080418995
 current potential contribution:		-1.2815336118719052
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-HB1
 AtomTypes:					HC-HC
 charge on i:					0.1123
 charge on j:					0.06029999999999999
 current length:				5.225818071063303
 current potential contribution:		0.4302780251419508
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-HB2
 AtomTypes:					HC-HC
 charge on i:					0.1123
 charge on j:					0.06029999999999999
 current length:				6.36751842056723
 current potential contribution:		0.3531288848894969
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-HB3
 AtomTypes:					HC-HC
 charge on i:					0.1123
 charge on j:					0.06029999999999999
 current length:				5.131039918982901
 current potential contribution:		0.43822591811251316
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-C 
 AtomTypes:					HC-C 
 charge on i:					0.1123
 charge on j:					0.5973000005487781
 current length:				5.745761441724308
 current potential contribution:		3.876422140952298
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-O 
 AtomTypes:					HC-O 
 charge on i:					0.1123
 charge on j:					-0.5679000016463344
 current length:				5.839354126961472
 current potential contribution:		-3.626546054262808
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-N 
 AtomTypes:					HC-N 
 charge on i:					0.1123
 charge on j:					-0.4157
 current length:				6.909518161959922
 current potential contribution:		-2.243460371998327
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-H 
 AtomTypes:					HC-H 
 charge on i:					0.1123
 charge on j:					0.27190000000000003
 current length:				7.036381888618735
 current potential contribution:		1.4409401625512004
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-CH3
 AtomTypes:					HC-CT
 charge on i:					0.1123
 charge on j:					-0.149
 current length:				8.112349130172817
 current potential contribution:		-0.6848978079296925
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-HH31
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0976
 current length:				8.271290829596037
 current potential contribution:		0.4400101219569923
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-HH32
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0976
 current length:				9.005850272335392
 current potential contribution:		0.404120829973395
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-HH33
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0976
 current length:				8.246023058796695
 current potential contribution:		0.4413584173512356
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-HA
 AtomTypes:					CT-H1
 charge on i:					-0.3662
 charge on j:					0.0823
 current length:				4.238980236963774
 current potential contribution:		-2.3608215889517536
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-CB
 AtomTypes:					CT-CT
 charge on i:					-0.3662
 charge on j:					-0.1825
 current length:				4.6929204642838895
 current potential contribution:		4.7287287158128395
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-HB1
 AtomTypes:					CT-HC
 charge on i:					-0.3662
 charge on j:					0.06029999999999999
 current length:				4.606547316309993
 current potential contribution:		-1.5917193662913753
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-HB2
 AtomTypes:					CT-HC
 charge on i:					-0.3662
 charge on j:					0.06029999999999999
 current length:				5.7095294409710196
 current potential contribution:		-1.2842267740126003
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-HB3
 AtomTypes:					CT-HC
 charge on i:					-0.3662
 charge on j:					0.06029999999999999
 current length:				4.720448848054136
 current potential contribution:		-1.5533121555020584
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-C 
 AtomTypes:					CT-C 
 charge on i:					-0.3662
 charge on j:					0.5973000005487781
 current length:				4.787048364635826
 current potential contribution:		-15.172230236716688
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-O 
 AtomTypes:					CT-O 
 charge on i:					-0.3662
 charge on j:					-0.5679000016463344
 current length:				4.784284804827676
 current potential contribution:		14.433762985903096
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-N 
 AtomTypes:					CT-N 
 charge on i:					-0.3662
 charge on j:					-0.4157
 current length:				5.997553207049491
 current potential contribution:		8.428118624391967
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-H 
 AtomTypes:					CT-H 
 charge on i:					-0.3662
 charge on j:					0.27190000000000003
 current length:				6.232604056070365
 current potential contribution:		-5.304743559455132
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-CH3
 AtomTypes:					CT-CT
 charge on i:					-0.3662
 charge on j:					-0.149
 current length:				7.129326259728168
 current potential contribution:		2.541338425856325
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-HH31
 AtomTypes:					CT-H1
 charge on i:					-0.3662
 charge on j:					0.0976
 current length:				7.265347973879653
 current potential contribution:		-1.6334961776714434
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-HH32
 AtomTypes:					CT-H1
 charge on i:					-0.3662
 charge on j:					0.0976
 current length:				8.061631976652373
 current potential contribution:		-1.4721483415711047
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-HH33
 AtomTypes:					CT-H1
 charge on i:					-0.3662
 charge on j:					0.0976
 current length:				7.2237500387962825
 current potential contribution:		-1.6429026587363713
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-H 
 AtomTypes:					HC-H 
 charge on i:					0.1123
 charge on j:					0.27190000000000003
 current length:				2.675743091465705
 current potential contribution:		3.7892297263877857
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-CA
 AtomTypes:					HC-CT
 charge on i:					0.1123
 charge on j:					0.0337
 current length:				4.104320166499668
 current potential contribution:		0.30617858400792103
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-HA
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0823
 current length:				4.303965269218218
 current potential contribution:		0.7130454581914094
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-CB
 AtomTypes:					HC-CT
 charge on i:					0.1123
 charge on j:					-0.1825
 current length:				5.225309312408246
 current potential contribution:		-1.302377863899645
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-HB1
 AtomTypes:					HC-HC
 charge on i:					0.1123
 charge on j:					0.06029999999999999
 current length:				5.28980963506177
 current potential contribution:		0.425072891936305
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-HB2
 AtomTypes:					HC-HC
 charge on i:					0.1123
 charge on j:					0.06029999999999999
 current length:				6.180104045221071
 current potential contribution:		0.36383767375356585
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-HB3
 AtomTypes:					HC-HC
 charge on i:					0.1123
 charge on j:					0.06029999999999999
 current length:				5.319582976038898
 current potential contribution:		0.4226937881214459
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-C 
 AtomTypes:					HC-C 
 charge on i:					0.1123
 charge on j:					0.5973000005487781
 current length:				4.844311542294117
 current potential contribution:		4.59776310315135
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-O 
 AtomTypes:					HC-O 
 charge on i:					0.1123
 charge on j:					-0.5679000016463344
 current length:				4.770300622233305
 current potential contribution:		-4.4392771746660085
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-N 
 AtomTypes:					HC-N 
 charge on i:					0.1123
 charge on j:					-0.4157
 current length:				5.994954552141331
 current potential contribution:		-2.5857127107699154
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-H 
 AtomTypes:					HC-H 
 charge on i:					0.1123
 charge on j:					0.27190000000000003
 current length:				6.268670164318147
 current potential contribution:		1.6174092744695296
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-CH3
 AtomTypes:					HC-CT
 charge on i:					0.1123
 charge on j:					-0.149
 current length:				7.0361074734993725
 current potential contribution:		-0.7896596459537017
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-HH31
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0976
 current length:				7.003518368773111
 current potential contribution:		0.519660475640313
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-HH32
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0976
 current length:				7.988497162219486
 current potential contribution:		0.4555865280749673
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-HH33
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0976
 current length:				7.206798206526415
 current potential contribution:		0.5050025798386374
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-H 
 AtomTypes:					HC-H 
 charge on i:					0.1123
 charge on j:					0.27190000000000003
 current length:				2.50184763882518
 current potential contribution:		4.052607003326424
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-CA
 AtomTypes:					HC-CT
 charge on i:					0.1123
 charge on j:					0.0337
 current length:				4.199356695675863
 current potential contribution:		0.2992493917432683
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-HA
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0823
 current length:				4.7970708648479095
 current potential contribution:		0.6397493332687961
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-CB
 AtomTypes:					HC-CT
 charge on i:					0.1123
 charge on j:					-0.1825
 current length:				4.966627196356567
 current potential contribution:		-1.3702109925829438
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-HB1
 AtomTypes:					HC-HC
 charge on i:					0.1123
 charge on j:					0.06029999999999999
 current length:				4.664091881480683
 current potential contribution:		0.48209913880478716
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-HB2
 AtomTypes:					HC-HC
 charge on i:					0.1123
 charge on j:					0.06029999999999999
 current length:				5.936206085766695
 current potential contribution:		0.378786492059233
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-HB3
 AtomTypes:					HC-HC
 charge on i:					0.1123
 charge on j:					0.06029999999999999
 current length:				5.160177830637454
 current potential contribution:		0.4357513932984874
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-C 
 AtomTypes:					HC-C 
 charge on i:					0.1123
 charge on j:					0.5973000005487781
 current length:				4.913833908629687
 current potential contribution:		4.532712599466215
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-O 
 AtomTypes:					HC-O 
 charge on i:					0.1123
 charge on j:					-0.5679000016463344
 current length:				4.678697634393349
 current potential contribution:		-4.526192612427964
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-N 
 AtomTypes:					HC-N 
 charge on i:					0.1123
 charge on j:					-0.4157
 current length:				6.208171982088087
 current potential contribution:		-2.4969073393398555
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-H 
 AtomTypes:					HC-H 
 charge on i:					0.1123
 charge on j:					0.27190000000000003
 current length:				6.591197552984567
 current potential contribution:		1.5382645082102795
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-CH3
 AtomTypes:					HC-CT
 charge on i:					0.1123
 charge on j:					-0.149
 current length:				7.213188564654918
 current potential contribution:		-0.7702737959244681
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-HH31
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0976
 current length:				7.371209320510816
 current potential contribution:		0.49373875146176077
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-HH32
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0976
 current length:				8.198069312055827
 current potential contribution:		0.44394009712997173
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-HH33
 AtomTypes:					HC-H1
 charge on i:					0.1123
 charge on j:					0.0976
 current length:				7.142870310128205
 current potential contribution:		0.5095222968715752
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -HB1
 AtomTypes:					C -HC
 charge on i:					0.5972000021951126
 charge on j:					0.06029999999999999
 current length:				3.294164057437306
 current potential contribution:		3.6299301920013223
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -HB2
 AtomTypes:					C -HC
 charge on i:					0.5972000021951126
 charge on j:					0.06029999999999999
 current length:				4.232531155205322
 current potential contribution:		2.8251618549319777
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -HB3
 AtomTypes:					C -HC
 charge on i:					0.5972000021951126
 charge on j:					0.06029999999999999
 current length:				3.1834382569228556
 current potential contribution:		3.7561857980106015
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -O 
 AtomTypes:					C -O 
 charge on i:					0.5972000021951126
 charge on j:					-0.5679000016463344
 current length:				4.006661784676363
 current potential contribution:		-28.107056807774423
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -N 
 AtomTypes:					C -N 
 charge on i:					0.5972000021951126
 charge on j:					-0.4157
 current length:				4.78828596877327
 current potential contribution:		-17.215757241883573
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -H 
 AtomTypes:					C -H 
 charge on i:					0.5972000021951126
 charge on j:					0.27190000000000003
 current length:				4.877109632064271
 current potential contribution:		11.055359629390091
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -CH3
 AtomTypes:					C -CT
 charge on i:					0.5972000021951126
 charge on j:					-0.149
 current length:				6.067833005166093
 current potential contribution:		-4.869437923721925
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -HH31
 AtomTypes:					C -H1
 charge on i:					0.5972000021951126
 charge on j:					0.0976
 current length:				6.3521076985857245
 current potential contribution:		3.0468996441280853
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -HH32
 AtomTypes:					C -H1
 charge on i:					0.5972000021951126
 charge on j:					0.0976
 current length:				6.904940948701919
 current potential contribution:		2.802954410482334
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -HH33
 AtomTypes:					C -H1
 charge on i:					0.5972000021951126
 charge on j:					0.0976
 current length:				6.256151307318738
 current potential contribution:		3.0936327680634306
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -HA
 AtomTypes:					O -H1
 charge on i:					-0.5679000016463344
 charge on j:					0.0823
 current length:				3.0738234677753127
 current potential contribution:		-5.048927886957505
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -CB
 AtomTypes:					O -CT
 charge on i:					-0.5679000016463344
 charge on j:					-0.1825
 current length:				3.154731411732784
 current potential contribution:		10.908844050599125
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -HB1
 AtomTypes:					O -HC
 charge on i:					-0.5679000016463344
 charge on j:					0.06029999999999999
 current length:				3.3416680736238007
 current potential contribution:		-3.402767351807862
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -HB2
 AtomTypes:					O -HC
 charge on i:					-0.5679000016463344
 charge on j:					0.06029999999999999
 current length:				4.204153622558106
 current potential contribution:		-2.704686850759479
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -HB3
 AtomTypes:					O -HC
 charge on i:					-0.5679000016463344
 charge on j:					0.06029999999999999
 current length:				2.7374978651277924
 current potential contribution:		-4.153763612515154
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -C 
 AtomTypes:					O -C 
 charge on i:					-0.5679000016463344
 charge on j:					0.5973000005487781
 current length:				4.317888158395876
 current potential contribution:		-26.085512916180672
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -O 
 AtomTypes:					O -O 
 charge on i:					-0.5679000016463344
 charge on j:					-0.5679000016463344
 current length:				4.872527494180181
 current potential contribution:		21.97838749840795
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -N 
 AtomTypes:					O -N 
 charge on i:					-0.5679000016463344
 charge on j:					-0.4157
 current length:				5.259277656210541
 current potential contribution:		14.905006840162445
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -H 
 AtomTypes:					O -H 
 charge on i:					-0.5679000016463344
 charge on j:					0.27190000000000003
 current length:				5.126053865114389
 current potential contribution:		-10.00240180771977
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -CH3
 AtomTypes:					O -CT
 charge on i:					-0.5679000016463344
 charge on j:					-0.149
 current length:				6.6492793346272965
 current potential contribution:		4.225615189848902
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -HH31
 AtomTypes:					O -H1
 charge on i:					-0.5679000016463344
 charge on j:					0.0976
 current length:				7.028208998626263
 current potential contribution:		-2.6186858682387038
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -HH32
 AtomTypes:					O -H1
 charge on i:					-0.5679000016463344
 charge on j:					0.0976
 current length:				7.369365806742033
 current potential contribution:		-2.4974566423195808
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -HH33
 AtomTypes:					O -H1
 charge on i:					-0.5679000016463344
 charge on j:					0.0976
 current length:				6.915848773116752
 current potential contribution:		-2.6612310632460936
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						N -H 
 AtomTypes:					N -H 
 charge on i:					-0.4157
 charge on j:					0.27190000000000003
 current length:				3.866263327503507
 current potential contribution:		-9.707428191281345
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						N -CH3
 AtomTypes:					N -CT
 charge on i:					-0.4157
 charge on j:					-0.149
 current length:				4.807972316318277
 current potential contribution:		4.277703894676168
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						N -HH31
 AtomTypes:					N -H1
 charge on i:					-0.4157
 charge on j:					0.0976
 current length:				5.103114448478487
 current potential contribution:		-2.6399817117607274
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						N -HH32
 AtomTypes:					N -H1
 charge on i:					-0.4157
 charge on j:					0.0976
 current length:				5.692131965728181
 current potential contribution:		-2.366798397879674
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						N -HH33
 AtomTypes:					N -H1
 charge on i:					-0.4157
 charge on j:					0.0976
 current length:				4.955998404077359
 current potential contribution:		-2.7183480942853926
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -HB1
 AtomTypes:					H -HC
 charge on i:					0.27190000000000003
 charge on j:					0.06029999999999999
 current length:				3.247913627633991
 current potential contribution:		1.6762100217434932
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -HB2
 AtomTypes:					H -HC
 charge on i:					0.27190000000000003
 charge on j:					0.06029999999999999
 current length:				4.040540303636411
 current potential contribution:		1.3473904387236764
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -HB3
 AtomTypes:					H -HC
 charge on i:					0.27190000000000003
 charge on j:					0.06029999999999999
 current length:				3.8643744512158666
 current potential contribution:		1.4088141408461154
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -O 
 AtomTypes:					H -O 
 charge on i:					0.27190000000000003
 charge on j:					-0.5679000016463344
 current length:				2.2601714789946805
 current potential contribution:		-22.685380699386194
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -N 
 AtomTypes:					H -N 
 charge on i:					0.27190000000000003
 charge on j:					-0.4157
 current length:				3.7396171802021274
 current potential contribution:		-10.036180660154143
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -H 
 AtomTypes:					H -H 
 charge on i:					0.27190000000000003
 charge on j:					0.27190000000000003
 current length:				4.238699731671951
 current potential contribution:		5.791514492332565
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -CH3
 AtomTypes:					H -CT
 charge on i:					0.27190000000000003
 charge on j:					-0.149
 current length:				4.714793469860834
 current potential contribution:		-2.8532458040190054
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -HH31
 AtomTypes:					H -H1
 charge on i:					0.27190000000000003
 charge on j:					0.0976
 current length:				4.913402248818552
 current potential contribution:		1.7934244637887222
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -HH32
 AtomTypes:					H -H1
 charge on i:					0.27190000000000003
 charge on j:					0.0976
 current length:				5.6992459827685975
 current potential contribution:		1.5461371241227042
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -HH33
 AtomTypes:					H -H1
 charge on i:					0.27190000000000003
 charge on j:					0.0976
 current length:				4.712292405339381
 current potential contribution:		1.8699637109703262
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CA-HH31
 AtomTypes:					CT-H1
 charge on i:					0.0337
 charge on j:					0.0976
 current length:				4.2790367340024575
 current potential contribution:		0.25523491777650836
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CA-HH32
 AtomTypes:					CT-H1
 charge on i:					0.0337
 charge on j:					0.0976
 current length:				4.525233109493576
 current potential contribution:		0.24134880182736063
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CA-HH33
 AtomTypes:					CT-H1
 charge on i:					0.0337
 charge on j:					0.0976
 current length:				4.091013042655716
 current potential contribution:		0.266965560260544
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HA-H 
 AtomTypes:					H1-H 
 charge on i:					0.0823
 charge on j:					0.27190000000000003
 current length:				2.1906839389820365
 current potential contribution:		3.3918426997256725
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HA-CH3
 AtomTypes:					H1-CT
 charge on i:					0.0823
 charge on j:					-0.149
 current length:				3.8714404181479147
 current potential contribution:		-1.0517678913505464
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HA-HH31
 AtomTypes:					H1-H1
 charge on i:					0.0823
 charge on j:					0.0976
 current length:				4.219929372582585
 current potential contribution:		0.6320491550752148
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HA-HH32
 AtomTypes:					H1-H1
 charge on i:					0.0823
 charge on j:					0.0976
 current length:				4.479560999930037
 current potential contribution:		0.5954161120832067
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HA-HH33
 AtomTypes:					H1-H1
 charge on i:					0.0823
 charge on j:					0.0976
 current length:				4.436377702507663
 current potential contribution:		0.6012118384127364
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CB-H 
 AtomTypes:					CT-H 
 charge on i:					-0.1825
 charge on j:					0.27190000000000003
 current length:				2.883862502290621
 current potential contribution:		-5.713522090848217
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CB-CH3
 AtomTypes:					CT-CT
 charge on i:					-0.1825
 charge on j:					-0.149
 current length:				4.400612472771147
 current potential contribution:		2.0518348240840014
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CB-HH31
 AtomTypes:					CT-H1
 charge on i:					-0.1825
 charge on j:					0.0976
 current length:				5.167353408918915
 current potential contribution:		-1.1445925266770174
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CB-HH32
 AtomTypes:					CT-H1
 charge on i:					-0.1825
 charge on j:					0.0976
 current length:				4.91055731925425
 current potential contribution:		-1.204448641981399
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CB-HH33
 AtomTypes:					CT-H1
 charge on i:					-0.1825
 charge on j:					0.0976
 current length:				4.578645522890268
 current potential contribution:		-1.2917606451468788
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB1-O 
 AtomTypes:					HC-O 
 charge on i:					0.06029999999999999
 charge on j:					-0.5679000016463344
 current length:				3.5649793927546045
 current potential contribution:		-3.189617040877088
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB1-N 
 AtomTypes:					HC-N 
 charge on i:					0.06029999999999999
 charge on j:					-0.4157
 current length:				3.6942402777636483
 current potential contribution:		-2.2530902188513804
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB1-H 
 AtomTypes:					HC-H 
 charge on i:					0.06029999999999999
 charge on j:					0.27190000000000003
 current length:				3.736819994423242
 current potential contribution:		1.456903297595859
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB1-CH3
 AtomTypes:					HC-CT
 charge on i:					0.06029999999999999
 charge on j:					-0.149
 current length:				4.86722925708587
 current potential contribution:		-0.6129543933916806
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB1-HH31
 AtomTypes:					HC-H1
 charge on i:					0.06029999999999999
 charge on j:					0.0976
 current length:				5.669923946527397
 current potential contribution:		0.3446642833063442
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB1-HH32
 AtomTypes:					HC-H1
 charge on i:					0.06029999999999999
 charge on j:					0.0976
 current length:				5.451723776738775
 current potential contribution:		0.3584591504378015
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB1-HH33
 AtomTypes:					HC-H1
 charge on i:					0.06029999999999999
 charge on j:					0.0976
 current length:				4.805559247577211
 current potential contribution:		0.40665824158064245
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB2-O 
 AtomTypes:					HC-O 
 charge on i:					0.06029999999999999
 charge on j:					-0.5679000016463344
 current length:				3.617202279359895
 current potential contribution:		-3.143567360440222
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB2-N 
 AtomTypes:					HC-N 
 charge on i:					0.06029999999999999
 charge on j:					-0.4157
 current length:				2.7360688442602337
 current potential contribution:		-3.0421225158048033
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB2-H 
 AtomTypes:					HC-H 
 charge on i:					0.06029999999999999
 charge on j:					0.27190000000000003
 current length:				2.462377011038523
 current potential contribution:		2.2109471246652195
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB2-CH3
 AtomTypes:					HC-CT
 charge on i:					0.06029999999999999
 charge on j:					-0.149
 current length:				3.921804536945718
 current potential contribution:		-0.7607185744903947
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB2-HH31
 AtomTypes:					HC-H1
 charge on i:					0.06029999999999999
 charge on j:					0.0976
 current length:				4.828359049184491
 current potential contribution:		0.4047379769243572
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB2-HH32
 AtomTypes:					HC-H1
 charge on i:					0.06029999999999999
 charge on j:					0.0976
 current length:				4.247020366131867
 current potential contribution:		0.4601391340186163
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB2-HH33
 AtomTypes:					HC-H1
 charge on i:					0.06029999999999999
 charge on j:					0.0976
 current length:				4.10201720391112
 current potential contribution:		0.47640469951419706
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB3-O 
 AtomTypes:					HC-O 
 charge on i:					0.06029999999999999
 charge on j:					-0.5679000016463344
 current length:				4.422111885397896
 current potential contribution:		-2.571377503824194
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB3-N 
 AtomTypes:					HC-N 
 charge on i:					0.06029999999999999
 charge on j:					-0.4157
 current length:				3.9136494441820484
 current potential contribution:		-2.1267762365097784
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB3-H 
 AtomTypes:					HC-H 
 charge on i:					0.06029999999999999
 charge on j:					0.27190000000000003
 current length:				3.5112175781734605
 current potential contribution:		1.5505121090301193
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB3-CH3
 AtomTypes:					HC-CT
 charge on i:					0.06029999999999999
 charge on j:					-0.149
 current length:				5.312841617362754
 current potential contribution:		-0.5615431009698039
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB3-HH31
 AtomTypes:					HC-H1
 charge on i:					0.06029999999999999
 charge on j:					0.0976
 current length:				6.034104002671551
 current potential contribution:		0.32386254406058107
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB3-HH32
 AtomTypes:					HC-H1
 charge on i:					0.06029999999999999
 charge on j:					0.0976
 current length:				5.7462404782025915
 current potential contribution:		0.34008675426034707
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB3-HH33
 AtomTypes:					HC-H1
 charge on i:					0.06029999999999999
 charge on j:					0.0976
 current length:				5.595943727271567
 current potential contribution:		0.3492208586565201
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -HH31
 AtomTypes:					O -H1
 charge on i:					-0.5679000016463344
 charge on j:					0.0976
 current length:				2.9165137259446263
 current potential contribution:		-6.310504017178804
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -HH32
 AtomTypes:					O -H1
 charge on i:					-0.5679000016463344
 charge on j:					0.0976
 current length:				3.7044988527039906
 current potential contribution:		-4.968194704743055
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -HH33
 AtomTypes:					O -H1
 charge on i:					-0.5679000016463344
 charge on j:					0.0976
 current length:				2.4900526140884267
 current potential contribution:		-7.39127819211498
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}

==============================
EE14 (41 terms)
==============================
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -H 
 AtomTypes:					O -H 
 charge on i:					-0.5679000016463344
 charge on j:					0.27190000000000003
 current length:				3.0735346136292963
 current potential contribution:		-16.68204750957562
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -HA
 AtomTypes:					C -H1
 charge on i:					0.5972000021951126
 charge on j:					0.0823
 current length:				2.84937561965346
 current potential contribution:		5.72764795372681
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-O 
 AtomTypes:					HC-O 
 charge on i:					0.1123
 charge on j:					-0.5679000016463344
 current length:				3.132840512481728
 current potential contribution:		-6.759580190630237
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH33-N 
 AtomTypes:					HC-N 
 charge on i:					0.1123
 charge on j:					-0.4157
 current length:				2.7433712994824324
 current potential contribution:		-5.65043098208554
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH33
 amberAtomType : HC
 x, y, z :       1.3951117 2.5823496 -0.5897169
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-O 
 AtomTypes:					HC-O 
 charge on i:					0.1123
 charge on j:					-0.5679000016463344
 current length:				3.0489303732435964
 current potential contribution:		-6.945611764182928
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH32-N 
 AtomTypes:					HC-N 
 charge on i:					0.1123
 charge on j:					-0.4157
 current length:				2.730117451201063
 current potential contribution:		-5.677862019870366
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH32
 amberAtomType : HC
 x, y, z :       1.8171406 2.5069901 1.1277959
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-H 
 AtomTypes:					CT-H 
 charge on i:					-0.3662
 charge on j:					0.27190000000000003
 current length:				2.530385682398355
 current potential contribution:		-13.06613709327345
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-O 
 AtomTypes:					HC-O 
 charge on i:					0.1123
 charge on j:					-0.5679000016463344
 current length:				2.486082259335306
 current potential contribution:		-8.518095726340647
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HH31-N 
 AtomTypes:					HC-N 
 charge on i:					0.1123
 charge on j:					-0.4157
 current length:				3.346271953858809
 current potential contribution:		-4.6323880424854
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.1123
 name :          HH31
 amberAtomType : HC
 x, y, z :       2.0773821 1.0381474 0.1406479
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -H 
 AtomTypes:					O -H 
 charge on i:					-0.5679000016463344
 charge on j:					0.27190000000000003
 current length:				3.0997037944571844
 current potential contribution:		-16.54120969189829
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -HH31
 AtomTypes:					C -H1
 charge on i:					0.5973000005487781
 charge on j:					0.0976
 current length:				2.891140220806125
 current potential contribution:		6.695446773305482
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -HH32
 AtomTypes:					C -H1
 charge on i:					0.5973000005487781
 charge on j:					0.0976
 current length:				3.3114007743545844
 current potential contribution:		5.845706026430153
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -HH33
 AtomTypes:					C -H1
 charge on i:					0.5973000005487781
 charge on j:					0.0976
 current length:				2.630799333223469
 current potential contribution:		7.358020514187875
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB3-C 
 AtomTypes:					HC-C 
 charge on i:					0.06029999999999999
 charge on j:					0.5973000005487781
 current length:				3.471811762234163
 current potential contribution:		3.444768504300261
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB2-C 
 AtomTypes:					HC-C 
 charge on i:					0.06029999999999999
 charge on j:					0.5973000005487781
 current length:				2.657304200854422
 current potential contribution:		4.500646861416159
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HB1-C 
 AtomTypes:					HC-C 
 charge on i:					0.06029999999999999
 charge on j:					0.5973000005487781
 current length:				2.969798767465494
 current potential contribution:		4.027070097281394
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HA-HB1
 AtomTypes:					H1-HC
 charge on i:					0.0823
 charge on j:					0.06029999999999999
 current length:				3.0849757314567534
 current potential contribution:		0.5341605126645184
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HA-HB2
 AtomTypes:					H1-HC
 charge on i:					0.0823
 charge on j:					0.06029999999999999
 current length:				2.6820652651237036
 current potential contribution:		0.6144042203971248
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HA-HB3
 AtomTypes:					H1-HC
 charge on i:					0.0823
 charge on j:					0.06029999999999999
 current length:				2.487806088150186
 current potential contribution:		0.6623796871153316
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HA-O 
 AtomTypes:					H1-O 
 charge on i:					0.0823
 charge on j:					-0.5679000016463344
 current length:				3.0513401572584233
 current potential contribution:		-5.086130102249634
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						HA-N 
 AtomTypes:					H1-N 
 charge on i:					0.0823
 charge on j:					-0.4157
 current length:				2.448093365880993
 current potential contribution:		-4.640430443682574
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CA-H 
 AtomTypes:					CT-H 
 charge on i:					0.0337
 charge on j:					0.27190000000000003
 current length:				2.4849872597259006
 current potential contribution:		1.2243943773868784
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -HA
 AtomTypes:					H -H1
 charge on i:					0.27190000000000003
 charge on j:					0.0823
 current length:				2.8238503749160757
 current potential contribution:		2.6313204806622705
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0823
 name :          HA
 amberAtomType : H1
 x, y, z :       5.6443838 4.4207961 0.665548
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -CB
 AtomTypes:					H -CT
 charge on i:					0.27190000000000003
 charge on j:					-0.1825
 current length:				3.281866039154961
 current potential contribution:		-5.020622998386887
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -C 
 AtomTypes:					H -C 
 charge on i:					0.27190000000000003
 charge on j:					0.5973000005487781
 current length:				2.438289749312608
 current potential contribution:		22.116825656501618
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       2.9717152 4.4937428 -0.2431061
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						N -HB1
 AtomTypes:					N -HC
 charge on i:					-0.4157
 charge on j:					0.06029999999999999
 current length:				2.764976443314303
 current potential contribution:		-3.010317377582783
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB1
 amberAtomType : HC
 x, y, z :       5.3814421 4.1908541 -2.3995888
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						N -HB2
 AtomTypes:					N -HC
 charge on i:					-0.4157
 charge on j:					0.06029999999999999
 current length:				3.495391082200002
 current potential contribution:		-2.381266198881898
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB2
 amberAtomType : HC
 x, y, z :       6.692539 5.0048201 -1.7331518
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						N -HB3
 AtomTypes:					N -HC
 charge on i:					-0.4157
 charge on j:					0.06029999999999999
 current length:				3.0209715186151374
 current potential contribution:		-2.7552251269590555
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.06029999999999999
 name :          HB3
 amberAtomType : HC
 x, y, z :       6.4508632 3.251242 -1.3767334
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -HH31
 AtomTypes:					H -H1
 charge on i:					0.27190000000000003
 charge on j:					0.0976
 current length:				2.8647407214152585
 current potential contribution:		3.0759557846171997
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH31
 amberAtomType : H1
 x, y, z :       5.2797388 8.5945068 1.1705004
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -HH32
 AtomTypes:					H -H1
 charge on i:					0.27190000000000003
 charge on j:					0.0976
 current length:				2.4724199482912708
 current potential contribution:		3.5640449348241594
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH32
 amberAtomType : H1
 x, y, z :       6.7276629 8.749399 0.2703644
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						H -HH33
 AtomTypes:					H -H1
 charge on i:					0.27190000000000003
 charge on j:					0.0976
 current length:				3.0153910302888898
 current potential contribution:		2.9222796330403282
name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.27190000000000003
 name :          H
 amberAtomType : H
 x, y, z :       6.7663435 6.2793881 0.3723956
}name.mjw.jamber.Atom Object {
 mass :          1.008
 charge :        0.0976
 name :          HH33
 amberAtomType : H1
 x, y, z :       5.1726618 8.6445622 -0.6067434
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -CA
 AtomTypes:					O -CT
 charge on i:					-0.5679000016463344
 charge on j:					0.0337
 current length:				2.870942775104311
 current potential contribution:		-2.2135208615884827
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       4.4091963 1.7777255 -0.3023778
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -CB
 AtomTypes:					C -CT
 charge on i:					0.5972000021951126
 charge on j:					-0.1825
 current length:				3.202675430387265
 current potential contribution:		-11.299939700713418
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						C -C 
 AtomTypes:					C -C 
 charge on i:					0.5972000021951126
 charge on j:					0.5973000005487781
 current length:				3.6770394131149073
 current potential contribution:		32.21220211766115
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5972000021951126
 name :          C
 amberAtomType : C
 x, y, z :       3.5930515 2.6099653 -0.1293539
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.5973000005487781
 name :          C
 amberAtomType : C
 x, y, z :       4.827823 6.0732417 -0.1702778
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CH3-CA
 AtomTypes:					CT-CT
 charge on i:					-0.3662
 charge on j:					0.0337
 current length:				3.8534674306534162
 current potential contribution:		-1.0634153692253756
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.3662
 name :          CH3
 amberAtomType : CT
 x, y, z :       2.1129473 2.1410015 0.1174182
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						O -CH3
 AtomTypes:					O -CT
 charge on i:					-0.5679000016463344
 charge on j:					-0.149
 current length:				2.6544667054066733
 current potential contribution:		10.584911726607965
name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CB-O 
 AtomTypes:					CT-O 
 charge on i:					-0.1825
 charge on j:					-0.5679000016463344
 current length:				3.4589680460272354
 current potential contribution:		9.949346896004364
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CB-N 
 AtomTypes:					CT-N 
 charge on i:					-0.1825
 charge on j:					-0.4157
 current length:				3.068011307850882
 current potential contribution:		8.210929475408316
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.1825
 name :          CB
 amberAtomType : CT
 x, y, z :       5.9909819 4.2294925 -1.5020124
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						CA-CH3
 AtomTypes:					CT-CT
 charge on i:					0.0337
 charge on j:					-0.149
 current length:				3.792479333829134
 current potential contribution:		-0.43964215806155527
name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        0.0337
 name :          CA
 amberAtomType : CT
 x, y, z :       5.0807617 4.5687833 -0.266202
}name.mjw.jamber.Atom Object {
 mass :          12.010000000000002
 charge :        -0.149
 name :          CH3
 amberAtomType : CT
 x, y, z :       5.7588658 8.2664338 0.2342529
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						N -O 
 AtomTypes:					N -O 
 charge on i:					-0.4157
 charge on j:					-0.5679000016463344
 current length:				2.6980621539169816
 current potential contribution:		29.05402654506218
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}name.mjw.jamber.Atom Object {
 mass :          16.0
 charge :        -0.5679000016463344
 name :          O
 amberAtomType : O
 x, y, z :       3.7664542 6.6075608 -0.3355101
}}
name.mjw.jamber.ElectroStatic Object {
 Atoms:						N -N 
 AtomTypes:					N -N 
 charge on i:					-0.4157
 charge on j:					-0.4157
 current length:				3.604184468349173
 current potential contribution:		15.920599700772929
name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       3.7580545 3.9140824 -0.1785294
}name.mjw.jamber.Atom Object {
 mass :          14.01
 charge :        -0.4157
 name :          N
 amberAtomType : N
 x, y, z :       5.8838025 6.8046139 0.162306
}}
