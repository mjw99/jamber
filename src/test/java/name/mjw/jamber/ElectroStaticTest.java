package name.mjw.jamber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.Assert.assertEquals;

public class ElectroStaticTest {

	final Logger LOG = Logger.getLogger(ElectroStaticTest.class);

	private static ElectroStatic myElectroStatic;

	private static Atom myAtom1;
	private static Atom myAtom2;

	private final double delta = 0.00001;

	@BeforeClass
	public static void setUp() {

		myAtom1 = new Atom(new Point3d(0.0, 0.0, 0.0));
		myAtom2 = new Atom(new Point3d(3.6, 0.0, 0.0));

		myAtom1.setCharge(1.0);
		myAtom2.setCharge(1.0);

		// Two protons separated by 3.6 A
		myElectroStatic = new ElectroStatic(myAtom1, myAtom2, 1.0);
	}

	@Test
	public void testGetPotentialEnergy() {

		assertEquals(92.236727, myElectroStatic.getPotentialEnergy(), delta);
		LOG.debug("myElectroStatic potential energy is "
				+ myElectroStatic.getPotentialEnergy() + " kcal/mol");

	}

	@Test
	public void testGetAnalyticalGradient() {

		assertEquals(-25.621313, myElectroStatic.getAnalyticalGradient(), delta);
		LOG.debug("myElectroStatic analytical gradient energy is "
				+ myElectroStatic.getAnalyticalGradient() + " kcal/(A mol)");

	}

	@Test
	public void testEvaluateForce() {

		myElectroStatic.evaluateForce();

		// Express the result as a Vector3d object
		Vector3d myAtom1Force = new Vector3d(25.621313063271604, 0.0, 0.0);
		Vector3d myAtom2Force = new Vector3d(-25.621313063271604, 0.0, 0.0);

		assertEquals(myAtom1Force, myAtom1.getForce());
		assertEquals(myAtom2Force, myAtom2.getForce());

		LOG.debug("Force on myAtom1 is " + myAtom1.getForce().toString()
				+ " kcal/mol/A");
		LOG.debug("Force on myAtom2 is " + myAtom2.getForce().toString()
				+ " kcal/mol/A");

	}

}
