package name.mjw.jamber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.Assert.assertEquals;

/**
 * 
 * @author mjw
 * 
 */
public class LJTwelveSixTest {

	final Logger LOG = Logger.getLogger(LJTwelveSixTest.class);

	private static Atom myAtom1;
	private static Atom myAtom2;

	private static LJTwelveSix myVdw;

	private final double delta = 0.00001;

	@BeforeClass
	public static void setUp() {

		myAtom1 = new Atom(new Point3d(0.0, 0.0, 0.0));
		myAtom2 = new Atom(new Point3d(3.6, 0.0, 0.0));

		// Based upon OH-CT
		myVdw = new LJTwelveSix(myAtom1, 1.7210, 0.2104, myAtom2, 1.9080,
				0.1094, 1.0);
	}

	@Test
	public void testGetLennardJonesA() {
		assertEquals(791544.1565721805, myVdw.getLennardJonesA(), delta);

	}

	@Test
	public void testGetLennardJonesB() {
		assertEquals(6.93079947E+02, myVdw.getLennardJonesB(), delta);

	}

	@Test
	public void testGetPotentialEnergy() {
		assertEquals(-0.15134704825581527, myVdw.getPotentialEnergy(), delta);
		LOG.debug("myVdw potential energy is " + myVdw.getPotentialEnergy()
				+ " kcal/mol");

	}

	@Test
	public void testGetAnalyticalGradient() {
		assertEquals(-0.026170, myVdw.getAnalyticalGradient(), delta);
		LOG.debug("myVdw analytical gradient is "
				+ myVdw.getAnalyticalGradient() + " kcal / (A mol)");

	}

	@Test
	public void testEvaluateForce() {

		myVdw.evaluateForce();

		// Express the result as a Vector3d object
		Vector3d myAtom1Force = new Vector3d(0.026170725626711078, 0.0, 0.0);
		Vector3d myAtom2Force = new Vector3d(-0.026170725626711078, 0.0, 0.0);

		assertEquals(myAtom1Force, myAtom1.getForce());
		assertEquals(myAtom2Force, myAtom2.getForce());

		LOG.debug("Force on myAtom1 is " + myAtom1.getForce().toString() + " kcal / (A mol)");
		LOG.debug("Force on myAtom2 is " + myAtom2.getForce().toString() + " kcal / (A mol)");

	}

}
