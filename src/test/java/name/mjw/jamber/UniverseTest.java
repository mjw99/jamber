package name.mjw.jamber;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import name.mjw.jamber.io.amber.Inpcrd;
import name.mjw.jamber.io.amber.Prmtop;

import javax.vecmath.Point3d;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;

/**
 * 
 * @author mjw
 * 
 */
public class UniverseTest {
	final Logger LOG = Logger.getLogger(UniverseTest.class);
	
	private final double delta = 0.00001;
	private static Universe universe;

	@Before
	public void setUp() {

		universe = new Universe();

		// Add three atoms
		Atom myAtom1 = new Atom(new Point3d(1.0, 0.0, 0.0));
		Atom myAtom2 = new Atom(new Point3d(1.0, 1.0, 0.0));
		Atom myAtom3 = new Atom(new Point3d(0.0, 1.0, 0.0));

		universe.addAtom(myAtom1);
		universe.addAtom(myAtom2);
		universe.addAtom(myAtom3);

		Bond myBond = new Bond(universe.getAtom(0), universe.getAtom(1), 0.8,
				50.0);

		universe.addBond(myBond);

		Angle angle = new Angle(universe.getAtom(0), universe.getAtom(1),
				universe.getAtom(2), 60.0, 4.3);

		universe.addAngle(angle);

		// TODO Dihedral test

	}

	@Test
	public void testUniverseGetPotentialEnergy() {

		LOG.debug("Bond.get(0) potential energy is "
				+ universe.bonds.get(0).getPotentialEnergy() + " kcal");
		LOG.debug("Angle.get(0) potential energy is "
				+ universe.angles.get(0).getPotentialEnergy() + " kcal");
		assertEquals(3.178869414574561, universe.getPotentialEnergy(), delta);

	}

	@Test
	public void testUniverseGetForce() {
		universe.evaluateForce();

		assertEquals(0.0, universe.getAtom(0).getForce().x, delta);
		assertEquals(-24.502949470145367, universe.getAtom(0).getForce().y,
				delta);
		assertEquals(0.0, universe.getAtom(0).getForce().z, delta);

		assertEquals(0.0, universe.getAtom(1).getForce().x, delta);
		assertEquals(19.999999, universe.getAtom(1).getForce().y, delta);
		assertEquals(0.0, universe.getAtom(1).getForce().z, delta);

		assertEquals(0.0, universe.getAtom(2).getForce().x, delta);
		assertEquals(4.50294947014537, universe.getAtom(2).getForce().y, delta);
		assertEquals(0.0, universe.getAtom(2).getForce().z, delta);

	}

	// TODO Move these out to their own tests
	@Test
	/**
	 * Blocked Alanine Dipeptide example system
	 * Reference values from modified PMEMD with longer write() printing 
	 * in the mdinfo output file
	 */
	public void testAMBERUniverseFromPrmtopInpcrd() {

		String prmtopName = null;
		try {
			prmtopName = URLDecoder.decode(getClass().getResource(
					"/name/mjw/jamber/io/amber/blockedAlanineDipeptide/prmtop").getFile(),"utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		Prmtop prmtop = new Prmtop();
		try {
			prmtop.read(prmtopName);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

        String inpcrdName = null;
		try {
			inpcrdName = URLDecoder.decode(getClass().getResource(
					"/name/mjw/jamber/io/amber/blockedAlanineDipeptide/inpcrd")
					.getFile(),"utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		Inpcrd inpcrd = new Inpcrd();
		try {
			inpcrd.read(inpcrdName);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

        universe = new Universe(prmtop, inpcrd);
		// universe.setCutoff(999);

		LOG.debug(universe);

		assertEquals(5.6366273691689504, universe.getBondPotentialEnergy(),
				delta);

		assertEquals(8.2046173826501736, universe.getAnglePotentialEnergy(),
				delta);

		// TODO precision could be improved here.
		// I think its because of radian <--> degrees interconversion
		assertEquals(11.365686464834990, universe.getDihedralPotentialEnergy(),
				delta);

		assertEquals(-1.0455001424914672, universe.getVdwPotentialEnergy(),
				delta);

		assertEquals(4.4614567778860472, universe.get14VdwPotentialEnergy(),
				delta);

		//assertEquals(-79.994516268914055, universe.getEEPotentialEnergy(),delta);

		//assertEquals(48.744345933282325, universe.get14EEPotentialEnergy(), delta);
		

		

	}

	/**
	 * Acetic acid with GAFF example Reference values from modified PMEMD with
	 * longer write() printing in the mdinfo output file
	 */
	@Test
	public void testAMBERUniverseFromPrmtopInpcrd2() {

		String prmtopName = null;
		try {
			prmtopName = URLDecoder.decode(getClass().getResource(
					"/name/mjw/jamber/io/amber/aceticAcid/prmtop").getFile(),"utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		Prmtop prmtop = new Prmtop();
		try {
			prmtop.read(prmtopName);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

        String inpcrdName = null;
		try {
			inpcrdName = URLDecoder.decode(getClass().getResource(
					"/name/mjw/jamber/io/amber/aceticAcid/inpcrd").getFile(), "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		Inpcrd inpcrd = new Inpcrd();
		try {
			inpcrd.read(inpcrdName);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

        universe = new Universe(prmtop, inpcrd);
		// universe.setCutoff(999);

		assertEquals(1.0058029411602141, universe.getBondPotentialEnergy(),
				delta);
		assertEquals(0.71799592728173889, universe.getAnglePotentialEnergy(),
				delta);
		assertEquals(2.4323563818867071, universe.getDihedralPotentialEnergy(),
				delta);

		assertEquals(0.0, universe.getVdwPotentialEnergy(), delta);

		assertEquals(0.2593454785090933, universe.get14VdwPotentialEnergy(),
				delta);

		//assertEquals(11.474374692714399, universe.getEEPotentialEnergy(), delta);

		//assertEquals(-51.347936049016937, universe.get14EEPotentialEnergy(),delta);

		LOG.debug(universe);
		
		
		universe.evaluateForce();

		for (Atom atom : universe.getAtoms()) {
			LOG.debug(atom.getForce());
		}
		
		// universe.dumpAllterms();
		//universe.dumpAlltermsCML();

	}

	/**
	 * CPDI with GAFF example Reference values from OpenMM, checked with SANDER
	 * 
	 */
	@Test
	public void testAMBERUniverseFromPrmtopInpcrd3() {

		String prmtopName = null;
		try {
			prmtopName = URLDecoder.decode(getClass().getResource(
					"/name/mjw/jamber/io/amber/CPDI/prmtop").getFile(), "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		Prmtop prmtop = new Prmtop();
		try {
			prmtop.read(prmtopName);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

        String inpcrdName = null;
		try {
			inpcrdName = URLDecoder.decode(getClass().getResource(
					"/name/mjw/jamber/io/amber/CPDI/inpcrd").getFile(), "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		Inpcrd inpcrd = new Inpcrd();
		try {
			inpcrd.read(inpcrdName);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

        universe = new Universe(prmtop, inpcrd);
		// universe.setCutoff(999);

		assertEquals(141.582807548, universe.getBondPotentialEnergy(), delta);
		assertEquals(88.888382108, universe.getAnglePotentialEnergy(), delta);
		assertEquals(65.4065605046, universe.getDihedralPotentialEnergy(),
				delta);

		assertEquals(-8.81511, universe.getVdwPotentialEnergy(), delta);

		assertEquals(5.18826, universe.get14VdwPotentialEnergy(), delta);

		assertEquals(12.35165, universe.getEEPotentialEnergy(), delta);

		assertEquals(18.83727, universe.get14EEPotentialEnergy(), delta);

		LOG.debug(universe);

	}

	/**
	 * 1_aminoethan_1_one with GAFF example
	 */
	@Test
	public void testAMBERUniverseFromPrmtopInpcrd4() {

		String prmtopName = null;
		try {
			prmtopName = URLDecoder.decode(getClass().getResource(
					"/name/mjw/jamber/io/amber/1_aminoethan_1_one/prmtop")
					.getFile(), "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		Prmtop prmtop = new Prmtop();
		try {
			prmtop.read(prmtopName);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

        String inpcrdName = null;
		try {
			inpcrdName = URLDecoder.decode(getClass().getResource(
					"/name/mjw/jamber/io/amber/1_aminoethan_1_one/opt/restrt")
					.getFile(),"utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		Inpcrd inpcrd = new Inpcrd();
		try {
			inpcrd.read(inpcrdName);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

        universe = new Universe(prmtop, inpcrd);
		universe.setCutoff(999);

		LOG.debug(universe);
	
		// universe.dumpAlltermsCML();
		// universe.dumpAllterms();

	}
}
