package name.mjw.jamber.util;

import org.apache.log4j.Logger;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChemicalSymbolTest {
	
	final Logger LOG = Logger.getLogger(ChemicalSymbolTest.class);

	@Test
	public void testZtoChemicalSymbol1() {
		LOG.debug(ChemicalSymbol.zToChemicalSymbol(6));

		assertEquals("C", ChemicalSymbol.zToChemicalSymbol(6));
		assertEquals(6, ChemicalSymbol.chemicalSymbolToZ("C"));

	}

	@Test
	public void testZtoChemicalSymbol2() {
		LOG.debug(ChemicalSymbol.zToChemicalSymbol(20));

		assertEquals("Ca", ChemicalSymbol.zToChemicalSymbol(20));
		assertEquals(20, ChemicalSymbol.chemicalSymbolToZ("Ca"));
	}

	@Test
	public void testZtoChemicalSymbol3() {
		LOG.debug(ChemicalSymbol.zToChemicalSymbol(47));

		assertEquals("Ag", ChemicalSymbol.zToChemicalSymbol(47));
		assertEquals(47, ChemicalSymbol.chemicalSymbolToZ("Ag"));
	}

	@Test(expected = RuntimeException.class)
	/**
	 * Assert that it fails properly
	 */
	public void testZtoChemicalSymbo3l() {
		ChemicalSymbol.zToChemicalSymbol(256);
	}

	@Test(expected = RuntimeException.class)
	/**
	 * Assert that it fails properly
	 */
	public void chemicalSymbolToZ1() {
		ChemicalSymbol.chemicalSymbolToZ("CA");
	}

	@Test(expected = RuntimeException.class)
	/**
	 * Assert that it fails properly
	 */
	public void chemicalSymbolToZ2() {
		ChemicalSymbol.chemicalSymbolToZ("CL");
	}

	@Test
	public void testAmberAtomTypeToChemicalSymbol1() {
		LOG.debug(ChemicalSymbol.amberAtomTypeToChemicalSymbol("ho"));

		assertEquals("H", ChemicalSymbol.amberAtomTypeToChemicalSymbol("ho"));
	}

	@Test
	public void testAmberAtomTypeToChemicalSymbol2() {
		LOG.debug(ChemicalSymbol.amberAtomTypeToChemicalSymbol("cl"));

		assertEquals("Cl", ChemicalSymbol.amberAtomTypeToChemicalSymbol("cl"));
	}

	@Test(expected = RuntimeException.class)
	/**
	 * Assert that it fails properly
	 */
	public void testAmberAtomTypeToChemicalSymbol3() {
		ChemicalSymbol.amberAtomTypeToChemicalSymbol("Foo");
	}

}
