package name.mjw.jamber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.Assert.assertEquals;

/**
 * 
 * @author mjw
 * 
 */
public class AngleTest {
	final Logger LOG = Logger.getLogger(AngleTest.class);

	private static Atom myAtom1;
	private static Atom myAtom2;
	private static Atom myAtom3;

	private static Angle myAngle;

	private final double delta = 0.00001;

	@BeforeClass
	public static void setUp() {

		myAtom1 = new Atom(new Point3d(1.0, 0.0, 0.0));
		myAtom2 = new Atom(new Point3d(1.0, 1.0, 0.0));
		myAtom3 = new Atom(new Point3d(0.0, 0.75, 0.0));

		myAngle = new Angle(myAtom1, myAtom2, myAtom3, 73.0, 4.3);

	}

	@Test
	public void testGetCurrentAngle() {

		assertEquals(75.96375653207353, myAngle.getCurrentAngle(), delta);
		LOG.debug("myAngle Current angle is " + myAngle.getCurrentAngle()
				+ " degrees");

	}

	@Test
	public void testGetEquilibriumAngle() {

		assertEquals(73.0, myAngle.getEquilibriumAngle(), delta);
		LOG.debug("myAngle Equilibrium angle is "
				+ myAngle.getEquilibriumAngle() + " degrees");

	}

	@Test
	public void testGetForceConstant() {

		assertEquals(4.3, myAngle.getForceConstant(), delta);
		LOG.debug("myAngle Force constant is " + myAngle.getForceConstant()
				+ " kcal/mol/(rad**2)");
	}

	@Test
	public void testGetPotentialEnergy() {

		myAngle.getPotentialEnergy();
		assertEquals(0.011505572651253569, myAngle.getPotentialEnergy(), delta);
		LOG.debug("myAngle Potential energy is " + myAngle.getPotentialEnergy()
				+ " kcal/mol");

	}

	@Test
	public void testGetAnalyticalGradient() {

		myAngle.getAnalyticalGradient();
		assertEquals(0.4448548635246797, myAngle.getAnalyticalGradient(), delta);
		LOG.debug("myAngle Potential energy is "
				+ myAngle.getAnalyticalGradient() + " kcal/mol");

	}

	@Test
	public void testEvaluateForce() {

		myAngle.evaluateForce();

		Vector3d myAtom1Force = new Vector3d(0.0, -0.4448548635246797, 0.0);
		Vector3d myAtom2Force = new Vector3d(0.0, 0.0, 0.0);
		Vector3d myAtom3Force = new Vector3d(0.0, 0.4448548635246797, 0.0);

		assertEquals(myAtom1Force, myAtom1.getForce());
		assertEquals(myAtom2Force, myAtom2.getForce());
		assertEquals(myAtom3Force, myAtom3.getForce());

		LOG.debug("Force on myAtom1 is " + myAtom1.getForce().toString()
				+ " kcal/mol/rad");
		LOG.debug("Force on myAtom2 is " + myAtom2.getForce().toString()
				+ " kcal/mol/rad");
		LOG.debug("Force on myAtom3 is " + myAtom3.getForce().toString()
				+ " kcal/mol/rad");

	}

}
