package name.mjw.jamber.io.openmm;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import name.mjw.jamber.io.amber.Lib;
import name.mjw.jamber.io.amber.ParameterStore;
import name.mjw.jamber.io.openmm.OpenMMXML;

import java.io.*;
import java.net.URLDecoder;

public class OpenMMXMLWithLibTest {

	final Logger LOG = Logger.getLogger(OpenMMXMLWithLibTest.class);

	private static OpenMMXML openMMXML;
	private static ParameterStore parameterStore;
	private static Lib lib;

	@BeforeClass
	public static void setUp() {

		InputStream parmIs = OpenMMXMLWithLibTest.class.getResourceAsStream("/name/mjw/jamber/io/amber/lib/parm99.dat");

		try {
			parameterStore = new ParameterStore();
			parameterStore.readParm(parmIs);

		} catch (IOException e) {

			e.printStackTrace();
		}

		InputStream amino94Stream = OpenMMXMLWithLibTest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/lib/all_amino94.lib");

		InputStream aminont94Stream = OpenMMXMLWithLibTest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/lib/all_aminont94.lib");

		InputStream aminoct94Stream = OpenMMXMLWithLibTest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/lib/all_aminoct94.lib");

		try {
			lib = new Lib(amino94Stream, aminont94Stream, aminoct94Stream);

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	@Test
	public void testOpenMMXML() {

		// Create the OpenMMXML object
		openMMXML = new OpenMMXML(parameterStore, lib);

		String coreName = "amber99";
		String path;
		try {
			// Get path relative to the test directory
			path = URLDecoder.decode(this.getClass().getResource("/name/mjw/jamber/io/amber/lib/").getFile(), "utf-8");

			String filePathFF = path + coreName + "_ff.xml";
			String filePathHydrogens = path + coreName + "_hydrogens.xml";
			String filePathResidues = path + coreName + "_residues.xml";
			String filePathGBSA_OBC = path + coreName + "_obc.xml";

			OutputStream outFF = new FileOutputStream(filePathFF);
			openMMXML.toFFXMLOutputStream(outFF);

			OutputStream outHydrogens = new FileOutputStream(filePathHydrogens);
			openMMXML.toHydrogensXMLOutputStream(outHydrogens);

			OutputStream outResidues = new FileOutputStream(filePathResidues);
			openMMXML.toResiduesXMLOutputStream(outResidues);

			OutputStream outGBSA_OBC = new FileOutputStream(filePathGBSA_OBC);
			openMMXML.toGBSA_OBC_XMLOutputStream(outGBSA_OBC);

		} catch (UnsupportedEncodingException | FileNotFoundException e1) {
			e1.printStackTrace();
		}

	}

}
