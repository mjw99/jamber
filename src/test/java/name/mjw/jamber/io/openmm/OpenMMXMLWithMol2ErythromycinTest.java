package name.mjw.jamber.io.openmm;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import name.mjw.jamber.io.Mol2;
import name.mjw.jamber.io.amber.Lib;
import name.mjw.jamber.io.amber.ParameterStore;
import name.mjw.jamber.io.openmm.OpenMMXML;

import java.io.*;
import java.net.URLDecoder;

/**
 * Tests based on gaff.dat (version 1.5) AMBER12 with md5sum
 * 7392a517db165cc715c63baf9f8c1ace
 * 
 * @author mjw
 * 
 */
public class OpenMMXMLWithMol2ErythromycinTest {

	final Logger LOG = Logger.getLogger(OpenMMXMLWithMol2ErythromycinTest.class);

	private static OpenMMXML openMMXML;
	private static ParameterStore parameterStore;
	private static Mol2 erythromycinMol2;
	private static Lib lib;

	@BeforeClass
	public static void setUp() {

		InputStream gaffIs = OpenMMXMLWithMol2ErythromycinTest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/lib/gaff.dat");

		InputStream frcmodIs = OpenMMXMLWithMol2ErythromycinTest.class
				.getResourceAsStream("/name/mjw/jamber/io/openmm/erythromycin/erythromycin.frcmod");

		try {
			parameterStore = new ParameterStore();
			parameterStore.readParm(gaffIs);
			parameterStore.readFrcmod(frcmodIs);

		} catch (Exception e) {

			e.printStackTrace();
		}

		InputStream erythromycinMol2Stream = OpenMMXMLWithMol2ErythromycinTest.class
				.getResourceAsStream("/name/mjw/jamber/io/openmm/erythromycin/erythromycin.mol2");

		try {

			erythromycinMol2 = new Mol2(erythromycinMol2Stream);

			lib = new Lib(erythromycinMol2);

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	@Test
	public void testOpenMMXML() {

		// Create the OpenMMXML object
		openMMXML = new OpenMMXML(parameterStore, lib);

		String coreName = "erythromycin";
		String path;
		try {
			// Get path relative to the test directory
			path = URLDecoder.decode(
					this.getClass().getResource("/name/mjw/jamber/io/openmm/" + coreName + "/").getFile(), "utf-8");

			String filePathFF = path + coreName + "_ff.xml";
			String filePathHydrogens = path + coreName + "_hydrogens.xml";
			String filePathResidues = path + coreName + "_residues.xml";
			String filePathGBSA_OBC = path + coreName + "_obc.xml";

			OutputStream outFF = new FileOutputStream(filePathFF);
			openMMXML.toFFXMLOutputStream(outFF);

			OutputStream outHydrogens = new FileOutputStream(filePathHydrogens);
			openMMXML.toHydrogensXMLOutputStream(outHydrogens);

			OutputStream outResidues = new FileOutputStream(filePathResidues);
			openMMXML.toResiduesXMLOutputStream(outResidues);

			OutputStream outGBSA_OBC = new FileOutputStream(filePathGBSA_OBC);
			openMMXML.toGBSA_OBC_XMLOutputStream(outGBSA_OBC);

		} catch (UnsupportedEncodingException | FileNotFoundException e1) {
			e1.printStackTrace();
		}

	}

}
