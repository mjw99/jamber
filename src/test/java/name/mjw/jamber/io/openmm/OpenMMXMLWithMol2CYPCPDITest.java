package name.mjw.jamber.io.openmm;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import name.mjw.jamber.io.Mol2;
import name.mjw.jamber.io.amber.Atom;
import name.mjw.jamber.io.amber.Lib;
import name.mjw.jamber.io.amber.ParameterStore;
import name.mjw.jamber.io.amber.Residue;
import name.mjw.jamber.io.openmm.OpenMMXML;

import java.io.*;
import java.net.URLDecoder;
import java.text.ParseException;

/**
 * Tests based on gaff.dat (version 1.5) AMBER12 with md5sum
 * 7392a517db165cc715c63baf9f8c1ace
 * 
 * @author mjw
 * 
 */
public class OpenMMXMLWithMol2CYPCPDITest {

	final Logger LOG = Logger.getLogger(OpenMMXMLWithMol2CYPCPDITest.class);

	private static OpenMMXML openMMXML;
	private static ParameterStore parameterStore;
	private static Mol2 heme;
	private static Mol2 cyp;
	private static Lib lib;

	@BeforeClass
	public static void setUp() {

		InputStream gaffIs = OpenMMXMLWithMol2CYPCPDITest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/lib/gaff.dat");

		InputStream frcmodIs = OpenMMXMLWithMol2CYPCPDITest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/CPDI/CPDI.frcmod");

		InputStream parmLiteFrcmod = OpenMMXMLWithMol2CYPCPDITest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/CPDI/parm99_lite.dat");

		try {
			parameterStore = new ParameterStore();

			parameterStore.readParm(gaffIs);

			// TODO make consistent with other methods
			try {
				parameterStore.readFrcmod(parmLiteFrcmod);
				parameterStore.readFrcmod(frcmodIs);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

		InputStream hemeMol2Stream = OpenMMXMLWithMol2CYPCPDITest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/CPDI/HEM.mol2");

		InputStream cypMol2Stream = OpenMMXMLWithMol2CYPCPDITest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/CYP/CYP.mol2");

		try {

			heme = new Mol2(hemeMol2Stream);
			cyp = new Mol2(cypMol2Stream);

			lib = new Lib(heme);

			/*
			 * Set the HEM residue external connection bond by hand since Mol2 does not
			 * contain that information.
			 */

			Residue hemeResidue = lib.getResidueByName("HEM");
			Atom connectionAtom = hemeResidue.getAtoms().get(27);
			hemeResidue.appendExternalBond(connectionAtom);

			// Load CYP residue information
			lib.addFromMol2(cyp);

			Residue cypResidue = lib.getResidueByName("CYP");
			Atom connectionAtomN = cypResidue.getAtoms().get(0);
			Atom connectionAtomC = cypResidue.getAtoms().get(8);
			Atom connectionAtomS = cypResidue.getAtoms().get(7);
			cypResidue.appendExternalBond(connectionAtomN);
			cypResidue.appendExternalBond(connectionAtomC);
			cypResidue.appendExternalBond(connectionAtomS);

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	@Test
	public void testOpenMMXML() {

		// Create the OpenMMXML object
		openMMXML = new OpenMMXML(parameterStore, lib);

		String coreName = "CPDI_CYP";
		String path;
		try {
			// Get path relative to the test directory
			path = URLDecoder.decode(
					this.getClass().getResource("/name/mjw/jamber/io/amber/" + coreName + "/").getFile(), "utf-8");

			String filePathFF = path + coreName + "_ff.xml";
			String filePathHydrogens = path + coreName + "_hydrogens.xml";
			String filePathResidues = path + coreName + "_residues.xml";
			String filePathGBSA_OBC = path + coreName + "_obc.xml";

			OutputStream outFF = new FileOutputStream(filePathFF);
			openMMXML.toFFXMLOutputStream(outFF);

			OutputStream outHydrogens = new FileOutputStream(filePathHydrogens);
			openMMXML.toHydrogensXMLOutputStream(outHydrogens);

			OutputStream outResidues = new FileOutputStream(filePathResidues);
			openMMXML.toResiduesXMLOutputStream(outResidues);

			OutputStream outGBSA_OBC = new FileOutputStream(filePathGBSA_OBC);
			openMMXML.toGBSA_OBC_XMLOutputStream(outGBSA_OBC);

		} catch (UnsupportedEncodingException | FileNotFoundException e1) {
			e1.printStackTrace();
		}

	}

}
