package name.mjw.jamber.io;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import name.mjw.jamber.io.Mol2;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class Mol2HEMTest {

	final Logger LOG = Logger.getLogger(Mol2HEMTest.class);

	private final double delta = 0.00001;

	private static Mol2 hemeMol2;

	@BeforeClass
	public static void setUp() {

		InputStream hemeIs = Mol2HEMTest.class.getResourceAsStream("/name/mjw/jamber/io/amber/CPDI/HEM.mol2");

		try {
			hemeMol2 = new Mol2(hemeIs);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testNumberOfAtoms() {

		assertEquals(74, hemeMol2.getResidue().getAtoms().size());

	}

	@Test
	public void testNameOfAtom() {

		assertEquals("C1C", hemeMol2.getResidue().getAtoms().get(1).getName());

	}

	@Test
	public void testChargeOfAtom() {

		assertEquals(-0.3729, hemeMol2.getResidue().getAtoms().get(73).getCharge(), delta);

	}

	@Test
	public void testNumberOfBonds() {

		assertEquals(81, hemeMol2.getResidue().getConnections().size());

	}

}
