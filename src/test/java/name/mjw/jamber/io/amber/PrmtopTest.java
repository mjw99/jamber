package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import name.mjw.jamber.io.amber.Prmtop;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;

/**
 * Parm7 parsing tests
 * 
 * @author mjw
 * 
 */
public class PrmtopTest {

	final Logger LOG = Logger.getLogger(PrmtopTest.class);

	private static final String PRMTOP = "/name/mjw/jamber/io/amber/blockedAlanineDipeptide/prmtop";
	private final double delta = 0.00001;
	private static Prmtop myPrmtop;

	@BeforeClass
	public static void setUp() {

		String in = null;
		try {
			in = URLDecoder.decode(PrmtopTest.class.getResource(PRMTOP).getFile(), "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		myPrmtop = new Prmtop();
		try {
			myPrmtop.read(in);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Tests that the POINTERS section in a prmtop file is being parsed correctly
	 */
	@Test
	public void testPrmtopPOINTERSSection() {

		assertEquals("ACE", myPrmtop.getTitle().trim());

		assertEquals(22, myPrmtop.getNumberOfAtoms());
		assertEquals(7, myPrmtop.getNumberOfAtomTypesInPrmtop());
		assertEquals(12, myPrmtop.getNumberOfBondsContainingHydrogen());
		assertEquals(9, myPrmtop.getNumberOfBondsNotContainingHydrogen());
		assertEquals(25, myPrmtop.getNumberOfAnglesContainingHydrogen());
		assertEquals(11, myPrmtop.getNumberOfAnglesNotContainingHydrogen());
		assertEquals(42, myPrmtop.getNumberOfDihedralsContainingHydrogen());
		assertEquals(24, myPrmtop.getNumberOfDihedralsNotContainingHydrogen());
		// Unused
		assertEquals(0, myPrmtop.getCreatedByAddles());

		assertEquals(99, myPrmtop.getNumberOfExcludedAtoms());
		assertEquals(3, myPrmtop.getNumberOfResidues());
		assertEquals(9, myPrmtop.getNumberOfConstrainedBonds());
		assertEquals(11, myPrmtop.getNumberOfConstrainedAngles());
		assertEquals(24, myPrmtop.getNumberOfConstrainedDihedrals());
		assertEquals(8, myPrmtop.getNumberOfUniqueBondTypes());
		assertEquals(16, myPrmtop.getNumberOfUniqueAnglesTypes());
		assertEquals(19, myPrmtop.getNumberOfUniqueDihedralsTypes());
		// Number of atom types again
		assertEquals(0, myPrmtop.getNumberOf1012HydrogenBondPairTypes());

		assertEquals(0, myPrmtop.getReadInPerturbation());
		assertEquals(0, myPrmtop.getNumberOfBondsToBePerturbed());
		assertEquals(0, myPrmtop.getNumberOfAnglesToBePerturbed());
		assertEquals(0, myPrmtop.getNumberOfDihedralsToBePerturbed());
		assertEquals(0, myPrmtop.getNumberOfBondsWithAtomsInPerturbedGroup());
		assertEquals(0, myPrmtop.getNumberOfAnglesWithAtomsInPerturbedGroup());
		assertEquals(0, myPrmtop.getNumberOfDihedralsWithAtomsInPerturbedGroup());
		assertEquals(0, myPrmtop.getBoxType());
		assertEquals(10, myPrmtop.getNumberOfAtomsInLargestResidue());
		assertEquals(0, myPrmtop.getCap());

		assertEquals(0, myPrmtop.getNumberOfExtraPoints());
	}

	/**
	 * Tests that the ATOM_NAME section in a prmtop file is being parsed correctly
	 */
	@Test
	public void testPrmtopATOM_NAMESection() {

		assertEquals("CH3", myPrmtop.getAtomName().get(1));
		assertEquals("HH31", myPrmtop.getAtomName().get(19));

	}

	/**
	 * Tests that the CHARGE section in a prmtop file is being parsed correctly
	 */
	@Test
	public void testPrmtopCHARGESection() {

		assertEquals(2.04636429E+00, myPrmtop.getCharges().get(2), delta);
		assertEquals(-2.71512270E+00, myPrmtop.getCharges().get(18), delta);

	}

	@Test
	public void testPrmtopMASSsection() {

		assertEquals(1.00800000E+00, myPrmtop.getMass().get(3), delta);
		assertEquals(1.20100000E+01, myPrmtop.getMass().get(10), delta);

	}

	@Test
	public void testPrmtopATOM_TYPE_INDEXsection() {

		assertEquals(1, myPrmtop.getAtomTypeIndex().get(0), delta);
		assertEquals(7, myPrmtop.getAtomTypeIndex().get(9), delta);

	}

	@Test
	public void testPrmtopNUMBER_EXCLUDED_ATOMSsection() {

		assertEquals(6, myPrmtop.getExcludedAtomsIndex().get(0), delta);
		assertEquals(1, myPrmtop.getExcludedAtomsIndex().get(21), delta);

	}

	@Test
	public void testPrmtopNONBONDED_PARM_INDEXsection() {

		assertEquals(1, myPrmtop.getNonbondedParmIndex().get(0), delta);
		assertEquals(18, myPrmtop.getNonbondedParmIndex().get(19), delta);
		assertEquals(12, myPrmtop.getNonbondedParmIndex().get(29), delta);

	}

	@Test
	public void testPrmtopRESIDUE_LABELsection() {

		assertEquals("ACE", myPrmtop.getResidueLabel().get(0));
		assertEquals("NME", myPrmtop.getResidueLabel().get(2));

	}

	@Test
	public void testPrmtopRESIDUE_POINTERsection() {

		assertEquals(1, myPrmtop.getResiduePointer().get(0), delta);
		assertEquals(7, myPrmtop.getResiduePointer().get(1), delta);
	}

	@Test
	public void testPrmtopBOND_FORCE_CONSTANTsection() {

		assertEquals(5.70000000E+02, myPrmtop.getBondForceConstant().get(0), delta);
		assertEquals(3.40000000E+02, myPrmtop.getBondForceConstant().get(4), delta);
		assertEquals(3.37000000E+02, myPrmtop.getBondForceConstant().get(7), delta);
	}

	@Test
	public void testPrmtopBOND_EQUIL_VALUEsection() {

		assertEquals(1.22900000E+00, myPrmtop.getBondEquilValue().get(0), delta);
		assertEquals(1.52200000E+00, myPrmtop.getBondEquilValue().get(3), delta);
		assertEquals(1.44900000E+00, myPrmtop.getBondEquilValue().get(7), delta);

	}

	@Test
	public void testPrmtopANGLE_FORCE_CONSTANTsection() {

		assertEquals(8.00000000E+01, myPrmtop.getAngleForceConstant().get(0), delta);
		assertEquals(3.50000000E+01, myPrmtop.getAngleForceConstant().get(15), delta);
	}

	@Test
	public void testPrmtopANGLE_EQUIL_VALUEsection() {

		assertEquals(2.14501057E+00, myPrmtop.getAngleEquilValue().get(0), delta);
		assertEquals(1.93906163E+00, myPrmtop.getAngleEquilValue().get(7), delta);
		assertEquals(1.91113635E+00, myPrmtop.getAngleEquilValue().get(15), delta);

	}

	@Test
	public void testPrmtopDIHEDRAL_FORCE_CONSTANTsection() {

		assertEquals(2.00000000E+00, myPrmtop.getDihedralForceConstant().get(0), delta);
		assertEquals(1.55555556E-01, myPrmtop.getDihedralForceConstant().get(11), delta);
		assertEquals(1.10000000E+000, myPrmtop.getDihedralForceConstant().get(18), delta);

	}

	@Test
	public void testPrmtopDIHEDRAL_PERIODICITYsection() {

		assertEquals(1.00000000E+00, myPrmtop.getDihedralPeriodicity().get(0), delta);
		assertEquals(4.00000000E+00, myPrmtop.getDihedralPeriodicity().get(5), delta);
		assertEquals(2.00000000E+00, myPrmtop.getDihedralPeriodicity().get(18), delta);

	}

	@Test
	public void testPrmtopDIHEDRAL_PHASEsection() {

		assertEquals(0.00000000E+00, myPrmtop.getDihedralPhase().get(0), delta);
		assertEquals(3.14159400E+00, myPrmtop.getDihedralPhase().get(10), delta);
		assertEquals(3.14159400E+00, myPrmtop.getDihedralPhase().get(18), delta);

	}

	@Test
	public void testPrmtopSOLTYsection() {

		assertEquals(0.00000000E+00, myPrmtop.getSolty().get(0), delta);
		assertEquals(0.00000000E+00, myPrmtop.getSolty().get(6), delta);

	}

	@Test
	public void testPrmtopLENNARD_JONES_ACOEFsection() {

		assertEquals(7.51607703E+03, myPrmtop.getLennardJonesAcoef().get(0), delta);
		assertEquals(1.07193646E+02, myPrmtop.getLennardJonesAcoef().get(15), delta);
		assertEquals(3.25969625E+03, myPrmtop.getLennardJonesAcoef().get(27), delta);

	}

	@Test
	public void testPrmtopLENNARD_JONES_BCOEFsection() {

		assertEquals(2.17257828E+01, myPrmtop.getLennardJonesBcoef().get(0), delta);
		assertEquals(2.59456373E+00, myPrmtop.getLennardJonesBcoef().get(15), delta);
		assertEquals(1.43076527E+01, myPrmtop.getLennardJonesBcoef().get(27), delta);

	}

	@Test
	public void testPrmtopBONDS_INC_HYDROGENsection() {

		assertEquals(3, myPrmtop.getBondsIncHydrogen().get(0), delta);
		assertEquals(30, myPrmtop.getBondsIncHydrogen().get(15), delta);
		assertEquals(7, myPrmtop.getBondsIncHydrogen().get(35), delta);

	}

	@Test
	public void testPrmtopBONDS_WITHOUT_HYDROGENsection() {

		assertEquals(12, myPrmtop.getBondsWithoutHydrogen().get(0), delta);
		assertEquals(45, myPrmtop.getBondsWithoutHydrogen().get(10), delta);
		assertEquals(8, myPrmtop.getBondsWithoutHydrogen().get(26), delta);

	}

	@Test
	public void testPrmtopANGLES_INC_HYDROGENsection() {

		assertEquals(12, myPrmtop.getAnglesIncHydrogen().get(0), delta);
		assertEquals(36, myPrmtop.getAnglesIncHydrogen().get(58), delta);
		assertEquals(13, myPrmtop.getAnglesIncHydrogen().get(99), delta);

	}

	@Test
	public void testPrmtopANGLES_WITHOUT_HYDROGENsection() {

		assertEquals(15, myPrmtop.getAnglesWithoutHydrogen().get(0), delta);
		assertEquals(42, myPrmtop.getAnglesWithoutHydrogen().get(20), delta);
		assertEquals(15, myPrmtop.getAnglesWithoutHydrogen().get(43), delta);

	}

	@Test
	public void testPrmtopDIHEDRALS_INC_HYDROGENsection() {

		assertEquals(15, myPrmtop.getDihedralsIncHydrogen().get(0), delta);
		assertEquals(-18, myPrmtop.getDihedralsIncHydrogen().get(7), delta);
		assertEquals(-48, myPrmtop.getDihedralsIncHydrogen().get(87), delta);
		assertEquals(19, myPrmtop.getDihedralsIncHydrogen().get(209), delta);

	}

	@Test
	public void testPrmtopDIHEDRALS_WITHOUT_HYDROGENsection() {

		assertEquals(15, myPrmtop.getDihedralsWithoutHydrogen().get(0), delta);
		assertEquals(-24, myPrmtop.getDihedralsWithoutHydrogen().get(42), delta);
		assertEquals(18, myPrmtop.getDihedralsWithoutHydrogen().get(119), delta);

	}

	@Test
	public void testPrmtopEXCLUDED_ATOMS_LISTsection() {

		assertEquals(2, myPrmtop.getExcludedAtomsList().get(0), delta);
		assertEquals(12, myPrmtop.getExcludedAtomsList().get(55), delta);
		assertEquals(0, myPrmtop.getExcludedAtomsList().get(98), delta);
	}

	// TODO
	// Assert nulls on
	//
	// HBOND_ACOEF myPrmtop.getHbondAcoef().get(0)
	// HBOND_BCOEF myPrmtop.getHbondBcoef().get(0)

	@Test
	public void testPrmtopAMBER_ATOM_TYPEsection() {

		assertEquals("HC", myPrmtop.getAmberAtomType().get(0));
		assertEquals("CT", myPrmtop.getAmberAtomType().get(10));
		assertEquals("H1", myPrmtop.getAmberAtomType().get(21));

	}

	@Test
	public void testPrmtopTREE_CHAIN_CLASSIFICATIONsection() {

		assertEquals("M", myPrmtop.getTreeChainClassification().get(0));
		assertEquals("3", myPrmtop.getTreeChainClassification().get(10));
		assertEquals("E", myPrmtop.getTreeChainClassification().get(21));

	}

	@Test
	public void testPrmtopJOIN_ARRAYsection() {

		assertEquals(0, myPrmtop.getJoinArray().get(0), delta);
		assertEquals(0, myPrmtop.getJoinArray().get(10), delta);
		assertEquals(0, myPrmtop.getJoinArray().get(21), delta);

	}

	@Test
	public void testPrmtopIROTATsection() {

		assertEquals(0, myPrmtop.getJoinArray().get(0), delta);
		assertEquals(0, myPrmtop.getJoinArray().get(10), delta);
		assertEquals(0, myPrmtop.getJoinArray().get(21), delta);

	}

	@Test
	public void testPrmtopRADIUS_SETsection() {

		assertEquals("modified Bondi radii (mbondi)", myPrmtop.getRadiusSet());

	}

	@Test
	public void testPrmtopRADIIsection() {

		assertEquals(1.30000000E+00, myPrmtop.getRadii().get(0), delta);
		assertEquals(1.70000000E+00, myPrmtop.getRadii().get(4), delta);
		assertEquals(1.30000000E+00, myPrmtop.getRadii().get(21), delta);

	}

	@Test
	public void testPrmtopSCREENsection() {

		assertEquals(8.50000000E-01, myPrmtop.getScreen().get(0), delta);
		assertEquals(7.20000000E-01, myPrmtop.getScreen().get(4), delta);
		assertEquals(8.50000000E-01, myPrmtop.getScreen().get(21), delta);

	}

}
