package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;
import org.junit.Test;

import name.mjw.jamber.io.amber.AtomType;
import name.mjw.jamber.io.amber.BondType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class BondTypeTest {
	final Logger LOG = Logger.getLogger(BondTypeTest.class);
	
	private static final AtomType a = new AtomType("CA", 1.0);
	private static final AtomType b = new AtomType("CA", 2.0);
	private static final AtomType c = new AtomType("ca", 1.0);

	private static final BondType bondAtoB1 = new BondType(a, b, 100, 1);
	private static final BondType bondAtoB2 = new BondType(a, b, 10, 1);
	private static final BondType bondAtoC1 = new BondType(a, c, 100, 1);

	@Test
	public void testBondTypesAreEqual1() {

		assertEquals(bondAtoB1, bondAtoB1);

	}

	@Test
	public void testBondTypesAreEqual2() {

		assertEquals(bondAtoB1, bondAtoB2);

	}

	@Test
	public void testBondTypesAreNotEqual() {

		assertNotSame(bondAtoB1, bondAtoC1);
	}

}
