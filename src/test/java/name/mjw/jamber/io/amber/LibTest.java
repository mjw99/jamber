package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import name.mjw.jamber.io.Mol2;
import name.mjw.jamber.io.amber.Atom;
import name.mjw.jamber.io.amber.Connection;
import name.mjw.jamber.io.amber.Lib;
import name.mjw.jamber.io.amber.Residue;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class LibTest {

	final Logger LOG = Logger.getLogger(LibTest.class);

	private static Lib lib;

	@BeforeClass
	public static void setUp() {

		InputStream amino94Stream = LibTest.class.getResourceAsStream("/name/mjw/jamber/io/amber/lib/all_amino94.lib");

		InputStream aminont94Stream = LibTest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/lib/all_aminont94.lib");

		InputStream aminoct94Stream = LibTest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/lib/all_aminoct94.lib");

		try {
			lib = new Lib(amino94Stream, aminont94Stream, aminoct94Stream);

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	@Test
	public void testLibGetName() {

		Residue residue = lib.getResidueByName("PRO");
		LOG.debug("Residue is " + residue);
		assertEquals("PRO", residue.getName());

	}

	@Test
	public void testLibGetAtoms() {

		Residue residue = lib.getResidueByName("CYS");

		LOG.debug(residue.getName() + " has " + residue.getAtoms().size() + " atoms");

		assertEquals(11, residue.getAtoms().size());

		for (Atom atom : residue.getAtoms()) {
			LOG.debug(atom + " " + atom.getType());

		}

	}

	@Test
	public void testLibGetAtomTypes() {

		Residue residue = lib.getResidueByName("LEU");

		assertEquals("HC", residue.getAtoms().get(5).getType());

	}

	@Test
	public void testLibGetConnections() {

		Residue residue = lib.getResidueByName("NGLY");
		ArrayList<Connection> connections = residue.getConnections();

		LOG.debug(residue.getName() + " connections are " + connections);

		assertEquals("N", connections.get(0).getI().toString());
		assertEquals("O", connections.get(7).getJ().toString());

	}

	@Test
	public void testLibGetExternalBonds() {

		Residue residue = lib.getResidueByName("TYR");
		ArrayList<Atom> externalBondAtoms = residue.getExternalBonds();

		LOG.debug(residue.getName() + " ExternalBondAtoms are " + externalBondAtoms);

		assertEquals("N", externalBondAtoms.get(0).toString());
		assertEquals("C", externalBondAtoms.get(1).toString());

	}

	@Test
	public void testLibEnumerateAllAtoms() {

		int residueCount = 0;
		int atomCount = 0;
		for (Residue residue : lib.getResidues()) {

			residueCount++;
			for (@SuppressWarnings("unused")
			Atom atom : residue.getAtoms()) {

				// LOG.debug(atom.getType() + " " + atom.getCharge());
				atomCount++;

			}

		}

		assertEquals(1251, atomCount);
		assertEquals(76, residueCount);

		assertEquals(1251, lib.getAtoms().size());

		assertEquals(1069, lib.getUniqueAtoms().size());

		LOG.debug("Atom count is " + atomCount);
		LOG.debug("Residue count is " + residueCount);

	}

	@Test
	public void testLibFromMol2() {

		Lib lib2;
		Mol2 mol2 = null;

		InputStream mol2Stream = LibTest.class.getResourceAsStream("/name/mjw/jamber/io/amber/CPDI/HEM.mol2");

		try {
			mol2 = new Mol2(mol2Stream);

		} catch (IOException e) {

			e.printStackTrace();
		}

		lib2 = new Lib(mol2);

		assertEquals(74, lib2.getAtoms().size());

	}

}
