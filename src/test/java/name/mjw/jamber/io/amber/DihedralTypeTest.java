package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;
import org.junit.Test;

import name.mjw.jamber.io.amber.AtomType;
import name.mjw.jamber.io.amber.ProperDihedralType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class DihedralTypeTest {
	final Logger LOG = Logger.getLogger(DihedralTypeTest.class);
	
	private static final AtomType a = new AtomType("CA", 1.0);
	private static final AtomType b = new AtomType("CA", 1.0);
	private static final AtomType c = new AtomType("CT", 1.0);
	private static final AtomType d = new AtomType("HT", 1.0);
	private static final AtomType e = new AtomType("CT", 1.0);

	private static final ProperDihedralType dihedral1 = new ProperDihedralType(
			a, b, c, d, 1, 10.0, 180.0, 2.0);

	private static final ProperDihedralType dihedral2 = new ProperDihedralType(
			a, b, c, d, 1, 5.0, 180.0, 2.0);

	private static final ProperDihedralType dihedral3 = new ProperDihedralType(
			a, b, c, e, 1, 10.0, 180.0, 2.0);

	@Test
	public void testDihedralTypesAreEqual1() {

		assertEquals(dihedral1, dihedral1);

	}

	@Test
	public void testDihedralTypesAreEqual2() {

		assertEquals(dihedral1, dihedral2);

	}

	@Test
	public void testDihedralTypesAreNotEqual() {

		assertNotSame(dihedral1, dihedral3);
	}

}
