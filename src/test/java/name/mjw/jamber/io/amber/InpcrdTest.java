package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import name.mjw.jamber.io.amber.Inpcrd;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;

/**
 * Inpcrd parsing tests
 * 
 * @author mjw
 * 
 */
public class InpcrdTest {
	
	final Logger LOG = Logger.getLogger(InpcrdTest.class);

	private static final String INPCRD = "/name/mjw/jamber/io/amber/blockedAlanineDipeptide/inpcrd";
	private final double delta = 0.00001;
	private static Inpcrd myInpcrd;

	@BeforeClass
	public static void setUp() {

		String in = null;

		try {
			in = URLDecoder.decode(
					InpcrdTest.class.getResource(INPCRD).getFile(),
					"utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		myInpcrd = new Inpcrd();
		try {
			myInpcrd.read(in);
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}

    }

	/**
	 * Test that the title is correctly read from an inpcrd file
	 */
	@Test
	@Ignore
	public void testInpcrdTitle() {

		assertEquals("ACE", myInpcrd.getTitle().trim());
	}

	/**
	 * Test that the number of atoms and the current time step is correctly read
	 * from an inpcrd file
	 */
	@Test
	public void testInpcrdAtomNumberAndTimeStep() {

		assertEquals(22, myInpcrd.getNumberOfAtoms());
		assertEquals(0.5005000E+00, myInpcrd.getTimeStep(), delta);
	}

	/**
	 * Test that some coordinates are correctly read from an inpcrd file
	 */
	@Test
	public void testInpcrdCoordinates() {

		assertEquals(2.0773821, myInpcrd.getPositions().get(0).x, delta);
		assertEquals(1.0381474, myInpcrd.getPositions().get(0).y, delta);
		assertEquals(0.1406479, myInpcrd.getPositions().get(0).z, delta);

		assertEquals(5.1726618, myInpcrd.getPositions().get(21).x, delta);
		assertEquals(8.6445622, myInpcrd.getPositions().get(21).y, delta);
		assertEquals(-0.6067434, myInpcrd.getPositions().get(21).z, delta);

	}

	/**
	 * Test that some velocities are correctly read from an inpcrd file
	 */
	@Test
	public void testInpcrdVelocities() {

		assertEquals(-0.1651850, myInpcrd.getVelocities().get(0).x, delta);
		assertEquals(0.7509604, myInpcrd.getVelocities().get(0).y, delta);
		assertEquals(-0.3443447, myInpcrd.getVelocities().get(0).z, delta);

		assertEquals(0.3337041, myInpcrd.getVelocities().get(21).x, delta);
		assertEquals(1.6765261, myInpcrd.getVelocities().get(21).y, delta);
		assertEquals(0.7422618, myInpcrd.getVelocities().get(21).z, delta);

	}

}
