package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import name.mjw.jamber.io.amber.ParameterStore;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;

public class ParameterStoreWithFrcmodTest {

	final Logger LOG = Logger.getLogger(ParameterStoreWithFrcmodTest.class);

	private static ParameterStore parameterStore;

	@BeforeClass
	public static void setUp() {

		InputStream frcmodIs = ParameterStoreWithFrcmodTest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/CPDI/CPDI.frcmod");

		InputStream ff99Is = ParameterStoreWithFrcmodTest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/lib/parm99.dat");

		InputStream gaffIs = ParameterStoreWithFrcmodTest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/lib/gaff.dat");

		try {
			parameterStore = new ParameterStore();
			parameterStore.readParm(ff99Is);
			parameterStore.readParm(gaffIs);

			try {
				parameterStore.readFrcmod(frcmodIs);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testAtomTypesSize() {
		assertEquals(135, parameterStore.getAtomTypesSize());

	}

}
