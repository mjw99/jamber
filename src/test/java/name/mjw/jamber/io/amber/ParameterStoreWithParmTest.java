package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import name.mjw.jamber.io.amber.ParameterStore;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class ParameterStoreWithParmTest {

	final Logger LOG = Logger.getLogger(ParameterStoreWithParmTest.class);

	private static ParameterStore parameterStore;

	@BeforeClass
	public static void setUp() {

		InputStream is = ParameterStoreWithParmTest.class
				.getResourceAsStream("/name/mjw/jamber/io/amber/lib/parm99.dat");

		try {
			parameterStore = new ParameterStore();
			parameterStore.readParm(is);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testAtomTypesSize() {

		assertEquals(62, parameterStore.getAtomTypesSize());

	}

	@Test
	public void testBondTypesSize() {

		assertEquals(116, parameterStore.getBondTypesSize());

	}

	@Test
	public void testAngleTypesSize() {

		assertEquals(281, parameterStore.getAngleTypesSize());

	}

	@Test
	public void testDihedralTypesSize() {

		// TODO Check
		assertEquals(156, parameterStore.getDihedralTypesSize());

	}

}
