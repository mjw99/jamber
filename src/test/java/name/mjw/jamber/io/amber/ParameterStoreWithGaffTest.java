package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import name.mjw.jamber.io.amber.ParameterStore;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

/**
 * Tests based on gaff.dat (version 1.4) AMBER12 with md5sum
 * 82644c065ffcc3997558bd56e772a146
 * 
 * @author mjw
 * 
 */
public class ParameterStoreWithGaffTest {

	final Logger LOG = Logger.getLogger(ParameterStoreWithGaffTest.class);

	private static ParameterStore parameterStore;

	@BeforeClass
	public static void setUp() {

		InputStream is = ParameterStoreWithGaffTest.class.getResourceAsStream("/name/mjw/jamber/io/amber/lib/gaff.dat");

		try {
			parameterStore = new ParameterStore();
			parameterStore.readParm(is);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testAtomTypesSize() {

		assertEquals(71, parameterStore.getAtomTypesSize());

	}

	@Test
	public void testBondTypesSize() {

		assertEquals(791, parameterStore.getBondTypesSize());

	}

	@Test
	public void testAngleTypesSize() {

		assertEquals(4071, parameterStore.getAngleTypesSize());

	}

	@Test
	public void testDihedralTypesSize() {

		assertEquals(675, parameterStore.getDihedralTypesSize());

	}

}
