package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;
import org.junit.Test;

import name.mjw.jamber.io.amber.AngleType;
import name.mjw.jamber.io.amber.AtomType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class AngleTypeTest {
	final Logger LOG = Logger.getLogger(AngleTypeTest.class);
	
	private static final AtomType a = new AtomType("CA", 1.0);
	private static final AtomType b = new AtomType("CA", 1.0);
	private static final AtomType c = new AtomType("HT", 1.0);
	private static final AtomType d = new AtomType("HT", 1.0);

	private static final AngleType angle1 = new AngleType(a, b, c, 10.0, 100.0);
	private static final AngleType angle2 = new AngleType(a, b, c, 1.0, 100.0);
	
	private static final AngleType angle3 =  new AngleType(a, b, d, 10.0, 100.0);
	
	
	@Test
	public void testAngleTypesAreEqual1() {

		assertEquals(angle1, angle1);

	}

	@Test
	public void testAngleTypesAreEqual2() {

		assertEquals(angle1, angle2);

	}

	@Test
	public void testBondTypesAreNotEqual() {

		assertNotSame(angle1, angle3);
	}

}
