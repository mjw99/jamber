package name.mjw.jamber.io.amber;

import org.apache.log4j.Logger;
import org.junit.Test;

import name.mjw.jamber.io.amber.AtomType;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class AtomTypeTest {
	
	final Logger LOG = Logger.getLogger(AtomTypeTest.class);

	private static final AtomType a = new AtomType("CA", 1.0);
	private static final AtomType b = new AtomType("CA", 2.0);
	private static final AtomType c = new AtomType("ca", 1.0);

	@Test
	public void testAtomTypesAreEqual() {

		assertEquals(a, b);

	}

	@Test
	public void testAtomTypesAreNotEqual() {

		assertNotSame(a, c);

	}

}
