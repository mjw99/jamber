package name.mjw.jamber.io;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import name.mjw.jamber.io.Mol2;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class Mol2CYPTest {

	final Logger LOG = Logger.getLogger(Mol2CYPTest.class);

	private final double delta = 0.00001;

	private static Mol2 cypMol2;

	@BeforeClass
	public static void setUp() {

		InputStream cypIs = Mol2CYPTest.class.getResourceAsStream("/name/mjw/jamber/io/amber/CYP/CYP.mol2");

		try {
			cypMol2 = new Mol2(cypIs);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testNumberOfAtoms() {

		assertEquals(10, cypMol2.getResidue().getAtoms().size());

	}

	@Test
	public void testNameOfAtom() {

		assertEquals("O", cypMol2.getResidue().getAtoms().get(9).getName());

	}

	@Test
	public void testChargeOfAtom() {

		assertEquals(0.5973, cypMol2.getResidue().getAtoms().get(8).getCharge(), delta);

	}

	@Test
	public void testNumberOfBonds() {

		assertEquals(9, cypMol2.getResidue().getConnections().size());

	}

}
