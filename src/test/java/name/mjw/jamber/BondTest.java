package name.mjw.jamber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.Assert.assertEquals;

/**
 * 
 * @author mjw
 * 
 */
public class BondTest {

	final Logger LOG = Logger.getLogger(BondTest.class);

	private static Atom myAtom1;
	private static Atom myAtom2;

	private static Bond myBond;

	private final double delta = 0.00001;

	@BeforeClass
	public static void setUp() {

		myAtom1 = new Atom(new Point3d(1.0, 0.0, 0.0));
		myAtom2 = new Atom(new Point3d(1.0, 1.0, 0.0));

		myBond = new Bond(myAtom1, myAtom2, 0.8, 50.0);

	}

	@Test
	public void testGetCurrentLength() {
		assertEquals(1.0, myBond.getCurrentLength(), delta);
		LOG.debug("myBond current length is " + myBond.getCurrentLength()
				+ " A");

	}

	@Test
	public void testGetEquilibriumLength() {
		assertEquals(0.8, myBond.getEquilibriumLength(), delta);
		LOG.debug("myBond equilibrium length is " + myBond.getForceConstant()
				+ " A");

	}

	@Test
	public void testGetForceConstant() {
		assertEquals(50.0, myBond.getForceConstant(), delta);
		LOG.debug("myBond force constant is " + myBond.getForceConstant()
				+ " kcal/mol/A**2");

	}

	@Test
	public void testGetPotentialEnergy() {
		assertEquals(2.0, myBond.getPotentialEnergy(), delta);
		LOG.debug("myBond potential energy is " + myBond.getPotentialEnergy()
				+ " kcal/mol");

	}

	@Test
	public void testGetAnalyticalGradient() {

		myBond.getAnalyticalGradient();

		assertEquals(20.0, myBond.getAnalyticalGradient(), delta);

		LOG.debug("Gradient due to bond is " + myBond.getAnalyticalGradient()
				+ " kcal/mol/A");
	}

	@Test
	public void testEvaluateForce() {

		myBond.evaluateForce();

		// Express the result as a Vector3d object
		Vector3d myAtom1Force = new Vector3d(0.0, -19.999999999999996, 0.0);
		Vector3d myAtom2Force = new Vector3d(0.0, 19.999999999999996, 0.0);

		assertEquals(myAtom1Force, myAtom1.getForce());
		assertEquals(myAtom2Force, myAtom2.getForce());

		LOG.debug("Force on myAtom1 is " + myAtom1.getForce().toString()
				+ " kcal/mol/A");
		LOG.debug("Force on myAtom2 is " + myAtom2.getForce().toString()
				+ " kcal/mol/A");

	}

}
