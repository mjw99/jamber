package name.mjw.jamber;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.Assert.assertEquals;

/**
 * 
 * @author mjw
 * 
 */
public class DihedralTest

{
	final Logger LOG = Logger.getLogger(DihedralTest.class);

	private final double delta = 0.00001;

	private static Atom myAtom1;
	private static Atom myAtom2;
	private static Atom myAtom3;
	private static Atom myAtom4;

	private static Dihedral myDihedral;

	@Before
	public void setUp() {

		// Should be 180 degrees
		myAtom1 = new Atom(new Point3d(0.0, 0.0, 0.0));
		myAtom2 = new Atom(new Point3d(1.0, 1.0, 0.0));
		myAtom3 = new Atom(new Point3d(2.0, 1.0, 0.0));
		myAtom4 = new Atom(new Point3d(3.0, 2.0, 0.0));

		myDihedral = new Dihedral(myAtom1, myAtom2, myAtom3, myAtom4, 5.0, 0.0,
				2.0);

	}

	@Test
	public void testBarrierHeight() {

		assertEquals(5.0, myDihedral.getBarrierHeight(), delta);
		LOG.debug("myDihedral barrierheight is "
				+ myDihedral.getBarrierHeight() + " kcal/mol");

	}

	@Test
	public void testGetPhase() {

		assertEquals(0, myDihedral.getPhase(), delta);
		LOG.debug("myDihedral phase angle is " + myDihedral.getPhase()
				+ " degrees");

	}

	@Test
	public void testGetMultiplicity() {

		assertEquals(1.0, myDihedral.getMultiplicity(), delta);
		LOG.debug("myDihedral multiplicity is " + myDihedral.getMultiplicity());
	}

	@Test
	public void testCurrentGetDihedralAngle0() {

		assertEquals(180.0, myDihedral.getCurrentDihedralAngle(), delta);
		LOG.debug("myDihedral angle is " + myDihedral.getCurrentDihedralAngle()
				+ " degrees");
	}

	@Test
	public void testCurrentGetDihedralAngle1() {

		myAtom1.setPosition(new Point3d(60.614941, 67.371346, 12.713049));
		myAtom2.setPosition(new Point3d(59.104156, 67.044731, 12.827555));
		myAtom3.setPosition(new Point3d(58.827194, 65.911911, 13.835642));
		myAtom4.setPosition(new Point3d(59.367950, 66.384277, 15.233553));

		LOG.debug("myDihedral angle is " + myDihedral.getCurrentDihedralAngle()
				+ " degrees");
		assertEquals(59.66383, myDihedral.getCurrentDihedralAngle(), delta);

	}

	@Test
	public void testCurrentGetDihedralAngle2() {
		myDihedral = new Dihedral(myAtom1, myAtom2, myAtom3, myAtom4, 10.0,
				2.0, 4.0);
		
		myAtom1.setPosition(new Point3d(30.958572, 73.709137, 38.031029));
		myAtom2.setPosition(new Point3d(31.924915, 72.577698, 37.985279));
		myAtom3.setPosition(new Point3d(31.643818, 71.563255, 36.873047));
		myAtom4.setPosition(new Point3d(31.253489, 70.131218, 37.371773));

		LOG.debug("myDihedral angle is " + myDihedral.getCurrentDihedralAngle()
				+ " degrees");
		
		// This is correct as of my fortran torsion tool
		assertEquals(-115.172063, myDihedral.getCurrentDihedralAngle(), delta);
		
		LOG.debug("myDihedral potential energy  is " + myDihedral.getPotentialEnergy());
				
		
		myDihedral.evaluateForce();
		
		LOG.debug("Force on myAtom1 is " + myAtom1.getForce().toString());
		LOG.debug("Force on myAtom2 is " + myAtom2.getForce().toString());
		LOG.debug("Force on myAtom3 is " + myAtom3.getForce().toString());
		LOG.debug("Force on myAtom4 is " + myAtom4.getForce().toString());


	}

	@Test
	public void testGetPotentialEnergy() {
		// Should be 180 degrees, pi radians
		myAtom1.setPosition(new Point3d(0.0, 0.0, 0.0));
		myAtom2.setPosition(new Point3d(1.0, 1.0, 0.0));
		myAtom3.setPosition(new Point3d(2.0, 1.0, 0.0));
		myAtom4.setPosition(new Point3d(3.0, 2.0, 0.0));

		LOG.debug("myDihedral Potential energy is "
				+ myDihedral.getPotentialEnergy() + " kcal/mol");

		assertEquals(10.0, myDihedral.getPotentialEnergy(), delta);
	}

	@Test
	public void testGetAnalyticalGradient() {

		myAtom1.setPosition(new Point3d(0.0, 0.0, 0.0));
		myAtom2.setPosition(new Point3d(1.0, 1.0, 0.0));
		myAtom3.setPosition(new Point3d(2.0, 1.0, 0.0));
		myAtom4.setPosition(new Point3d(3.0, 2.0, 0.0));

		LOG.debug("myDihedral analytical gradient is "
				+ myDihedral.getAnalyticalGradient() + " kcal/mol/degree");

		assertEquals(0.0, myDihedral.getAnalyticalGradient(), delta);

	}

	@Test
	public void testEvaluateForce1() {

		myDihedral = new Dihedral(myAtom1, myAtom2, myAtom3, myAtom4, 5.0, 1.0,
				2.0);

		myDihedral.evaluateForce();

		Vector3d ExpectedForceI = new Vector3d(0.0, 0.0, -8.414709848078965);
		Vector3d ExpectedForceJ = new Vector3d(0.0, 0.0, 8.414709848078965);
		Vector3d ExpectedForceK = new Vector3d(0.0, 0.0, 8.414709848078965);
		Vector3d ExpectedForceL = new Vector3d(0.0, 0.0, -8.414709848078965);

		LOG.debug("Force on myAtom1 is " + myAtom1.getForce().toString());
		LOG.debug("Force on myAtom2 is " + myAtom2.getForce().toString());
		LOG.debug("Force on myAtom3 is " + myAtom3.getForce().toString());
		LOG.debug("Force on myAtom4 is " + myAtom4.getForce().toString());

		assertEquals(ExpectedForceI, myAtom1.getForce());
		assertEquals(ExpectedForceJ, myAtom2.getForce());
		assertEquals(ExpectedForceK, myAtom3.getForce());
		assertEquals(ExpectedForceL, myAtom4.getForce());

	}

	@Test
	public void testEvaluateForce2() {

		myDihedral = new Dihedral(myAtom1, myAtom2, myAtom3, myAtom4, 10.0,
				2.0, 4.0);

		myDihedral.evaluateForce();

		Vector3d ExpectedForceI = new Vector3d(0.0, 0.0, -36.371897073027256);
		Vector3d ExpectedForceJ = new Vector3d(0.0, 0.0, 36.371897073027256);
		Vector3d ExpectedForceK = new Vector3d(0.0, 0.0, 36.371897073027256);
		Vector3d ExpectedForceL = new Vector3d(0.0, 0.0, -36.371897073027256);

		LOG.debug("Force on myAtom1 is " + myAtom1.getForce().toString());
		LOG.debug("Force on myAtom2 is " + myAtom2.getForce().toString());
		LOG.debug("Force on myAtom3 is " + myAtom3.getForce().toString());
		LOG.debug("Force on myAtom4 is " + myAtom4.getForce().toString());

		assertEquals(ExpectedForceI, myAtom1.getForce());
		assertEquals(ExpectedForceJ, myAtom2.getForce());
		assertEquals(ExpectedForceK, myAtom3.getForce());
		assertEquals(ExpectedForceL, myAtom4.getForce());

	}

}
