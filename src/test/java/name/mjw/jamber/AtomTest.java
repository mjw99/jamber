package name.mjw.jamber;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.vecmath.Point3d;

import static org.junit.Assert.assertEquals;

/**
 * 
 * @author mjw
 * 
 */
public class AtomTest

{
	final Logger LOG = Logger.getLogger(AtomTest.class);

	private static Atom myAtom;

	@BeforeClass
	public static void setUp() {

		myAtom = new Atom(new Point3d(1.0, 2.0, 3.0));

	}

	@Test
	public void testGetPosition() {
		Point3d myAtomPosition = new Point3d(1.0, 2.0, 3.0);

		assertEquals(myAtomPosition, myAtom.getPosition());

	}

}
