package name.mjw.jamber;

import name.mjw.jamber.util.Torsion;
import name.mjw.jamber.util.TorsionGradients;

import org.apache.log4j.Logger;
import org.junit.Test;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.Assert.assertEquals;

public class TorsionTest {
	final Logger LOG = Logger.getLogger(TorsionTest.class);
	
	private final double delta = 0.00001;

	@Test
	public void testTorsionAngle1() {

		
		// Should be 180 degrees
		//
		//				l
		//		j	k
		//	i
		//
		Point3d i = new Point3d(0.0, 0.0, 0.0);
		Point3d j = new Point3d(1.0, 1.0, 0.0);
		Point3d k = new Point3d(2.0, 1.0, 0.0);
		Point3d l = new Point3d(3.0, 2.0, 0.0);

		Torsion torsion = new Torsion();
		
		assertEquals(180.0, torsion.getAngle(i,j,k,l), delta);		
	}
	
	@Test
	public void testTorsionAngle2() {
		
		Point3d i = new Point3d(60.614941, 67.371346, 12.713049);
		Point3d j = new Point3d(59.104156, 67.044731, 12.827555);
		Point3d k = new Point3d(58.827194, 65.911911, 13.835642);
		Point3d l = new Point3d(59.367950, 66.384277, 15.233553);

		Torsion torsion = new Torsion();
		
		assertEquals(59.66383665358091, torsion.getAngle(i,j,k,l), delta);
	}
	
	@Test
	public void testTorsionAngle3() {
//		mw529@cyclops:~/lab/torsion_test$ make
//		gfortran -O3 torsion.F90 -o torsion
//		./torsion
//
//		          Coordinates: x,y,z
//		          i 30.958572  73.709137  38.031029
//		          j 31.924915  72.577698  37.985279
//		          k 31.643818  71.563255  36.873047
//		          l 31.253489  70.131218  37.371773
//
//		          rij -0.966343   1.131439   0.045750
//		          rkj -0.281097  -1.014442  -1.112232
//		          rlk -0.390329  -1.432037   0.498726
//
//		            onesizcb  0.653000
//		                 ucb -0.183556  -0.662430  -0.726287
//
//		           (R)   pab -1.077459   0.730438  -0.393907
//		           onesizpab  0.735290
//		                upab -0.792245   0.537084  -0.289636
//
//		           (S)   pdc -0.269539  -0.996122   0.976662
//		           onesizpdc  0.703810
//		                updc -0.189704  -0.701081   0.687385
//
//		              rcross  0.166125   0.599522   0.657315
//
//		               upabc -0.581941  -0.522233   0.623392
//
//		               upbcd -0.964531   0.263954   0.003022
//
//		          dotp_ab_cb -0.605349
//		          dotp_dc_cb  0.658054
//		                VecS -0.122563   0.231618  -0.180278
//
//
//		            cosphi:   -0.425
//		            sinphi:   -0.905
//		             Angle: -115.230
//
//		          Gradients x,y,z
//		          i -0.427895  -0.383993   0.458374
//		          j  0.305333   0.615610  -0.638652
//		          k  0.801409  -0.417391   0.178151
//		          l -0.678846   0.185773   0.002127

		
		Point3d i = new Point3d(30.958572, 73.709137, 38.031029);
		Point3d j = new Point3d(31.924915, 72.577698, 37.985279);
		Point3d k = new Point3d(31.643818, 71.563255, 36.873047);
		Point3d l = new Point3d(31.253489, 70.131218, 37.371773);

		Torsion torsion = new Torsion();
				
		assertEquals(-115.17206338216852, torsion.getAngle(i,j,k,l), delta);
		
		TorsionGradients gradients;
		
		gradients = torsion.getGradients(i,j,k,l);
		
		LOG.debug(gradients.getI());
		LOG.debug(gradients.getJ());
		LOG.debug(gradients.getK());
		LOG.debug(gradients.getL());
		
		// Correct Gradients
		Vector3d a = new Vector3d(-0.427895,  -0.383993,   0.458374 );
		Vector3d b = new Vector3d( 0.305333,   0.615610,  -0.638652 );
		Vector3d c = new Vector3d( 0.801409,  -0.417391,   0.178151 );
		Vector3d d = new Vector3d(-0.678846,   0.185773,   0.002127 );
		
		assertEquals(a.x, gradients.getI().x, delta );
		assertEquals(a.y, gradients.getI().y, delta );
		assertEquals(a.z, gradients.getI().z, delta );
		
		assertEquals(b.x, gradients.getJ().x, delta );
		assertEquals(b.y, gradients.getJ().y, delta );
		assertEquals(b.z, gradients.getJ().z, delta );
		
		assertEquals(c.x, gradients.getK().x, delta );
		assertEquals(c.y, gradients.getK().y, delta );
		assertEquals(c.z, gradients.getK().z, delta );
		
		assertEquals(d.x, gradients.getL().x, delta );
		assertEquals(d.y, gradients.getL().y, delta );
		assertEquals(d.z, gradients.getL().z, delta );
		
	}
	

	@Test
	public void testTorsionGradients() {
		
		//
		//      cosphi:    0.505
		//      sinphi:    0.863
		//       Angle:   59.694		
		//
		//      Gradients x,y,z
		//      i  0.062885  -0.469977  -0.510851
		//      j -0.267922   0.730722   0.747527
		//      k  0.799196  -0.529675  -0.375642
		//      l -0.594160   0.268930   0.138966
		
		Point3d i = new Point3d(60.614941, 67.371346, 12.713049);
		Point3d j = new Point3d(59.104156, 67.044731, 12.827555);
		Point3d k = new Point3d(58.827194, 65.911911, 13.835642);
		Point3d l = new Point3d(59.367950, 66.384277, 15.233553);

			
		Torsion torsion = new Torsion();
		
		TorsionGradients gradients;
		
		gradients = torsion.getGradients(i,j,k,l);
		
		// Correct Gradients
		Vector3d a = new Vector3d( 0.062885,  -0.469977,  -0.510851 );
		Vector3d b = new Vector3d(-0.267922,   0.730722,   0.747527 );
		Vector3d c = new Vector3d( 0.799196,  -0.529675,  -0.375642 );
		Vector3d d = new Vector3d(-0.594160,   0.268930,   0.138966 );
		
		LOG.debug(gradients.getI());
		LOG.debug(gradients.getJ());
		LOG.debug(gradients.getK());
		LOG.debug(gradients.getL());
						
		assertEquals(a.x, gradients.getI().x, delta );
		assertEquals(a.y, gradients.getI().y, delta );
		assertEquals(a.z, gradients.getI().z, delta );
		
		assertEquals(b.x, gradients.getJ().x, delta );
		assertEquals(b.y, gradients.getJ().y, delta );
		assertEquals(b.z, gradients.getJ().z, delta );
		
		assertEquals(c.x, gradients.getK().x, delta );
		assertEquals(c.y, gradients.getK().y, delta );
		assertEquals(c.z, gradients.getK().z, delta );
		
		assertEquals(d.x, gradients.getL().x, delta );
		assertEquals(d.y, gradients.getL().y, delta );
		assertEquals(d.z, gradients.getL().z, delta );
		
	}



}
