- Make use of Flag class within prmtop class

- Add a write method to the prmtop class

- Make use of jline2 for console aspect and tab complete of commands
	https://github.com/jline/jline2

- Make use of jtransforms for FFT in PME
	https://sites.google.com/site/piotrwendykier/software/jtransforms

	<dependency>
    		<groupId>edu.emory.mathcs</groupId>
    		<artifactId>JTransforms</artifactId>
    		<version>2.4</version>
	</dependency>
